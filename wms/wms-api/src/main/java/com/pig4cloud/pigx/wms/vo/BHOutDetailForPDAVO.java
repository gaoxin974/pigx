package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "出库单备货商品托盘信息forPDA")
public class BHOutDetailForPDAVO {

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="托盘数量")
	private int tpNum;
}
