/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 库位信息
 *
 * @author gaoxin
 * @date 2021-04-13 09:16:42
 */
@Data
@TableName("s_location")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "库位信息")
public class SLocationVO extends Model<SLocationVO> {
private static final long serialVersionUID = 1L;

    /**
     * 库位号
     */
    @TableId
    @ApiModelProperty(value="库位号")
    private String sLocationNo;
    /**
     * 物理编号
     */
    @ApiModelProperty(value="物理编号")
    private String wNo;
    /**
     * 优先级序号
     */
    @ApiModelProperty(value="优先级序号")
    private Integer sOrder;
    /**
     * 托盘编码
     */
    @ApiModelProperty(value="托盘编码")
    private String trayNo;
    /**
     * 批号
     */
    @ApiModelProperty(value="批号")
    private String batchNo;
    /**
     * 批次
     */
    @ApiModelProperty(value="批次")
    private String batchCi;
    /**
     * 商品sku
     */
    @ApiModelProperty(value="商品sku")
    private String sku;
    /**
     * 货主ID
     */
    @ApiModelProperty(value="货主ID")
    private String huozId;
	/**
	 * 货主名称
	 */
	@ApiModelProperty(value="货主名称")
	private String huozName;
    /**
     * 真实货主ID
     */
    @ApiModelProperty(value="真实货主ID")
    private String realHuoz;
	/**
	 * 真实货主名称
	 */
	@ApiModelProperty(value="真实货主名称")
	private String realHuozName;
    /**
     * 库存
     */
    @ApiModelProperty(value="库存")
    private Integer stockNum;
	/**
	 * 最小包装单位
	 */
	@ApiModelProperty(value="最小包装单位")
	private String minUnit;
    /**
     * 所属区域
     */
    @ApiModelProperty(value="所属区域")
    private Integer areaId;
    /**
     * 状态 1：已使用 0：未使用
     */
    @ApiModelProperty(value="状态 1：已使用 0：未使用")
    private String isUsed;
    /**
     * 状态 1：启用 0：停用
     */
    @ApiModelProperty(value="状态 1：启用 0：停用")
    private String stauts;

    }
