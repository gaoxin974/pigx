package com.pig4cloud.pigx.wms.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
//@ColumnWidth(30)
public class StockQueryExcelVO {

	@ExcelProperty("库位编号")
	private String locationNo;

	@ExcelProperty(value="sku")
	private String sku;

	@ExcelProperty(value="商品名称")
	private String skuName;

	@ExcelProperty(value="托盘号")
	private String trayNo;

	@ExcelProperty(value="批号")
	private String batchNo;

	@ExcelProperty(value="入库批次")
	private String inBillNo;

	@ExcelProperty(value="库存数量")
	private String stockAmount;

	@ExcelProperty(value="出库数量")
	private String outAmount;

	@ExcelProperty(value="生产日期")
	private String proDate;

	@ExcelProperty(value="失效日期")
	private String expDate;

	@ExcelProperty(value="wNo")
	private String wNo;

	@ExcelProperty(value="货主id")
	private String hzId;

	@ExcelProperty(value="库区编号")
	private String stockAreaNo;

	@ExcelProperty(value="可用状态")
	private String status;

	@ExcelProperty(value="库区锁定状态")
	private String areaLockStatus;

}
