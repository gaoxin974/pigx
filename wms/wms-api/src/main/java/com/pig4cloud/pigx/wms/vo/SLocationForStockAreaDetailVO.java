package com.pig4cloud.pigx.wms.vo;

import java.util.List;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "库区详情中库位信息VO")
public class SLocationForStockAreaDetailVO {

private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="库位编号主键")
    private String sLocationNo;

	@ApiModelProperty(value="库位序号")
	private String sOrder;

    @ApiModelProperty(value="托盘编号")
    private String trayNo;

	@ApiModelProperty(value="库存数量")
	private int stockNum;

    @ApiModelProperty(value="单位")
    private String unit;

}
