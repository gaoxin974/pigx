package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "出库单查询PageVO")
public class OutStoragePageVO {

	@ApiModelProperty(value="原始出库单号")
	private String outPageNo;

	@ApiModelProperty(value="wms出库单号")
	private String wmsBillNo;

	@ApiModelProperty(value="出库状态")
	private String status;

	@ApiModelProperty(value="波次单号")
	private String packageNo;

	@ApiModelProperty(value="波次状态")
	private String packageStatus;

	@ApiModelProperty(value="出库类型")
	private String outType;

	@ApiModelProperty(value="车辆信息")
	private String vehicleInformation;

	@ApiModelProperty(value="出库位置")
	private String outLocationNO;

	@ApiModelProperty(value="创建时间")
	private String createTime;

//	@ApiModelProperty(value="sku")
//	private String sku;
//
//	@ApiModelProperty(value="托盘号")
//	private String trayNo;

}
