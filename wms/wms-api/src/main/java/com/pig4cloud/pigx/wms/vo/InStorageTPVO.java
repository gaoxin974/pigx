package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "入库单TP查询VO")
public class InStorageTPVO {

	@ApiModelProperty(value="托盘号")
	private String uniqueCode;

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="批次号")
	private String itemId;

	@ApiModelProperty(value="入库上架库位号")
	private String location;

	@ApiModelProperty(value="库区")
	private String areaName;

	@ApiModelProperty(value="货主")
	private String huoz;

	@ApiModelProperty(value="实际货主")
	private String realHuoz;

	@ApiModelProperty(value="操作员")
	private String operator;

	@ApiModelProperty(value="状态")
	private String status;

	@ApiModelProperty(value="上架数量")
	private int amount;

}
