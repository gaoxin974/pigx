package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/25 16:47
 */
@Data
@ApiModel(value = "PDA电商出库拣货单详情列表")
public class PDADSOutStorageJHVO {

	@ApiModelProperty(value="小车信息ID")
	private String cartInfoId;

	@ApiModelProperty(value="订单号")
	private String outPageNo;

	@ApiModelProperty(value="wms订单号")
	private String wmsBillNo;

	@ApiModelProperty(value="订单行号")
	private int outStorageItemId;

	@ApiModelProperty(value="子波次号")
	private String subPackageNo;

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="sku商品名称")
	private String skuName;

	@ApiModelProperty(value="拣货数量")
	private int amount;

	@ApiModelProperty(value="位置")
	private String location;

	@ApiModelProperty(value="状态")
	private String status;

	@ApiModelProperty(value="绿色标识")
	private boolean isGreen;

}
