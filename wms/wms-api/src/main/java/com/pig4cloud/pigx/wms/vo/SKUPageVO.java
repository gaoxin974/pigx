package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2022/7/8 13:35
 */
@Data
@ApiModel(value = "SKU查询PageVO")
public class SKUPageVO {

	@ApiModelProperty(value="SKU")
	private String sku;

	@ApiModelProperty(value="物流中心编码 外键")
	private String wlzxCode;

	@ApiModelProperty(value="货主ID 外键")
	private String huozId;

	@ApiModelProperty(value="商品名称")
	private String skuName;

	@ApiModelProperty(value="最小包装单位")
	private String unit;

}
