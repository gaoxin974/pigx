package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "盘点下发返回的信息")
public class WMSReturnInfo {

	@ApiModelProperty(value="是否成功")
	private boolean success;

	@ApiModelProperty(value="失败后返回的消息")
	private String message;

}
