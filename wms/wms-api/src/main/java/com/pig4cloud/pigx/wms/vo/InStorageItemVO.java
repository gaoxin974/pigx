package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "入库单item查询VO")
public class InStorageItemVO {

	@ApiModelProperty(value="入库单主键")
	private String inPageNo;

	@ApiModelProperty(value="sku名称")
	private String skuName;

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="inbillnumber")
	private String inBillNumber;

	@ApiModelProperty(value="批号")
	private String batchNo;

	@ApiModelProperty(value="入库数量")
	private int amount;

	@ApiModelProperty(value="实际入库数量")
	private String realAmount;

	@ApiModelProperty(value="最小包装单位")
	private String unit;


}
