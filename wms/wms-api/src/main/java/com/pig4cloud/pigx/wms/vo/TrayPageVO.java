package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "托盘查询PageVO")
public class TrayPageVO {

	@ApiModelProperty(value="托盘编号")
	private String trayNo;

	@ApiModelProperty(value="状态")
	private String status;


}
