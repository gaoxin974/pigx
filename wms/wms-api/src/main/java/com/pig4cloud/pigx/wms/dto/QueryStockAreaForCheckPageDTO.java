package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "盘点库区查询DTO")
public class QueryStockAreaForCheckPageDTO {

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="批次")
	private String inBillNumber;

	@ApiModelProperty(value="批号")
	private String batchNo;

	@ApiModelProperty(value="托盘编号")
	private String trayNo;

}
