/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "ERP出库单行项目托盘信息")
public class ERPOutStorageTpDTO {
private static final long serialVersionUID = 1L;

	@ApiModelProperty(value="sku")
	private String sku;

    @ApiModelProperty(value="托盘编号")
    private String uniqueCode;

    @ApiModelProperty(value="批次号")
    private String itemId;

    @ApiModelProperty(value="上架库位编号")
    private String location;

	@ApiModelProperty(value="托盘入库数量")
	private Integer amount;

}
