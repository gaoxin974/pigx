package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "ERP取消出入库单接口DTO")
public class ERPCancelDTO {

	@ApiModelProperty(value="立体库；AGV库")
	private String storageId;

	@ApiModelProperty(value="1001福贝  1002福益")
	private String huozId;

	@ApiModelProperty(value="物流中心ID：如6001")
	private String wlzxCode;

	@ApiModelProperty(value="取消单号:ERP单号")
	private String pageNo;

	@ApiModelProperty(value="1.入库上架 2.出库下架")
	private String document;

	@ApiModelProperty(value="出入库时间")
	private String date;

	@ApiModelProperty(value="备注")
	private String comment;


}
