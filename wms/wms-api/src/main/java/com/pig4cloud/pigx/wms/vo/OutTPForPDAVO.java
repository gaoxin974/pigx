/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.vo;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 出库单托盘详情forPDA
 *
 * @author gaoxin
 * @date 2021-04-08 15:34:28
 */
@Data
@ApiModel(value = "出库单托盘详情forPDA")
public class OutTPForPDAVO {
private static final long serialVersionUID = 1L;


    @ApiModelProperty(value="sku")
    private String sku;

	@ApiModelProperty(value="sku名称")
	private String skuName;

	@ApiModelProperty(value="托盘编码")
	private String uniqueCode;

	@ApiModelProperty(value="库存")
	private int stockNo;

	@ApiModelProperty(value="实际出库数量")
	private int realAmount;

	@ApiModelProperty(value="应该出库数量")
	private int mustAmount;

	@ApiModelProperty(value="上架时间")
	private LocalDateTime inTime;

	@ApiModelProperty(value="批次")
	private String batchCi;

	@ApiModelProperty(value="批号")
	private String batchNo;

	@ApiModelProperty(value="生产日期")
	private LocalDateTime prdTime;

	@ApiModelProperty(value="有效期")
	private LocalDateTime effTime;

	@ApiModelProperty(value="货车信息")
	private String carInf;

	@ApiModelProperty(value="出库位置")
	private String location;


}
