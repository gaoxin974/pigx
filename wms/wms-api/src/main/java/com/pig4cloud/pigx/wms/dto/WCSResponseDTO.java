package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "WCS请求响应DTO")
public class WCSResponseDTO {

	@ApiModelProperty(value="错误代码")
	private String errorCode;

	@ApiModelProperty(value="错误信息")
	private String errorMsg;

	@ApiModelProperty(value="请求返回数据")
	private String resultData;

}
