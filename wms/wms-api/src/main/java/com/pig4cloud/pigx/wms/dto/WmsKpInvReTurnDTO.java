package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "WCS空托盘申请出库接口返回DTO")
public class WmsKpInvReTurnDTO {

	@ApiModelProperty(value="任务ID号")
	private String taskId;

	@ApiModelProperty(value="空托出库货位")
	private String outLoc;

	@ApiModelProperty(value="空盘缓存位置编号")
	private String kpDevId;

	@ApiModelProperty(value="备注说明信息1")
	private String memoInfo1;

}
