/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 出库单TP详情
 *
 * @author gaoxin
 * @date 2021-07-14 15:34:28
 */
@Data
@ApiModel(value = "出库单TP详情VO")
public class OutTPDetailForVO {
private static final long serialVersionUID = 1L;

    /**
     * 托盘编号
     */
    @ApiModelProperty(value="托盘编号")
    private String tpNo;
	/**
	 * 库存数量
	 */
	@ApiModelProperty(value="sku")
	private int stockNo;
	/**
	 * 单位
	 */
	@ApiModelProperty(value="单位")
	private String unit;
	/**
	 * 实际出库数量
	 */
	@ApiModelProperty(value="实际出库数量")
	private int realAmount;
    /**
	 * 托盘状态
	 */
	@ApiModelProperty(value="托盘状态")
	private String status;
	/**
	 * 操作员
	 */
	@ApiModelProperty(value="operator")
	private String operator;

}
