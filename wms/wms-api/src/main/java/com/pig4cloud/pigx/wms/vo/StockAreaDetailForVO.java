package com.pig4cloud.pigx.wms.vo;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "库区信息详情")
public class StockAreaDetailForVO {

private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="库区编号主键")
    private String stockAreaNo;

	@ApiModelProperty(value="sku")
	private String sku;

    @ApiModelProperty(value="入库批次")
    private String inBillNumber;

	@ApiModelProperty(value="使用状态 0：空库区 1：使用中 2：满库区")
	private String status;

    @ApiModelProperty(value="库区锁定状态:0：未锁定状态 1锁定状态")
    private String lockStatus;

	@ApiModelProperty(value="库区里库位信息")
	private List<SLocationForStockAreaDetailVO> sLocationForStockAreaDetailVOS;
}
