package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "WCS入库请求DTO")
public class WCSInvInDTO {

	@ApiModelProperty(value="任务ID,UUID型")
	private String taskId;

	@ApiModelProperty(value="任务类型")
	private String taskSource;

	@ApiModelProperty(value="入库位置 任务发起位置A")
	private String locatorFrom;

	@ApiModelProperty(value="托盘号")
	private String stockNo;

	@ApiModelProperty(value="入库库位B")
	private String Inloc;

	@ApiModelProperty(value="备注说明信息1")
	private String memoInfo1;

	@ApiModelProperty(value="备注说明信息2")
	private String memoInfo2;

}
