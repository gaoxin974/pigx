package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@ApiModel(value = "获取备货中备货完成出库单信息forPDA")
public class BHOutForPDAVO {
private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="出库单id")
    private String outPageNo;

	@ApiModelProperty(value="出库单状态")
	private String status;

	@ApiModelProperty(value="出库单商品和托盘信息")
	private List<BHOutDetailForPDAVO> skuTps;

}
