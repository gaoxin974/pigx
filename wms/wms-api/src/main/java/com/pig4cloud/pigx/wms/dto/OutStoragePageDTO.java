package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "出库单查询DTO")
public class OutStoragePageDTO {

	@ApiModelProperty(value="原始出库单号")
	private String outPageNo;

	@ApiModelProperty(value="wms出库单号")
	private String wmsBillNo;

	@ApiModelProperty(value="出库类型")
	private String outType;

	@ApiModelProperty(value="状态")
	private String status;

	@ApiModelProperty(value="是否是单品规 1是 0多品规")
	private String isOneItem;

	@ApiModelProperty(value="波次单状态")
	private String packageStatus;

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="托盘号")
	private String trayNo;


}
