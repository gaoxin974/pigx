package com.pig4cloud.pigx.wms.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "库区VO")
public class StockAreaForVO {
private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="库区使用状态 0：空库区 1：使用中 2：满库区")
    private String status;

	@ApiModelProperty(value="库区层级")
	private int tier;

	@ApiModelProperty(value="对应状态库区总数")
	private int statusTotal;

	@ApiModelProperty(value="比例")
	private float statusRatio;

	@ApiModelProperty(value="锁定状态库区总数")
	private int lockedTotal;

	@ApiModelProperty(value="锁定状态库区比例")
	private float lockedRatio;

	@ApiModelProperty(value="未锁定状态库区总数")
	private int unLockTotal;

	@ApiModelProperty(value="未锁定状态库区比例")
	private float unLockRatio;

}
