package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "WCS空托盘申请入库接口DTO")
public class WmsDpInvDTO {

	@ApiModelProperty(value="任务ID号")
	private String taskId;

	@ApiModelProperty(value="叠盘机编号")
	private String dpDevId;

	@ApiModelProperty(value="备注说明信息1")
	private String memoInfo1;

	@ApiModelProperty(value="备注说明信息1")
	private String memoInfo2;

}
