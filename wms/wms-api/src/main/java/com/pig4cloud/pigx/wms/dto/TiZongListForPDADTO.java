package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "电商提总列表DTOForPDA")
public class TiZongListForPDADTO {

	@ApiModelProperty(value="子波次单号")
	private String subPackageNo;

	@ApiModelProperty(value="提总区域")
	private String lLocation;

	@ApiModelProperty(value="商品名称")
	private String skuName;

	@ApiModelProperty(value="托盘编号")
	private String trayNo;

	@ApiModelProperty(value="状态")
	private String status;

}
