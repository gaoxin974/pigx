/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.vo;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 波次信息
 *
 * @author gaoxin
 * @date 2021-04-08 15:34:28
 */
@Data
@ApiModel(value = "原始出库单和出库单VO")
public class OutAndOOutForPackage {
private static final long serialVersionUID = 1L;

    /**
     * 原始出库单号
     */
    @ApiModelProperty(value="原始出库单号")
    private String outPageNo;
	/**
	 * wms出库单号
	 */
	@ApiModelProperty(value="wms出库单号")
	private String wmsBillNo;
	/**
	 * wms出库单状态
	 */
	@ApiModelProperty(value="wms出库单状态")
	private String status;

}
