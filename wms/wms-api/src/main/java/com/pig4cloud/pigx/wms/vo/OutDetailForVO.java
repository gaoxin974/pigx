/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 出库单item详情
 *
 * @author gaoxin
 * @date 2021-07-14 15:34:28
 */
@Data
@ApiModel(value = "出库单item详情VO")
public class OutDetailForVO {
private static final long serialVersionUID = 1L;

    /**
     * 出库单itemid
     */
    @ApiModelProperty(value="出库单outItemId")
    private int outItemId;
	/**
	 * sku
	 */
	@ApiModelProperty(value="sku")
	private String sku;
	/**
	 * skuName
	 */
	@ApiModelProperty(value="skuName")
	private String skuName;
    /**
	 * 出库数量
	 */
	@ApiModelProperty(value="出库数量")
	private int amount;
	/**
	 * 单位
	 */
	@ApiModelProperty(value="unit")
	private String unit;

	@ApiModelProperty(value="批号")
	private String batchNo;

}
