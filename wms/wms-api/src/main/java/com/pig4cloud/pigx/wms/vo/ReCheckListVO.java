package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "复盘List")
public class ReCheckListVO {

	@ApiModelProperty(value="库区编号")
	private String stockAreaNo;

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="批号")
	private String batchNo;

	@ApiModelProperty(value="批次")
	private String inBillNumber;

}
