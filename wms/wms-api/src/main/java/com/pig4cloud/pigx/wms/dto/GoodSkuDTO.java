package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "畅销skuDTO")
public class GoodSkuDTO {

	@ApiModelProperty(value="商品名称")
	private String skuName;


	@ApiModelProperty(value="状态")
	private String status;


}
