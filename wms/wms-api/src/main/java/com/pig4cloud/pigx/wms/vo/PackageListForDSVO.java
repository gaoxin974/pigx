package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "PDA使用电商波次列表VO")
public class PackageListForDSVO {

	@ApiModelProperty(value="波次单号")
	private String packageNo;

	@ApiModelProperty(value="波次单状态")
	private String packageStatus;

}
