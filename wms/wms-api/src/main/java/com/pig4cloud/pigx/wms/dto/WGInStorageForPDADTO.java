package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "PDA提交外购入库请求")
public class WGInStorageForPDADTO {

	@ApiModelProperty(value="wms入库单号")
	private String wmsBillNo;

	@ApiModelProperty(value="wms入库单商品行号")
	private String inStorageItemId;

	@ApiModelProperty(value="托盘号")
	private String uniqueCode;

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="开始库位编号")
	private String fromLocation;

	@ApiModelProperty(value="实际入库数量")
	private Integer amount;


}
