package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "WCS回调参数实体")
public class WcsWmsCallBackDTO {

	@ApiModelProperty(value="wms交互编号")
	private String wmsTrxId;

	@ApiModelProperty(value="返回状态码")
	private Integer statusCode;

}
