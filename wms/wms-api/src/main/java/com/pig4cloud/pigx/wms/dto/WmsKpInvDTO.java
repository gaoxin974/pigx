package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "WCS空托盘申请出库接口DTO")
public class WmsKpInvDTO {

	@ApiModelProperty(value="任务ID号")
	private String taskId;

	@ApiModelProperty(value="产线缓存空托编号,托盘出库目的地")
	private String kpDevId;

	@ApiModelProperty(value="备注说明信息1")
	private String memoInfo1;

	@ApiModelProperty(value="备注说明信息1")
	private String memoInfo2;

}
