package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel(value = "库区forcheckDTO")
public class StockAreaForCheckDTO {

	@ApiModelProperty(value="库区编号")
	private String stockAreaNo;

}
