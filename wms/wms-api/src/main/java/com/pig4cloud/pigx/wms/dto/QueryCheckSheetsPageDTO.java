package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "盘点库区查询DTO")
public class QueryCheckSheetsPageDTO {

	@ApiModelProperty(value="盘点单号")
	private String checkSheetNo;

	@ApiModelProperty(value="盘点状态")
	private String checkStatus;

}
