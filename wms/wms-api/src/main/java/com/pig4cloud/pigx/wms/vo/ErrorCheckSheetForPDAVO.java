package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "盘点异常托盘forPDA")
public class ErrorCheckSheetForPDAVO {

	@ApiModelProperty(value="异常盘点详情ID")
	private int checkSheetItemId;

	@ApiModelProperty(value="托盘编号")
	private String trayNo;

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="盘点单号")
	private String checkSheetNo;

	@ApiModelProperty(value="sku")
	private String checkResult;

}
