package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "盘点详情forPDA")
public class CheckSheetDetailForPDAVO {

	@ApiModelProperty(value="盘点单itemid")
	private int checkSheetItemId;

	@ApiModelProperty(value="原始商品名称")
	private String oriSkuName;

	@ApiModelProperty(value="原始商品编号")
	private String oriSku;

	@ApiModelProperty(value="原始托盘编号")
	private String oriTrayNo;

	@ApiModelProperty(value="原始批号")
	private String oriBatchNo;

	@ApiModelProperty(value="原始库存量")
	private int oriStockNum;

	@ApiModelProperty(value="实际商品名称")
	private String realSkuName;

	@ApiModelProperty(value="实际商品编号")
	private String realSku;

	@ApiModelProperty(value="实际托盘编号")
	private String realTrayNo;

	@ApiModelProperty(value="实际批号")
	private String realBatchNo;

	@ApiModelProperty(value="原始库存量")
	private int realStockNum;

}
