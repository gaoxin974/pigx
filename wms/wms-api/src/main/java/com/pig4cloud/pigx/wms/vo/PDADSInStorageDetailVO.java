package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/25 16:47
 */
@Data
@ApiModel(value = "PDA电商入库单详情查询")
public class PDADSInStorageDetailVO {

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="sku商品名称")
	private String skuName;

	@ApiModelProperty(value="托盘编码")
	private String uniqueCode;

	@ApiModelProperty(value="实际入库量")
	private int amount;

	@ApiModelProperty(value="上架时间")
	private String inDate;

	@ApiModelProperty(value="批号")
	private String batchNo;

	@ApiModelProperty(value="生产日期")
	private String productDate;

	@ApiModelProperty(value="有效期")
	private String effDate;

	@ApiModelProperty(value="0:未上架 1:正在上架 2:已完成")
	private String status;

}
