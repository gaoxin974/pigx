package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "WCS出库请求DTO")
public class WCSInvOutDTO {

	@ApiModelProperty(value="任务ID,UUID型")
	private String taskId;

	@ApiModelProperty(value="出库类型")
	private String invOutType;

	@ApiModelProperty(value="出库货位A")
	private String outLoc;

	@ApiModelProperty(value="出库目的地B")
	private String locDesti;

	@ApiModelProperty(value="托盘编号")
	private String stockNo;

	@ApiModelProperty(value="备注说明信息1")
	private String memoInfo1;

	@ApiModelProperty(value="备注说明信息2")
	private String memoInfo2;

}
