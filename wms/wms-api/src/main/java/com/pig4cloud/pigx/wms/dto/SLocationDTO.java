package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "库位查询DTO")
public class SLocationDTO {

	@ApiModelProperty(value="库位编号")
	private String sLocationNo;

	@ApiModelProperty(value="区域ID")
	private String areaId;

	@ApiModelProperty(value="货主ID")
	private String huozId;

	@ApiModelProperty(value="状态")
	private String status;


}
