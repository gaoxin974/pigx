package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "ERP下发返回DTO")
public class ERPSystemReturnDTO {

	@ApiModelProperty(value="是否成功")
	private boolean success;

	@ApiModelProperty(value="失败后返回的消息")
	private String message;

}
