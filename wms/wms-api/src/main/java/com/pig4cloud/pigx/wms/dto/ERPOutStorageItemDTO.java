/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 出库单行项目信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:36
 */
@Data
@ApiModel(value = "出库单行项目信息")
public class ERPOutStorageItemDTO {
private static final long serialVersionUID = 1L;


	@ApiModelProperty(value="行编号")
	private String lineNumber;

    @ApiModelProperty(value="sku")
    private String sku;

    @ApiModelProperty(value="批号")
    private String batchNo;

    @ApiModelProperty(value="出库数量")
    private Integer amount;

    @ApiModelProperty(value="最小单位")
    private String minUnitName;

	@ApiModelProperty(value="托盘信息")
	private List<ERPOutStorageTpDTO> erpOutStorageTpDTOList;

    @ApiModelProperty(value="扩展字段1")
    private String sckext1;

    @ApiModelProperty(value="扩展字段2")
    private String sckext2;

    @ApiModelProperty(value="扩展字段3")
    private String sckext3;

    @ApiModelProperty(value="扩展字段4")
    private String sckext4;

    @ApiModelProperty(value="扩展字段5")
    private String sckext5;

    @ApiModelProperty(value="扩展字段6")
    private String sckext6;

    @ApiModelProperty(value="扩展字段7")
    private String sckext7;

	@ApiModelProperty(value="扩展字段8")
	private String sckext8;

    }
