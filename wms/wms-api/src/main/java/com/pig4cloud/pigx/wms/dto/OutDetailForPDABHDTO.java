package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "出库单提总补货托盘详情提交DTO")
public class OutDetailForPDABHDTO {

	@ApiModelProperty(value="子波次号")
	private String subPackageNo;

	@ApiModelProperty(value="托盘编号")
	private String tpNo;

	@ApiModelProperty(value="提总出库位置")
	private String TZlocation;

}
