package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "入库单查询DTO")
public class InStoragePageDTO {

	@ApiModelProperty(value="原始单号")
	private String inPageNo;

	@ApiModelProperty(value="wms入库单号")
	private String wmsBillNo;

	@ApiModelProperty(value="入库类型")
	private String inType;

	@ApiModelProperty(value="状态")
	private String status;

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="托盘号")
	private String trayNo;

}
