package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2023/5/21 13:35
 */
@Data
@ApiModel(value = "WCS出库请求DTO")
public class WCSEmptyCtlDTO {

	@ApiModelProperty(value="任务ID,UUID型")
	private String taskId;

	@ApiModelProperty(value="托盘编号")
	private String stockNo;

}
