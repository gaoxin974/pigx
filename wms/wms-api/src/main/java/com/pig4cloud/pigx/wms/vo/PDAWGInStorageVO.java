package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/25 16:47
 */
@Data
@ApiModel(value = "PDA外购入库单查询")
public class PDAWGInStorageVO {

	@ApiModelProperty(value="wms入库单号")
	private String wmsBillNo;

	@ApiModelProperty(value="入库单商品行ID")
	private String inStorageItemId;

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="sku名称")
	private String skuName;

	@ApiModelProperty(value="单位")
	private String unit;

	@ApiModelProperty(value="预计入库数量")
	private Integer amount;

}
