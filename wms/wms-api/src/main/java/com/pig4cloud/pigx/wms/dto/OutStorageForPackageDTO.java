package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "创建波次DTO")
public class OutStorageForPackageDTO {

	@ApiModelProperty(value="出库单编号")
	private String outStorageId;

	@ApiModelProperty(value="出库单状态")
	private String status;

	@ApiModelProperty(value="出库单类型")
	private String outType;

}
