/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.vo;

import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 出库单详情
 *
 * @author gaoxin
 * @date 2021-07-14 15:34:28
 */
@Data
@ApiModel(value = "出库单详情VO")
public class OutItemForVO {
private static final long serialVersionUID = 1L;

    /**
     * 原始订单号
     */
    @ApiModelProperty(value="原始订单号")
    private String outPageNo;
	/**
	 * wms出库单号
	 */
	@ApiModelProperty(value="wms出库单号")
	private String wmsBillNo;
	/**
	 * 出库单波次号
	 */
	@ApiModelProperty(value="出库单波次号")
	private String packageNo;
    /**
	 * 车辆信息
	 */
	@ApiModelProperty(value="车辆信息")
	private String vehicleInformation;
	/**
	 * 出库位置
	 */
	@ApiModelProperty(value="出库位置")
	private String locationNo;

	/**
	 * 出库单行项目托盘信息
	 */
	@ApiModelProperty(value="出库单行项目托盘信息")
	private Map<OutDetailForVO, List<OutTPDetailForVO>> outTps;

}
