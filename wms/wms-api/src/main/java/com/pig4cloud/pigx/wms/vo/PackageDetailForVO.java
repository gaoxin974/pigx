/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.vo;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 波次信息详情
 *
 * @author gaoxin
 * @date 2021-04-08 15:34:28
 */
@Data
@ApiModel(value = "波次详情VO")
public class PackageDetailForVO {
private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="波次单号")
    private String packageNo;

	@ApiModelProperty(value="波次单状态")
	private String packageStatus;

	@ApiModelProperty(value="波次单状态")
	private List<SubPackageForVO> subPackageForVOS;

    @ApiModelProperty(value="原始单和出库单列表")
    private List<OutAndOOutForPackage> outAndOOutForPackages;

}
