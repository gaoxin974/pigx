/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.dto;

import java.time.LocalDateTime;
import java.util.List;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 入库单信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:32
 */
@Data
@ApiModel(value = "ERP入库单实体")
public class ERPOutStorageDTO {
private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="入库单号 主键")
    private String outPageNo;

	@ApiModelProperty(value="ERP上游出库单号")
	private String outBillNumber;

    @ApiModelProperty(value="库区ID")
    private Integer storageId;

    @ApiModelProperty(value="货主ID")
    private String huozId;

    @ApiModelProperty(value="物流中心ID")
    private String wlzxCode;

    @ApiModelProperty(value="车辆信息编码：20******000000001\n"
			+ "例：2021030500000001（2B出库 、2大C出库)")
    private String vehicleInformation;

	@ApiModelProperty(value="车牌照号")
	private String licenceNum;

	@ApiModelProperty(value="司机姓名")
	private String driverName;

    @ApiModelProperty(value="出库时间")
//	@JsonFormat(shape = JsonFormat.Shape.NUMBER)
//	@JsonFormat(shape = JsonFormat.Shape.STRING,pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private String outDate;

	@ApiModelProperty(value="站台编号:出库位置")
	private String locationNo;

    @ApiModelProperty(value="1ERP产线出库 2电商出库 3T2C出库")
    private String outType;

	@ApiModelProperty(value="0未完成1已完成")
	private String orderStatus;

	@ApiModelProperty(value="出库单行项目信息")
	private List<ERPOutStorageItemDTO> erpOutStorageItemDTOList;

}
