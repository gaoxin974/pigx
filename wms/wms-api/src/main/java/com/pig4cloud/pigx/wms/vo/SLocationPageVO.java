package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/15 13:35
 */
@Data
@ApiModel(value = "库位查询PageVO")
public class SLocationPageVO {

	@ApiModelProperty(value="库位编号")
	private String sLocationNo;

	@ApiModelProperty(value="物理编号")
	private String wNo;

	@ApiModelProperty(value="优先级序号")
	private String sOrder;

	@ApiModelProperty(value="所在区域名称")
	private String areaName;

	@ApiModelProperty(value="真正货主id")
	private String realHuozId;

	@ApiModelProperty(value="状态")
	private String status;


}
