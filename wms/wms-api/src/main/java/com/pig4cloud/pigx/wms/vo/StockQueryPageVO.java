package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "库存查询PageVO")
public class StockQueryPageVO {

	@ApiModelProperty(value="库位编号")
	private String locationNo;

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="商品名称")
	private String skuName;

	@ApiModelProperty(value="托盘号")
	private String trayNo;

	@ApiModelProperty(value="批号")
	private String batchNo;

	@ApiModelProperty(value="入库批次")
	private String inBillNo;

	@ApiModelProperty(value="库存数量")
	private String stockAmount;

	@ApiModelProperty(value="出库数量")
	private String outAmount;

	@ApiModelProperty(value="生产日期")
	private String proDate;

	@ApiModelProperty(value="失效日期")
	private String expDate;

	@ApiModelProperty(value="wNo")
	private String wNo;

	@ApiModelProperty(value="货主id")
	private String hzId;

	@ApiModelProperty(value="库区编号")
	private String stockAreaNo;

	@ApiModelProperty(value="可用状态")
	private String status;

	@ApiModelProperty(value="库区锁定状态")
	private String areaLockStatus;

}
