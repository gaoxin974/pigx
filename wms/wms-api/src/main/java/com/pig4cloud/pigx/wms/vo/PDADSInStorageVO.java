package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2021/4/25 16:47
 */
@Data
@ApiModel(value = "PDA入库单查询")
public class PDADSInStorageVO {

	@ApiModelProperty(value="wms入库单号")
	private String wmsBillNo;

	@ApiModelProperty(value="托盘编码")
	private String uniqueCode;

	@ApiModelProperty(value="sku名称")
	private String skuName;

	@ApiModelProperty(value="入库单状态")
	private String status;

}
