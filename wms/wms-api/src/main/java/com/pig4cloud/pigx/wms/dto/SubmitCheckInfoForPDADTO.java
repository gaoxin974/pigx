package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "盘点详情提交DTO")
public class SubmitCheckInfoForPDADTO {

	@ApiModelProperty(value="盘点行项目编号")
	private String checkSheetItemId;

	@ApiModelProperty(value="实际skuname")
	private String realSkuName;

	@ApiModelProperty(value="实际sku")
	private String realSku;

	@ApiModelProperty(value="实际batchNo")
	private String realBatchNo;

	@ApiModelProperty(value="实际库存")
	private String realStockNum;

}
