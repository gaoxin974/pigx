package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "库存查询DTO")
public class StockQueryDTO {

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="库位编号")
	private String locationNo;

	@ApiModelProperty(value="托盘编号")
	private String trayNo;

	@ApiModelProperty(value="入库批次")
	private String inBillNo;

	@ApiModelProperty(value="批号")
	private String batchNo;

	@ApiModelProperty(value="库区编号")
	private String stockAreaNo;

}
