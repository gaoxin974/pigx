package com.pig4cloud.pigx.wms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: 高欣
 * @Date: 2023/2/10 13:35
 */
@Data
@ApiModel(value = "出库单托盘详情提交和回库DTO")
public class OutDetailAndBackForPDADTO {

	@ApiModelProperty(value="托盘编号")
	private String tpNo;

	@ApiModelProperty(value="fromLocation")
	private String fromLocation;

}
