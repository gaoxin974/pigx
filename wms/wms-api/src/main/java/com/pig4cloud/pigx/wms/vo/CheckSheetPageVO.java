package com.pig4cloud.pigx.wms.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;


@Data
@ApiModel(value = "盘点单查询PageVO")
public class CheckSheetPageVO {

	@ApiModelProperty(value="盘点单号")
	private String checkSheetNo;

	@ApiModelProperty(value="盘点位置")
	private String checkLocation;

	@ApiModelProperty(value="盘点状态")
	private String checkStatus;

	@ApiModelProperty(value="盘点结果")
	private String checkResult;

	@ApiModelProperty(value="创建时间")
	private LocalDateTime createTime;

}
