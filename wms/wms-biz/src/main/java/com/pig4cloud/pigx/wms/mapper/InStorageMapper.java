/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.data.datascope.PigxBaseMapper;
import com.pig4cloud.pigx.wms.dto.InStoragePageDTO;
import com.pig4cloud.pigx.wms.entity.InStorage;
import com.pig4cloud.pigx.wms.vo.InStorageItemVO;
import com.pig4cloud.pigx.wms.vo.InStoragePageVO;
import com.pig4cloud.pigx.wms.vo.InStorageTPVO;
import com.pig4cloud.pigx.wms.vo.PDADSInStorageDetailVO;
import com.pig4cloud.pigx.wms.vo.PDADSInStorageVO;
import com.pig4cloud.pigx.wms.vo.PDAWGInStorageDetailVO;
import com.pig4cloud.pigx.wms.vo.PDAWGInStorageVO;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 入库单信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:32
 */
@Mapper
public interface InStorageMapper extends PigxBaseMapper<InStorage> {

	IPage<PDADSInStorageVO> pageDSInStorageForPDA(Page page, @Param("sort")String sort);

	PDADSInStorageDetailVO getDSInStorageDetailByTPForPDA(@Param("uniqueCode")String uniqueCode);

	IPage<PDAWGInStorageVO> pageWGInStorageForPDA(Page page, @Param("wmsBillNo") String wmsBillNo,
												  @Param("sort") String sort);

	IPage<InStoragePageVO> getInStoragePageForVO(Page page,
												 @Param("inStoragePageDTO") InStoragePageDTO inStoragePageDTO);

	List<InStorageItemVO> listItemForVO(@Param("wmsBillNo") String wmsBillNo);

	List<InStorageTPVO> listInStorageTP(@Param("wmsBillNo") String wmsBillNo);

	List<PDAWGInStorageDetailVO> pageWGInStorageDetailForPDA(@Param("userName") String userName,
															  @Param("wmsBillNo") String wmsBillNo,
															  @Param("inStorageItemId") String inStorageItemId);

	PDADSInStorageDetailVO getWGInStorageDetailForPDA(@Param("wmsBillNo")String wmsBillNo, @Param("inStorageItemId")String inStorageItemId);
}
