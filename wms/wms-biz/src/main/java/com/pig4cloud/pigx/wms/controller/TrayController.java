/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.wms.dto.TrayDTO;
import com.pig4cloud.pigx.wms.entity.Tray;
import com.pig4cloud.pigx.wms.service.TrayService;
import com.pig4cloud.pigx.wms.vo.TrayPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * 托盘信息
 *
 * @author gaoxin
 * @date 2021-04-08 09:31:32
 */
@RestController
@AllArgsConstructor
@RequestMapping("/tray")
@Api(value = "tray", tags = "托盘信息管理")
public class TrayController {

	private final TrayService trayService;

	/**
	 * 分页查询
	 *
	 * @param page 分页对象
	 * @param tray 托盘信息
	 *
	 * @return
	 */
	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/page")
	//@PreAuthorize("@pms.hasPermission('wms_tray_view')")
	public R getTrayPage(Page page, Tray tray) {
		return R.ok(trayService.page(page, Wrappers.query(tray)));
	}

	/**
	 * 分页查询ForVO
	 * @param page 分页对象
	 * @param trayDTO
	 * @return
	 */
	@ApiOperation(value = "分页查询ForVO", notes = "分页查询ForVO")
	@GetMapping("/pageForVO" )
	//@PreAuthorize("@pms.hasPermission('wms_tray_view')" )
	public R pageTrayForVO(Page page, TrayDTO trayDTO) {

		IPage<TrayPageVO> trayPageVOPage = trayService.pageTrayForVO(page, trayDTO);

		return R.ok(trayPageVOPage);
	}

	/**
	 * 通过id查询托盘信息
	 *
	 * @param trayNo id
	 *
	 * @return R
	 */
	@ApiOperation(value = "通过id查询", notes = "通过id查询")
	@GetMapping("/{trayNo}")
	//@PreAuthorize("@pms.hasPermission('wms_tray_view')")
	public R getById(@PathVariable("trayNo") String trayNo) {
		return R.ok(trayService.getById(trayNo));
	}

	/**
	 * 新增托盘信息
	 *
	 * @param tray 托盘信息
	 *
	 * @return R
	 */
	@ApiOperation(value = "新增托盘信息", notes = "新增托盘信息")
	@SysLog("新增托盘信息")
	@PostMapping
	//@PreAuthorize("@pms.hasPermission('wms_tray_add')")
	public R save(@RequestBody Tray tray) {
		return R.ok(trayService.save(tray));
	}

	/**
	 * 修改托盘信息
	 *
	 * @param tray 托盘信息
	 *
	 * @return R
	 */
	@ApiOperation(value = "修改托盘信息", notes = "修改托盘信息")
	@SysLog("修改托盘信息")
	@PutMapping
	//@PreAuthorize("@pms.hasPermission('wms_tray_edit')")
	public R updateById(@RequestBody Tray tray) {
		return R.ok(trayService.updateById(tray));
	}

	/**
	 * 通过id删除托盘信息
	 *
	 * @param trayNo id
	 *
	 * @return R
	 */
	@ApiOperation(value = "通过id删除托盘信息", notes = "通过id删除托盘信息")
	@SysLog("通过id删除托盘信息")
	@DeleteMapping("/{trayNo}")
	//@PreAuthorize("@pms.hasPermission('wms_tray_del')")
	public R removeById(@PathVariable String trayNo) {
		return R.ok(trayService.removeById(trayNo));
	}

}
