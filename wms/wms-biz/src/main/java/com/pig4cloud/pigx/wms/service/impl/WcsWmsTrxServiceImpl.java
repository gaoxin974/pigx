package com.pig4cloud.pigx.wms.service.impl;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.wms.dto.*;
import com.pig4cloud.pigx.wms.entity.*;
import com.pig4cloud.pigx.wms.mapper.WcsWmsTrxMapper;
import com.pig4cloud.pigx.wms.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import cn.hutool.core.date.DatePattern;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * wcs和wms交互记录
 *
 * @author gaoxin
 * @date 2021-04-15 09:50:40
 */
@Slf4j
@Service
@RefreshScope
@RequiredArgsConstructor
public class WcsWmsTrxServiceImpl extends ServiceImpl<WcsWmsTrxMapper, WcsWmsTrx> implements WcsWmsTrxService {

	private final StockAreaService stockAreaService;

	private final SLocationService sLocationService;

	private final InStorageService inStorageService;

	private final InStorageItemService inStorageItemService;

	private final InStorageTpService inStorageTpService;

	private final OutStorageService outStorageService;

	private final CheckSheetService checkSheetService;

	private final CheckSheetItemService checkSheetItemService;

	private final TraySkuService traySkuService;

	private final ERPCallBackService erpCallBackService;

	private final AgvInQueueService agvInQueueService;

	private final AgvOutQueueService agvOutQueueService;

	@Value("${wcs.invin.url}")
	private String invInUrl;

	@Value("${wcs.invout.url}")
	private String invOutUrl;

	@Value("${wcs.emptyctl.url}")
	private String emptyCtlUrl;

	@Override
	public boolean getTestWCS() {

		RestTemplate restTemplate = new RestTemplate();

		MultiValueMap<String, String> paramMap = new LinkedMultiValueMap() {
			{
				add("taskId", "gxtestrukutask0001");
				add("taskSource", "1");
				add("locatorFrom", "CX100001");
				add("InLoc", "A100004");
				add("stockNo", "TP202104080945");
				add("memoInfo1", "入库备注说明1");
				add("memoInfo2", "入库备注说明2");
			}
		};

		//封装请求头
		try {
			RequestEntity requestEntity = RequestEntity
					// 可以不填
					.post(new URI(""))
					// 参数传输类型 url编码格式 application/x-www-form-urlencoded
					.contentType(MediaType.APPLICATION_FORM_URLENCODED)
					.accept(MediaType.ALL).acceptCharset(StandardCharsets.UTF_8)
					.body(paramMap);
			//基于注解映射对应实体
			ResponseEntity<WCSResponseDTO> result = restTemplate.postForEntity("http://112.4.208.194:8801/api/WmsInvIn",
					requestEntity, WCSResponseDTO.class);
			WCSResponseDTO wcsResponseDTO = result.getBody();
			log.info(wcsResponseDTO.getErrorCode());
			log.info(wcsResponseDTO.getErrorMsg());
			log.info(result.toString());
		} catch (Exception e) {
			log.error("调用wcs接口出现异常:" + e.getMessage());
			e.printStackTrace();
			return false;
		}

		return false;
	}

	@Override
	public boolean handleWCSInvIn(WCSInvInDTO wcsInvInDTO) {

		RestTemplate restTemplate = new RestTemplate();

		MultiValueMap<String, String> paramMap = new LinkedMultiValueMap() {
			{
				add("taskId", wcsInvInDTO.getTaskId());
				add("taskSource", wcsInvInDTO.getTaskSource());
				add("locatorFrom", wcsInvInDTO.getLocatorFrom());
				add("InLoc", wcsInvInDTO.getInloc());
				add("stockNo", wcsInvInDTO.getStockNo());
				add("memoInfo1", wcsInvInDTO.getMemoInfo1());
				add("memoInfo2", wcsInvInDTO.getMemoInfo2());
			}
		};

		log.info("发送wcs入库请求的参数是:" + wcsInvInDTO);

		//封装请求头
		try {
			RequestEntity requestEntity = RequestEntity
					// 可以不填
					.post(new URI(""))
					// 参数传输类型 url编码格式 application/x-www-form-urlencoded
					.contentType(MediaType.APPLICATION_FORM_URLENCODED)
					.accept(MediaType.ALL).acceptCharset(StandardCharsets.UTF_8)
					.body(paramMap);
			//基于注解映射对应实体
			ResponseEntity<WCSResponseDTO> result = restTemplate.postForEntity(invInUrl,
					requestEntity, WCSResponseDTO.class);
			WCSResponseDTO wcsResponseDTO = result.getBody();

			log.info("WCS入库请求响应errorCode:" + wcsResponseDTO.getErrorCode());
			log.info("WCS入库请求响应errorMsg:" + wcsResponseDTO.getErrorMsg());

			if ("0".equals(wcsResponseDTO.getErrorCode())) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.error("调用wcs入库接口出现异常:" + e.getMessage());
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean handleWCSInvOut(WCSInvOutDTO wcsInvOutDTO) {
		RestTemplate restTemplate = new RestTemplate();

		MultiValueMap<String, String> paramMap = new LinkedMultiValueMap() {
			{
				add("taskId", wcsInvOutDTO.getTaskId());
				add("invOutType", wcsInvOutDTO.getInvOutType());
				add("OutLoc", wcsInvOutDTO.getOutLoc());
				add("Locdesti", wcsInvOutDTO.getLocDesti());
				add("stockNo", wcsInvOutDTO.getStockNo());
				add("memoInfo1", wcsInvOutDTO.getMemoInfo1());
				add("memoInfo2", wcsInvOutDTO.getMemoInfo2());
			}
		};

		log.info("发送wcs出库请求的参数是:" + wcsInvOutDTO);

		//封装请求头
		try {
			RequestEntity requestEntity = RequestEntity
					// 可以不填
					.post(new URI(""))
					// 参数传输类型 url编码格式 application/x-www-form-urlencoded
					.contentType(MediaType.APPLICATION_FORM_URLENCODED)
					.accept(MediaType.ALL).acceptCharset(StandardCharsets.UTF_8)
					.body(paramMap);
			//基于注解映射对应实体
			ResponseEntity<WCSResponseDTO> result = restTemplate.postForEntity(invOutUrl,
					requestEntity, WCSResponseDTO.class);
			WCSResponseDTO wcsResponseDTO = result.getBody();

			log.info("WCS出库请求响应errorCode:" + wcsResponseDTO.getErrorCode());
			log.info("WCS出库请求响应errorMsg:" + wcsResponseDTO.getErrorMsg());

			if ("0".equals(wcsResponseDTO.getErrorCode())) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.error("调用wcs出库接口出现异常:" + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public String putEmptyTrayLocation(WmsDpInvDTO wmsDpInvDTO) {

		StockArea stockArea = stockAreaService.getStockAreaForTrayIn();

		//获取启用的托盘区未使用的排序级别最高的库位编号
		SLocation sLocation =
				sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
						.eq(SLocation::getAreaId, 3).eq(SLocation::getIsUsed, 0)
						.eq(SLocation::getLocationStatus, "1")
						.eq(SLocation::getStockAreaNo, stockArea.getStockAreaNo())
						.orderByAsc(SLocation::getSOrder).last("limit 1"));

		if (sLocation == null) {
			return null;
		} else {

			log.info("托盘入库更新库位" + sLocation.getSLocationNo() + "为使用中状态");
			String sNo = sLocation.getSLocationNo();
			sLocation.setIsUsed("2");
			sLocation.setUpdateTime(LocalDateTime.now());
			sLocationService.updateById(sLocation);

			log.info("更新库区" + stockArea.getStockAreaNo() + "为使用中锁定状态");

			stockArea.setStatus("1");
			stockArea.setLockStatus("1");
			stockArea.setUpdateTime(LocalDateTime.now());
			stockAreaService.updateById(stockArea);

			WcsWmsTrx wcsWmsTrx = new WcsWmsTrx();
			wcsWmsTrx.setWmsTrxId(wmsDpInvDTO.getTaskId());
			wcsWmsTrx.setTrxType("1");
			wcsWmsTrx.setFromLocation(wmsDpInvDTO.getDpDevId());
			wcsWmsTrx.setToLocation(sNo);

			this.save(wcsWmsTrx);
			return sNo;
		}

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public String getEmptyTrayLocation(WmsKpInvDTO wmsKpInvDTO) {

		StockArea stockArea = stockAreaService.getStockAreaForTrayOut();

		//获取启用的托盘区已使用的已放置托盘的排序级别最高的库位编号
		SLocation sLocation =
				sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
						.eq(SLocation::getAreaId, 3)
						.eq(SLocation::getIsUsed, 1)
						.eq(SLocation::getLocationStatus, "2")
						.eq(SLocation::getStockAreaNo, stockArea.getStockAreaNo())
						.orderByDesc(SLocation::getSOrder).last("limit 1"));
		if (sLocation == null) {

			return null;

		} else {
			//获取1个产线入库工作区
//			SLocation sLocationWork =
//					sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
//							.eq(SLocation::getAreaId, 4)
//							.eq(SLocation::getIsUsed, 0)
//							.eq(SLocation::getLocationStatus,"1")
//							.orderByAsc(SLocation::getSOrder).last("limit 1"));

//
//			String to = sLocationWork.getSLocationNo();
			String from = sLocation.getSLocationNo();

			log.info("托盘出库更新库位" + sLocation.getSLocationNo() + "为使用中状态");
			sLocation.setIsUsed("2");
			sLocation.setUpdateTime(LocalDateTime.now());
			sLocationService.updateById(sLocation);

			log.info("更新库区" + stockArea.getStockAreaNo() + "为锁定状态");
			stockArea.setStatus("1");
			stockArea.setLockStatus("1");
			stockArea.setUpdateTime(LocalDateTime.now());
			stockAreaService.updateById(stockArea);

			WcsWmsTrx wcsWmsTrx = new WcsWmsTrx();
			wcsWmsTrx.setWmsTrxId(wmsKpInvDTO.getTaskId());
			wcsWmsTrx.setTrxType("2");
			wcsWmsTrx.setToLocation(wmsKpInvDTO.getKpDevId());
			wcsWmsTrx.setFromLocation(from);

			this.save(wcsWmsTrx);

			return from;
		}

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public  boolean getEmptyTrayLocationForProduction(String trayNo, WCSInvInDTO wcsInvInDTO) {

		synchronized (InStorageServiceImpl.class){
			log.info("处理产线扫描托盘入库，获取并锁定入库事务锁。。。");

		//获取产线入库单中托盘信息
		InStorageTp inStorageTp =
				inStorageTpService.getOne(Wrappers.<InStorageTp>query().lambda().eq(InStorageTp::getUniqueCode,
						trayNo).eq(InStorageTp::getStatus, "0"));

		InStorageItem inStorageItem =
				inStorageItemService
						.getOne(Wrappers.<InStorageItem>query().lambda().eq(InStorageItem::getInStorageItemId,
								inStorageTp.getInStorageItemId()));

		InStorage instorage =
				inStorageService.getOne(Wrappers.<InStorage>query().lambda().eq(InStorage::getWmsBillNo,
						inStorageItem.getWmsBillNo()));

		StockArea stockArea;

		try {
			stockArea = inStorageService.getStockArea(inStorageItem, instorage,"3");
		} catch (Exception e) {
			log.error("获取库区出现异常。。。", e);
			return false;
		}
		if (null == stockArea) {
			log.error("获取库区出现异常。。。没找到库区");
			return false;
		}
		//获取启用的立库区未使用的排序级别最高的库位编号
		SLocation sLocation =
				sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
						.eq(SLocation::getAreaId, 1).eq(SLocation::getIsUsed, 0)
						.eq(SLocation::getLocationStatus, "1")
						.eq(SLocation::getStockAreaNo, stockArea.getStockAreaNo())
						.orderByAsc(SLocation::getSOrder).last("limit 1"));
		//获取产线入库开始位置的库位编号
		SLocation sLocationFrom =
				sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
						.eq(SLocation::getAreaId, 4).eq(SLocation::getIsUsed, 0)
						.eq(SLocation::getLocationStatus, "1")
						.orderByAsc(SLocation::getSOrder).last("limit 1"));

		if (sLocation == null) {

			return false;

		} else {

			inStorageTp.setStatus("1");
			inStorageTpService.updateById(inStorageTp);

			String sNo = sLocation.getSLocationNo();
			sLocation.setIsUsed("2");
			sLocation.setUpdateTime(LocalDateTime.now());
			sLocationService.updateById(sLocation);

			//更新库区状态 先检查是否符合预占用状态
			boolean yzyStatus = stockAreaService.checkStockAreaYuZhanYong(stockArea.getStockAreaNo());

			if (yzyStatus) {

				//更新库区为预占用状态
				StockArea stockAreaForCheck = stockAreaService.getById(sLocation.getStockAreaNo());
				stockAreaForCheck.setSku(stockAreaForCheck.getSku());
				stockAreaForCheck.setLockStatus("2");
				stockAreaService.updateById(stockAreaForCheck);
				log.info("更新库区为预占用状态,库区:" + sLocation.getStockAreaNo());

			} else {
				log.info("库区不符合预占用状态,按原来逻辑更新库区信息:" + sLocation.getStockAreaNo());
				//查找此库位所属库区是否有可使用的库位 判断是否锁定库区
				log.info("产线入库检查库区状态,查找此库位所属库区是否有可使用的库位,库区:" + sLocation.getStockAreaNo());
				SLocation sLocationForCheck =
						sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
								.eq(SLocation::getAreaId, 1).eq(SLocation::getIsUsed, 0)
								.eq(SLocation::getStockAreaNo, sLocation.getStockAreaNo())
								.eq(SLocation::getLocationStatus, "1")
								.orderByAsc(SLocation::getSOrder).last("limit 1"));
				if (null == sLocationForCheck) {
					//如果没有可使用的库位，更新库区为锁定状态
					StockArea stockAreaForCheck = stockAreaService.getById(sLocation.getStockAreaNo());
					stockAreaForCheck.setSku(stockAreaForCheck.getSku());
					stockAreaForCheck.setStatus("2");
					stockAreaForCheck.setLockStatus("1");
					stockAreaForCheck.setUpdateTime(LocalDateTime.now());
					stockAreaService.updateById(stockAreaForCheck);
					log.info("没有可使用的库位，更新库区为锁定状态,库区:" + sLocation.getStockAreaNo());
				} else {

					//如果有可使用的库位，更新库区为未锁定状态
					StockArea stockAreaForCheck = stockAreaService.getById(sLocation.getStockAreaNo());
					stockAreaForCheck.setSku(stockAreaForCheck.getSku());
					stockAreaForCheck.setStatus("1");
					stockAreaForCheck.setLockStatus("0");
					stockAreaForCheck.setUpdateTime(LocalDateTime.now());
					stockAreaService.updateById(stockAreaForCheck);
					log.info("有可使用的库位，更新库区为未锁定状态,库区:" + sLocation.getStockAreaNo());

				}
			}

			//更新产线入库单状态为正在入库
			//更新入库单信息 产线一次回调就更新成入库中
			instorage.setStatus("1");
			inStorageService.updateById(instorage);

			//调用wcs做产线入库
//			WCSInvInDTO wcsInvInDTO = new WCSInvInDTO();
			String taskId = "wmstrx" + LocalDateTime.now()
					.format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_MS_PATTERN));
			wcsInvInDTO.setTaskId(taskId);
			wcsInvInDTO.setTaskSource("3");
			wcsInvInDTO.setLocatorFrom(sLocationFrom.getSLocationNo());
			wcsInvInDTO.setInloc(sNo);
			wcsInvInDTO.setStockNo(trayNo);
			wcsInvInDTO.setMemoInfo1("wms调用wcs做产线入库");
			wcsInvInDTO.setMemoInfo2(stockArea.getStockAreaNo());

			if (!this.handleWCSInvIn(wcsInvInDTO)) {
				throw new RuntimeException("调用WCS入库接口异常!!!");
			}

			WcsWmsTrx wcsWmsTrx = new WcsWmsTrx();
			wcsWmsTrx.setWmsTrxId(taskId);
			wcsWmsTrx.setTrxType("3");
			wcsWmsTrx.setTrayNo(trayNo);
			wcsWmsTrx.setFromLocation(sLocationFrom.getSLocationNo());
			wcsWmsTrx.setToLocation(sNo);

			this.save(wcsWmsTrx);
			log.info("处理产线扫描托盘入库完成。。。正在释放入库事务锁。。。");
			return true;
		}

		}

	}

	@Override
	public void getWGInStorageTpForPDA() {

		StockArea stockArea = stockAreaService.getStockAreaForTrayOut();

		SLocation fromLocation =
				sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getAreaId, "3")
						.eq(SLocation::getIsUsed, "1").eq(SLocation::getStatus, "1")
						.eq(SLocation::getLocationStatus, "2")
						.eq(SLocation::getStockAreaNo, stockArea.getStockAreaNo())
						.orderByDesc(SLocation::getSOrder).last("limit 1"));
		//获取1个外购入库工作区
		SLocation sLocationWork =
				sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
						.eq(SLocation::getAreaId, 6)
						.eq(SLocation::getIsUsed, 0)
						.eq(SLocation::getLocationStatus, "1")
						.orderByAsc(SLocation::getSOrder).last("limit 1"));

		if (fromLocation == null) {

			throw new RuntimeException("未找到空托盘库位");

		}

		//外购入库空托补给 调用WCS 出库接口
		WCSInvOutDTO wcsInvOutDTO = new WCSInvOutDTO();
		String taskId = "wmstrx" + LocalDateTime.now()
				.format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_MS_PATTERN));
		wcsInvOutDTO.setTaskId(taskId);
		wcsInvOutDTO.setInvOutType("9");
		wcsInvOutDTO.setOutLoc(fromLocation.getSLocationNo());
		wcsInvOutDTO.setStockNo("多个空托");
		wcsInvOutDTO.setLocDesti(sLocationWork.getSLocationNo());
		wcsInvOutDTO.setMemoInfo1("wms调用wcs出库接口,用于空托出库forPDA");

		if (!this.handleWCSInvOut(wcsInvOutDTO)) {
			throw new RuntimeException("调用WCS出库接口异常!!!");
		}

		log.info("wms调用wcs出库接口,用于空托出库forPDA");

		fromLocation.setIsUsed("2");
		fromLocation.setUpdateTime(LocalDateTime.now());
		sLocationService.updateById(fromLocation);

		log.info("更新库区" + stockArea.getStockAreaNo() + "为锁定状态");
		stockArea.setStatus("1");
		stockArea.setLockStatus("1");
		stockArea.setUpdateTime(LocalDateTime.now());
		stockAreaService.updateById(stockArea);

		WcsWmsTrx wcsWmsTrx = new WcsWmsTrx();
		wcsWmsTrx.setWmsTrxId(taskId);
		wcsWmsTrx.setTrxType("5");
		wcsWmsTrx.setToLocation(sLocationWork.getSLocationNo());
		wcsWmsTrx.setFromLocation(fromLocation.getSLocationNo());

		this.save(wcsWmsTrx);

	}

	/**
	 * statusCode	statusInfor
	 * 0	任务新建/等待执行
	 * 1	任务执行中
	 * 2	到达起始点
	 * 3	离开起始点
	 * 4	到达结束点
	 * 5	离开结束点
	 * 6	故障错误
	 * wms wcs交互类型：
	 * 1:托盘入库 2:托盘出库 3:产线入库 4:电商入库 5:空托补给PDA 6空框入库 7空框出库 8外购入库
	 * 9产线出库 10产线返库 11空托返叠 12补货出库 13备货入库 14备货出库 15回库 16盘点出库 17盘点入库
	 * 18人工叠盘
	 * @param wcsWmsCallBackDTO
	 * @return
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean wcsCallBack(WcsWmsCallBackDTO wcsWmsCallBackDTO) {

		//获取wmstrx的记录
		WcsWmsTrx wcsWmsTrx =
				this.getOne(Wrappers.<WcsWmsTrx>query().lambda().eq(WcsWmsTrx::getWmsTrxId,
						wcsWmsCallBackDTO.getWmsTrxId()));
		//获取wmstrx中from库位信息
		SLocation sLocationFrom =
				sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getSLocationNo,
						wcsWmsTrx.getFromLocation()));
		//获取wmstrx中目标库位信息
		SLocation sLocationTO =
				sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getSLocationNo,
						wcsWmsTrx.getToLocation()));
		//如果wcs返回操作成功
		if (wcsWmsCallBackDTO.getStatusCode() == 5) {
			//如果是空托入库 更新库位信息
			if ("1".equals(wcsWmsTrx.getTrxType())) {

				sLocationTO.setIsUsed("1");
				sLocationTO.setSku("101");
				sLocationTO.setLocationStatus("2");
				sLocationTO.setUpdateTime(LocalDateTime.now());
				//更新库位信息
				sLocationService.updateById(sLocationTO);

				//更新库区状态
				//查找此库位所属库区是否有可使用的空库位
				SLocation sLocation =
						sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
								.eq(SLocation::getAreaId, 3).eq(SLocation::getIsUsed, 0)
								.eq(SLocation::getStockAreaNo, sLocationTO.getStockAreaNo())
								.eq(SLocation::getLocationStatus, "1")
								.orderByAsc(SLocation::getSOrder).last("limit 1"));

				StockArea stockArea = stockAreaService.getById(sLocationTO.getStockAreaNo());
				//如果没有可使用的库位，更新库区为满库状态
				if (null == sLocation) {
					stockArea.setStatus("2");
				}
				stockArea.setLockStatus("0");
				stockArea.setUpdateTime(LocalDateTime.now());
				stockAreaService.updateById(stockArea);

				//更新wmstrx信息
				wcsWmsTrx.setTrxStatus("1");
				this.updateById(wcsWmsTrx);
				return true;

			} else if ("2".equals(wcsWmsTrx.getTrxType()) || "7".equals(wcsWmsTrx.getTrxType())) {
				//托盘出库
				sLocationFrom.setIsUsed("0");
				sLocationFrom.setSku(null);
				sLocationFrom.setLocationStatus("1");
				sLocationFrom.setUpdateTime(LocalDateTime.now());
				//更新库位信息
				sLocationService.updateById(sLocationFrom);

				//更新库区状态
				//查找此库位所属库区是否有可使用的满货库位
				SLocation sLocation =
						sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
								.eq(SLocation::getAreaId, 3)
								.eq(SLocation::getStockAreaNo, sLocationFrom.getStockAreaNo())
								.eq(SLocation::getLocationStatus, "2")
								.orderByAsc(SLocation::getSOrder).last("limit 1"));

				StockArea stockArea = stockAreaService.getById(sLocationFrom.getStockAreaNo());
				//如果没有可使用的库位，更新库区为空库状态
				if (null == sLocation) {
					stockArea.setStatus("0");
				}
				stockArea.setLockStatus("0");
				stockArea.setUpdateTime(LocalDateTime.now());
				stockAreaService.updateById(stockArea);

				//更新wmstrx信息
				wcsWmsTrx.setTrxStatus("1");
				this.updateById(wcsWmsTrx);
				return true;
			} else if ("3".equals(wcsWmsTrx.getTrxType()) || "4".equals(wcsWmsTrx.getTrxType()) || "8".equals(wcsWmsTrx.getTrxType())) {
				//产线入库、电商入库、外购入库
				//获取入库单中托盘信息 不等于<>2已完成状态的 <>3已取消的
				InStorageTp inStorageTp =
						inStorageTpService.getOne(Wrappers.<InStorageTp>query().lambda().eq(InStorageTp::getUniqueCode,
								wcsWmsTrx.getTrayNo()).ne(InStorageTp::getStatus, "2").ne(InStorageTp::getStatus, "3"));

				if (null == inStorageTp) {

					log.error("此托盘" + wcsWmsTrx.getTrayNo() + "状态非法...");
					return false;
				}

				InStorageItem inStorageItem =
						inStorageItemService
								.getOne(Wrappers.<InStorageItem>query().lambda().eq(InStorageItem::getInStorageItemId,
										inStorageTp.getInStorageItemId()));

				InStorage instorage =
						inStorageService.getOne(Wrappers.<InStorage>query().lambda().eq(InStorage::getWmsBillNo,
								inStorageItem.getWmsBillNo()));

				//更新库位信息
				sLocationTO.setTrayNo(wcsWmsTrx.getTrayNo());
				sLocationTO.setBatchNo(inStorageItem.getBatchNo());
				String pici = LocalDateTime.now()
						.format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_MS_PATTERN));
				sLocationTO.setBatchCi(pici);
				sLocationTO.setHuozId(instorage.getHuozId());
				sLocationTO.setRealHuoz("1001");
				sLocationTO.setStockNum(inStorageTp.getAmount());
				sLocationTO.setIsUsed("1");
				sLocationTO.setSku(inStorageItem.getSku());
				if ("4".equals(wcsWmsTrx.getTrxType())) {
					//暂时取消空框货物状态'5'
					sLocationTO.setLocationStatus("4");
				} else {
					sLocationTO.setLocationStatus("4");
				}
				sLocationTO.setInPageNo(instorage.getInPageNo());
				sLocationTO.setInStorageItemId(inStorageItem.getInStorageItemId());
				sLocationTO.setUpdateTime(LocalDateTime.now());
				sLocationService.updateById(sLocationTO);

				int sumAmount = inStorageItem.getRealAmount() + inStorageTp.getAmount();
				inStorageItem.setRealAmount(sumAmount);
				inStorageItemService.updateById(inStorageItem);

				//如果是外购入库不更新入库单状态
				if (!"8".equals(wcsWmsTrx.getTrxType())) {

					//更新入库单信息 产线一次回调就更新成已完成
					instorage.setStatus("2");
					inStorageService.updateById(instorage);
					log.info("已更新入库单为入库完成：入库单编号：" + instorage.getWmsBillNo());
				} else {
					log.info("外购入库托盘wcs回调不更新入库单状态");
				}

				//更新库区状态 先检查是否符合预占用状态
				boolean yzyStatus = stockAreaService.checkStockAreaYuZhanYong(sLocationTO.getStockAreaNo());

				if (yzyStatus) {

					//更新库区为预占用状态
					StockArea stockAreaForCheck = stockAreaService.getById(sLocationTO.getStockAreaNo());
					stockAreaForCheck.setSku(stockAreaForCheck.getSku());
					stockAreaForCheck.setLockStatus("2");
					stockAreaService.updateById(stockAreaForCheck);
					log.info("更新库区为预占用状态,库区:" + sLocationTO.getStockAreaNo());

				} else {

					//查找此库位所属库区是否有可使用的库位
					SLocation sLocation =
							sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
									.eq(SLocation::getAreaId, 1).eq(SLocation::getIsUsed, 0)
									.eq(SLocation::getStockAreaNo, sLocationTO.getStockAreaNo())
									.eq(SLocation::getLocationStatus, "1")
									.orderByAsc(SLocation::getSOrder).last("limit 1"));
					if (null == sLocation) {
						//如果没有可使用的库位，更新库区为锁定状态
						StockArea stockArea = stockAreaService.getById(sLocationTO.getStockAreaNo());
						stockArea.setSku(stockArea.getSku());
						stockArea.setStatus("2");
						stockArea.setLockStatus("1");
						stockAreaService.updateById(stockArea);
					} else {

						//如果有可使用的库位，更新库区使用状态为使用中 锁定状态不更新
						StockArea stockArea = stockAreaService.getById(sLocationTO.getStockAreaNo());
						stockArea.setSku(stockArea.getSku());
						stockArea.setStatus("1");
						//stockArea.setLockStatus("0");
						stockAreaService.updateById(stockArea);

					}

				}

				inStorageTp.setItemId(pici);
				inStorageTp.setLocation(wcsWmsTrx.getToLocation());
				if (!"8".equals(wcsWmsTrx.getTrxType())) {

					inStorageTp.setAmount(inStorageItem.getAmount());
				}
				if ("3".equals(wcsWmsTrx.getTrxType())) {

					inStorageTp.setOperator("产线操作");
				}
				inStorageTp.setStatus("2");
				inStorageTpService.updateById(inStorageTp);

				//更新wmstrx信息
				wcsWmsTrx.setTrxStatus("1");
				this.updateById(wcsWmsTrx);

				//如果是外购入库不回调ERP入库单完成状态
//				if (!"8".equals(wcsWmsTrx.getTrxType())) {
				log.info("非外购入库wcs回调中,调用ERP上架回调。");
				//回调ERP：上架回调接口
				ERPInStorageDTO erpInStorageDTO = new ERPInStorageDTO();
				ERPInStorageItemDTO erpInStorageItemDTO = new ERPInStorageItemDTO();
				ERPInStorageTpDTO erpInStorageTpDTO = new ERPInStorageTpDTO();

				BeanUtils.copyProperties(inStorageTp, erpInStorageTpDTO);
				List<ERPInStorageTpDTO> erpInStorageTpDTOList = new ArrayList<>();
				erpInStorageTpDTOList.add(erpInStorageTpDTO);
				erpInStorageItemDTO.setErpInStorageTpDTOList(erpInStorageTpDTOList);
				BeanUtils.copyProperties(inStorageItem, erpInStorageItemDTO);
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

				String pdStr = inStorageItem.getProductDate().format(formatter);
				erpInStorageItemDTO.setProductDate(pdStr);

				String effStr = inStorageItem.getEffDate().format(formatter);
				erpInStorageItemDTO.setEffDate(effStr);

				List<ERPInStorageItemDTO> erpInStorageItemDTOS = new ArrayList<>();
				erpInStorageItemDTOS.add(erpInStorageItemDTO);
				erpInStorageDTO.setErpInStorageItemDTOList(erpInStorageItemDTOS);
				BeanUtils.copyProperties(instorage, erpInStorageDTO);

				String inDateStr = instorage.getInDate().format(formatter);
				erpInStorageDTO.setInDate(inDateStr);

				if (!"8".equals(wcsWmsTrx.getTrxType())) {
					erpInStorageDTO.setOrderStatus("1");
				} else {
					erpInStorageDTO.setOrderStatus("0");
				}
				erpCallBackService.callBackIn(erpInStorageDTO);
				log.info("调用ERP上架回调完成");
//				} else {
//					log.info("外购入库wcs回调中,不调用ERP上架回调。");
//				}

				return true;

			} else if ("5".equals(wcsWmsTrx.getTrxType())) {

				//空托补给 PDA
				sLocationFrom.setIsUsed("0");
				sLocationFrom.setSku(null);
				sLocationFrom.setLocationStatus("1");
				sLocationFrom.setUpdateTime(LocalDateTime.now());
				//更新库位信息
				sLocationService.updateById(sLocationFrom);

				//更新库区状态
				//查找此库位所属库区是否有可使用的满货库位
				SLocation sLocation =
						sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
								.eq(SLocation::getAreaId, 3)
								.eq(SLocation::getStockAreaNo, sLocationFrom.getStockAreaNo())
								.eq(SLocation::getLocationStatus, "2")
								.orderByAsc(SLocation::getSOrder).last("limit 1"));

				StockArea stockArea = stockAreaService.getById(sLocationFrom.getStockAreaNo());
				//如果没有可使用的库位，更新库区为空库状态
				if (null == sLocation) {
					stockArea.setStatus("0");
				}
				stockArea.setLockStatus("0");
				stockAreaService.updateById(stockArea);

				//更新wmstrx信息
				wcsWmsTrx.setTrxStatus("1");
				this.updateById(wcsWmsTrx);
				return true;

			} else if ("9".equals(wcsWmsTrx.getTrxType()) || "13".equals(wcsWmsTrx.getTrxType())) {

				//如果不需要托盘返库释放from库位
				if (sLocationFrom.getStockNum().equals(sLocationFrom.getOutAmount())) {
					//产线出库/备货入库 释放当前库位
					sLocationFrom.setTrayNo(null);
					sLocationFrom.setBatchNo("");
					sLocationFrom.setBatchCi("");
					sLocationFrom.setSku(null);
					sLocationFrom.setHuozId(null);
					sLocationFrom.setRealHuoz(null);
					sLocationFrom.setStockNum(0);
					sLocationFrom.setOutAmount(0);
					sLocationFrom.setIsUsed("0");
					sLocationFrom.setLocationStatus("1");
					sLocationFrom.setInPageNo("");
					sLocationFrom.setInStorageItemId(0);
					sLocationFrom.setUpdateTime(LocalDateTime.now());
					//更新库位信息
					sLocationService.updateById(sLocationFrom);
					log.info("不需要返库,已释放" + sLocationFrom.getSLocationNo() + "库位");
				} else {
					log.info("需要返库,不释放" + sLocationFrom.getSLocationNo() + "库位");
				}

				//检查库区情况是否释放库区
				StockArea stockArea = stockAreaService.getById(sLocationFrom.getStockAreaNo());

				log.info("检查" + stockArea.getStockAreaNo() + "库区是否可以释放");
				log.info("检查" + stockArea.getStockAreaNo() + "库区是否有location_status=4的情况");
				List<SLocation> sLocationList =
						sLocationService.list(Wrappers.<SLocation>query().lambda().eq(SLocation::getStockAreaNo,
										stockArea.getStockAreaNo())
								.eq(SLocation::getIsUsed, "1")
								.eq(SLocation::getLocationStatus, "4")
								.eq(SLocation::getStatus, "1"));
				log.info("检查结果:有" + sLocationList.size() + "个");

				log.info("检查" + stockArea.getStockAreaNo() + "库区是否有is_used=2的情况,正在使用中的");
				List<SLocation> sLocationListUsing =
						sLocationService.list(Wrappers.<SLocation>query().lambda().eq(SLocation::getStockAreaNo,
										stockArea.getStockAreaNo())
								.eq(SLocation::getIsUsed, "2")
								.eq(SLocation::getStatus, "1"));

				log.info("检查结果:有" + sLocationListUsing.size() + "个");

				sLocationList.addAll(sLocationListUsing);

				if (sLocationList.size() == 0) {

					stockArea.setInBillNumber("");
					stockArea.setBatchNo("");
					stockArea.setSku(null);
					stockArea.setStatus("0");
					stockArea.setLockStatus("0");
					stockAreaService.updateById(stockArea);
					log.info(stockArea.getStockAreaNo() + "库区没有找到正在使用有托盘货物并激活的库位，已经释放库区");
				} else {
					log.info(stockArea.getStockAreaNo() + "库区找到正在使用有托盘货物并激活的库位，不可以释放");
				}

				//更新wmstrx信息
				wcsWmsTrx.setTrxStatus("1");
				this.updateById(wcsWmsTrx);

				//如果是备货出库 再处理以下逻辑
				if ("13".equals(wcsWmsTrx.getTrxType())) {
					//查找agvin队列里此托盘任务 更新为已完成
					AgvInQueue agvInQueue =
							agvInQueueService.getOne(Wrappers.<AgvInQueue>query().lambda()
									.eq(AgvInQueue::getTrayNo, wcsWmsTrx.getTrayNo())
									.eq(AgvInQueue::getQueueStatus, "2"));
					agvInQueue.setQueueStatus("1");
					agvInQueue.setUpdateTime(LocalDateTime.now());
					agvInQueueService.updateById(agvInQueue);
					log.info("更新agvin队列任务为完成状态");

					//更新AGV库位信息
					SLocation agvSlocation = sLocationService.getById(agvInQueue.getAgvLocationNo());
					agvSlocation.setTrayNo(agvInQueue.getTrayNo());
					agvSlocation.setBatchNo(agvInQueue.getBatchNo());
					agvSlocation.setBatchCi(agvInQueue.getBatchCi());
					agvSlocation.setHuozId(agvInQueue.getHuozId());
					agvSlocation.setRealHuoz(agvInQueue.getRealHuoz());
					agvSlocation.setStockNum(agvInQueue.getStockNum());
					agvSlocation.setOutAmount(agvInQueue.getMustNum());
					agvSlocation.setIsUsed("1");
					agvSlocation.setSku(agvInQueue.getSku());
					agvSlocation.setLocationStatus("4");
					agvSlocation.setInPageNo(agvInQueue.getInPageNo());
					agvSlocation.setInStorageItemId(agvInQueue.getInStorageItemId());
					agvSlocation.setUpdateTime(LocalDateTime.now());
					sLocationService.updateById(agvSlocation);
					log.info("已更新agv库位信息" + agvSlocation.getSLocationNo());

					//更新out队列里agv信息
					log.info("查询agv out任务队列里有没有此托盘将要备货出库的任务 有可能没有agv信息");
					AgvOutQueue agvOutQueueToDoTask = agvOutQueueService.getOne(Wrappers.<AgvOutQueue>query().lambda()
							.eq(AgvOutQueue::getTrayNo, wcsWmsTrx.getTrayNo())
							.eq(AgvOutQueue::getQueueStatus, "0"));

					if (null == agvOutQueueToDoTask) {
						log.info("agv out任务队列里没有此托盘将要备货出库的任务无须更新agv信息");
					} else {
						log.info("当前agv out任务队列里此托盘AGV信息是:" + agvOutQueueToDoTask.getAgvLocationNo() + ",更新为：" + agvInQueue.getAgvLocationNo());
						agvOutQueueToDoTask.setAgvLocationNo(agvInQueue.getAgvLocationNo());
						agvOutQueueToDoTask.setUpdateTime(LocalDateTime.now());
						agvOutQueueService.updateById(agvOutQueueToDoTask);
					}

					//判断此出库单托盘任务是否都已完成
					List<AgvInQueue> agvInQueueList = agvInQueueService.list(Wrappers.<AgvInQueue>query().lambda()
							.eq(AgvInQueue::getOutPageNo, agvInQueue.getOutPageNo())
							.and(tmp -> tmp.eq(AgvInQueue::getQueueStatus, "2")
									.or().eq(AgvInQueue::getQueueStatus, "0")));

					if (agvInQueueList.size() == 0) {

						log.info("此出库单" + agvInQueue.getOutPageNo() + "没有正在作业的备货入库任务 更新此出库单状态为备货完成 7");
						OutStorage outStorage = outStorageService.getById(agvInQueue.getOutPageNo());
						if ("6".equals(outStorage.getStatus())) {
							log.info("此出库单" + agvInQueue.getOutPageNo() + "当前状态是：" + outStorage.getStatus() + "为备货中 可以更新此出库单状态为备货完成 7");
							outStorage.setStatus("7");
							outStorageService.updateById(outStorage);
						} else {
							log.warn("此出库单" + agvInQueue.getOutPageNo() + "当前状态是：" + outStorage.getStatus() + "<不>为备货中 不可以更新此出库单状态为备货完成 7");
						}

					}
					log.info("查询agv out任务队列里有没有此托盘备货出库任务");
					List<AgvOutQueue> agvOutQueueOneTask = agvOutQueueService.list(Wrappers.<AgvOutQueue>query().lambda()
							.eq(AgvOutQueue::getTrayNo, wcsWmsTrx.getTrayNo())
							.eq(AgvOutQueue::getQueueStatus, "3"));

					if (null == agvOutQueueOneTask || agvOutQueueOneTask.size() == 0) {
						log.info("查询agv out任务队列里没有此托盘备货出库任务");
					} else {
						log.info("查询agv out任务队列里有此托盘备货出库任务，发送wcs备货出库指令");
						for (AgvOutQueue agvOutQueueForOut : agvOutQueueOneTask) {

							agvOutQueueForOut.setAgvLocationNo(agvInQueue.getAgvLocationNo());

						}

						AgvOutQueue agvOutQueueForOut = agvOutQueueOneTask.get(0);
						agvOutQueueForOut.setUpdateTime(LocalDateTime.now());
						log.info("更新补充agvout队列ID为" + agvOutQueueForOut.getId() + "的AGVlocation信息 再发送wcs指令 AGV库位是：" + agvOutQueueForOut.getAgvLocationNo());
						agvOutQueueService.updateById(agvOutQueueForOut);
						OutStorage outStorageOneTask = outStorageService.getById(agvOutQueueOneTask.get(0).getOutPageNo());
						outStorageService.agvBHOut(outStorageOneTask, agvOutQueueOneTask);
					}

				}
				return true;

			} else if ("14".equals(wcsWmsTrx.getTrxType())) {
				synchronized (OutStorageServiceImpl.class){
					log.info("接收wcs备货出库任务回调，开始获取并锁定出库事务。。。");

				//备货出库 释放AGV库位
				sLocationFrom.setTrayNo(null);
				sLocationFrom.setBatchNo("");
				sLocationFrom.setBatchCi("");
				sLocationFrom.setSku(null);
				sLocationFrom.setHuozId(null);
				sLocationFrom.setRealHuoz(null);
				sLocationFrom.setStockNum(0);
				sLocationFrom.setOutAmount(0);
				sLocationFrom.setIsUsed("0");
				sLocationFrom.setLocationStatus("1");
				sLocationFrom.setInPageNo("");
				sLocationFrom.setInStorageItemId(0);
				sLocationFrom.setUpdateTime(LocalDateTime.now());
				//更新库位信息
				sLocationService.updateById(sLocationFrom);
				log.info("已释放AGV" + sLocationFrom.getSLocationNo() + "库位");

				//更新wmstrx信息
				wcsWmsTrx.setTrxStatus("1");
				this.updateById(wcsWmsTrx);

				//查找agvout队列里此托盘任务 更新为已完成
				AgvOutQueue agvOutQueue =
						agvOutQueueService.getOne(Wrappers.<AgvOutQueue>query().lambda()
								.eq(AgvOutQueue::getTrayNo, wcsWmsTrx.getTrayNo())
								.eq(AgvOutQueue::getQueueStatus, "2"));
				agvOutQueue.setQueueStatus("1");
				agvOutQueue.setUpdateTime(LocalDateTime.now());
				agvOutQueueService.updateById(agvOutQueue);
				log.info("更新agvout队列任务为完成状态");

				log.info("检查此出库单这个备货出库任务sku和批次是否都是已完成(没有正在执行或等待的任务)，如果完成触发下一顺序sku和批次备货出库");
				AgvOutQueue agvOutQueueForBHOutCheck =
						agvOutQueueService.getOne(Wrappers.<AgvOutQueue>query().lambda()
								.eq(AgvOutQueue::getOutPageNo, agvOutQueue.getOutPageNo())
								.eq(AgvOutQueue::getSku, agvOutQueue.getSku())
								.and(tmp -> tmp.eq(AgvOutQueue::getQueueStatus, "2")
										.or().eq(AgvOutQueue::getQueueStatus, "3")).last("limit 1"));
				if (null == agvOutQueueForBHOutCheck) {
					log.info("出库单" + agvOutQueue.getOutPageNo() +
							"SKU:" + agvOutQueue.getSku() + "已经没有正在作业或等待任务,触发后续<<此月台" + agvOutQueue.getOutLocation() + ">>队列任务");
					AgvOutQueue agvOutQueueForNextBHOutCheck =
							agvOutQueueService.getOne(Wrappers.<AgvOutQueue>query().lambda()
									.eq(AgvOutQueue::getQueueStatus, "0")
									.eq(AgvOutQueue::getOutLocation, agvOutQueue.getOutLocation())
									.orderByAsc(AgvOutQueue::getId)
									.last("limit 1"));
					if (null == agvOutQueueForNextBHOutCheck) {
						log.info("AGVOUT队列中没有此月台任务需要作业");
					} else {
						List<AgvOutQueue> agvOutQueuesTodo = agvOutQueueService.list(Wrappers.<AgvOutQueue>query().lambda()
								.eq(AgvOutQueue::getOutPageNo, agvOutQueueForNextBHOutCheck.getOutPageNo())
								.eq(AgvOutQueue::getSku, agvOutQueueForNextBHOutCheck.getSku())
								.eq(AgvOutQueue::getBatchNo, agvOutQueueForNextBHOutCheck.getBatchNo())
								.orderByAsc(AgvOutQueue::getId));
						log.info("AGVOUT队列中有任务需要作业,出库单：" + agvOutQueueForNextBHOutCheck.getOutPageNo()
								+ "batchno:" + agvOutQueueForNextBHOutCheck.getBatchNo()
								+ "SKU:" + agvOutQueueForNextBHOutCheck.getSku() + "队列任务：" + agvOutQueuesTodo + "条");
						OutStorage outStorage = outStorageService.getById(agvOutQueueForNextBHOutCheck.getOutPageNo());

						outStorageService.agvBHOut(outStorage, agvOutQueuesTodo);

					}

				} else {
					log.info("此出库单sku还有正在作业的任务，等待全部完毕再触发下一批任务。");
				}
				log.info("释放AGV库位后查询有没有还未作业的单订单备货入库任务");
				List<AgvInQueue> agvInQueueOneTask = agvInQueueService.list(Wrappers.<AgvInQueue>query().lambda()
						.eq(AgvInQueue::getQueueStatus, "0")
						.isNull(AgvInQueue::getAgvLocationNo)
						.orderByAsc(AgvInQueue::getId).last("limit 1"));
				if (null == agvInQueueOneTask || agvInQueueOneTask.size() == 0) {
					log.info("AGV in队列中没有未作业的任务");
				} else {
					log.info("找到还未作业的单订单备货入库任务，先补充AGVlocation信息 再发送wcs指令");
					for (AgvInQueue agvInQueueOne : agvInQueueOneTask) {

						agvInQueueOne.setAgvLocationNo(agvOutQueue.getAgvLocationNo());

					}
					SLocation agvLoaction = sLocationService.getById(agvOutQueue.getAgvLocationNo());
					agvLoaction.setIsUsed("2");
					agvLoaction.setUpdateTime(LocalDateTime.now());
					sLocationService.updateById(agvLoaction);
					AgvInQueue agvInQueueForOut = agvInQueueOneTask.get(0);
					log.info("更新补充agvin队列ID为" + agvInQueueForOut.getId() + "的AGVlocation信息 并锁定agv库位 再发送wcs指令" + agvInQueueForOut.getAgvLocationNo());
					agvInQueueForOut.setUpdateTime(LocalDateTime.now());
					agvInQueueService.updateById(agvInQueueForOut);
					outStorageService.agvInQueuetoWCS(agvInQueueOneTask);
				}
					log.info("已处理完成wcs出库备货请求，正在释放出库事务锁。。。");
				return true;
				}

			} //else if ("10".equals(wcsWmsTrx.getTrxType())) {
				//产线返库
//				TraySku traySku = traySkuService.getById(wcsWmsTrx.getTrayNo());
//				sLocationTO.setTrayNo(wcsWmsTrx.getTrayNo());
//				sLocationTO.setBatchNo(traySku.getBatchNo());
//				sLocationTO.setBatchCi(traySku.getBatchCi());
//				sLocationTO.setSku(traySku.getSku());
//				sLocationTO.setHuozId(traySku.getHuozId());
//				sLocationTO.setRealHuoz(traySku.getRealHuoz());
//				sLocationTO.setStockNum(traySku.getStockNum());
//				sLocationTO.setOutAmount(0);
//				sLocationTO.setIsUsed("1");
//				sLocationTO.setLocationStatus("4");
//				sLocationTO.setInPageNo(traySku.getInPageNo());
//				sLocationTO.setInStorageItemId(traySku.getInStorageItemId());
//				sLocationTO.setUpdateTime(LocalDateTime.now());
//				//更新库位信息
//				sLocationService.updateById(sLocationTO);
//				//移除临时托盘信息
//				traySkuService.removeById(wcsWmsTrx.getTrayNo());
//				//更新wmstrx信息
//				wcsWmsTrx.setTrxStatus("1");
//
//				//检查库区情况是否是锁定状态
//				StockArea stockArea = stockAreaService.getById(sLocationTO.getStockAreaNo());
//
//				log.info("托盘返库 检查" + stockArea.getStockAreaNo() + "库区是否锁定");
//
//				if ("1".equals(stockArea.getLockStatus())) {
//					log.info("托盘返库 库区是锁定状态 不必更新");
//				} else {
//					log.info(stockArea.getStockAreaNo() + "库区不是锁定状态，重新设置库区为满库和锁定状态");
//					stockArea.setSku(stockArea.getSku());
//					stockArea.setStatus("2");
//					stockArea.setLockStatus("1");
//					stockAreaService.updateById(stockArea);
//				}
//
//				this.updateById(wcsWmsTrx);
//
//				return true;
			//}
			else if ("11".equals(wcsWmsTrx.getTrxType())) {
				//更新wmstrx信息
				wcsWmsTrx.setTrxStatus("1");
				this.updateById(wcsWmsTrx);

				return true;
			} else if ("12".equals(wcsWmsTrx.getTrxType())) {
				//补货出库 释放当前库位
				sLocationFrom.setTrayNo(null);
				sLocationFrom.setBatchNo("");
				sLocationFrom.setBatchCi("");
				sLocationFrom.setSku(null);
				sLocationFrom.setHuozId(null);
				sLocationFrom.setRealHuoz(null);
				sLocationFrom.setStockNum(0);
				sLocationFrom.setIsUsed("0");
				sLocationFrom.setLocationStatus("1");
				sLocationFrom.setInPageNo("");
				sLocationFrom.setInStorageItemId(0);
				sLocationFrom.setUpdateTime(LocalDateTime.now());
				//更新库位信息
				sLocationService.updateById(sLocationFrom);

				//更新wmstrx信息
				wcsWmsTrx.setTrxStatus("1");
				this.updateById(wcsWmsTrx);
				return true;
			} else if ("18".equals(wcsWmsTrx.getTrxType())) {
				log.info("正在处理wcs人工叠盘回调...");

				//更新wmstrx信息
				wcsWmsTrx.setTrxStatus("1");
				this.updateById(wcsWmsTrx);

				return true;
			} else if ("15".equals(wcsWmsTrx.getTrxType()) || "17".equals(wcsWmsTrx.getTrxType())
					|| "10".equals(wcsWmsTrx.getTrxType())) {

				log.info("开始处理返库、回库或盘点回库到库位逻辑");
				TraySku traySku = traySkuService.getById(wcsWmsTrx.getTrayNo());

				//更新库位信息
				sLocationTO.setTrayNo(traySku.getTrayNo());
				sLocationTO.setBatchNo(traySku.getBatchNo());
				sLocationTO.setBatchCi(traySku.getBatchCi());
				sLocationTO.setHuozId(traySku.getHuozId());
				sLocationTO.setRealHuoz("1001");
				sLocationTO.setStockNum(traySku.getStockNum());
				sLocationTO.setIsUsed("1");
				sLocationTO.setSku(traySku.getSku());
				sLocationTO.setLocationStatus("4");
				sLocationTO.setInPageNo(traySku.getInPageNo());
				sLocationTO.setInStorageItemId(traySku.getInStorageItemId());
				sLocationTO.setUpdateTime(LocalDateTime.now());
				sLocationService.updateById(sLocationTO);

				//更新库区状态 先检查是否符合预占用状态
				boolean yzyStatus = stockAreaService.checkStockAreaYuZhanYong(sLocationTO.getStockAreaNo());

				if (yzyStatus) {

					//更新库区为预占用状态
					StockArea stockAreaForCheck = stockAreaService.getById(sLocationTO.getStockAreaNo());
					stockAreaForCheck.setSku(stockAreaForCheck.getSku());
					stockAreaForCheck.setLockStatus("2");
					stockAreaForCheck.setUpdateTime(LocalDateTime.now());
					stockAreaService.updateById(stockAreaForCheck);
					log.info("更新库区为预占用状态,库区:" + sLocationTO.getStockAreaNo());

					log.info("清除临时托盘和货品信息");
					traySkuService.removeById(wcsWmsTrx.getTrayNo());

				} else {

					//查找此库位所属库区是否有可使用的库位
					SLocation sLocation =
							sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
									.eq(SLocation::getIsUsed, 0)
									.eq(SLocation::getStockAreaNo, sLocationTO.getStockAreaNo())
									.eq(SLocation::getLocationStatus, "1")
									.orderByAsc(SLocation::getSOrder).last("limit 1"));
					if (null == sLocation) {
						//如果没有可使用的库位，更新库区为锁定状态
						StockArea stockArea = stockAreaService.getById(sLocationTO.getStockAreaNo());
						stockArea.setSku(stockArea.getSku());
						stockArea.setStatus("2");
						stockArea.setLockStatus("1");
						stockArea.setUpdateTime(LocalDateTime.now());
						stockAreaService.updateById(stockArea);
					} else {

						//如果有可使用的库位，更新库区使用状态为使用中 锁定状态不更新
						StockArea stockArea = stockAreaService.getById(sLocationTO.getStockAreaNo());
						stockArea.setSku(stockArea.getSku());
						stockArea.setStatus("1");
						//stockArea.setLockStatus("0");
						stockArea.setUpdateTime(LocalDateTime.now());
						stockAreaService.updateById(stockArea);

					}
					log.info("清除临时托盘和货品信息");
					traySkuService.removeById(wcsWmsTrx.getTrayNo());

				}
				if("17".equals(wcsWmsTrx.getTrxType())){
					CheckSheetItem csi = checkSheetItemService.getOne(Wrappers.<CheckSheetItem>query().lambda()
							.eq(CheckSheetItem::getTrayNo, wcsWmsTrx.getTrayNo())
							.eq(CheckSheetItem::getSubmitStatus, "2"));
					csi.setSubmitStatus("3");
					csi.setUpdateTime(LocalDateTime.now());
					checkSheetItemService.updateById(csi);
					List<CheckSheetItem> checkSheetItemListTotal = checkSheetItemService.list(Wrappers.<CheckSheetItem>query().lambda()
							.eq(CheckSheetItem::getCheckSheetNo,csi.getCheckSheetNo()));
					List<CheckSheetItem> checkSheetItemListDone = checkSheetItemService.list(Wrappers.<CheckSheetItem>query().lambda()
							.eq(CheckSheetItem::getCheckSheetNo,csi.getCheckSheetNo())
							.eq(CheckSheetItem::getSubmitStatus,"3"));
					if(checkSheetItemListDone.size() == checkSheetItemListTotal.size()){
						log.info("盘点单所有行项目都已完成 修改盘点单为完成状态");
						CheckSheet cs = checkSheetService.getById(csi.getCheckSheetNo());
						cs.setCheckStatus("3");
						cs.setUpdateTime(LocalDateTime.now());
						checkSheetService.updateById(cs);

					}
				}
				//更新wmstrx信息
				wcsWmsTrx.setTrxStatus("1");
				this.updateById(wcsWmsTrx);
			} else if ("16".equals(wcsWmsTrx.getTrxType())) {

				//修改盘点明细状态为盘点中
				CheckSheetItem csi = checkSheetItemService.getOne(Wrappers.<CheckSheetItem>query().lambda()
						.eq(CheckSheetItem::getTrayNo, wcsWmsTrx.getTrayNo())
						.eq(CheckSheetItem::getSubmitStatus, "0"));
				csi.setSubmitStatus("1");
				csi.setUpdateTime(LocalDateTime.now());
				checkSheetItemService.updateById(csi);

				//盘点出库
				sLocationFrom.setTrayNo(null);
				sLocationFrom.setBatchNo("");
				sLocationFrom.setBatchCi("");
				sLocationFrom.setSku(null);
				sLocationFrom.setHuozId(null);
				sLocationFrom.setRealHuoz(null);
				sLocationFrom.setStockNum(0);
				sLocationFrom.setOutAmount(0);
				sLocationFrom.setIsUsed("0");
				sLocationFrom.setLocationStatus("1");
				sLocationFrom.setInPageNo("");
				sLocationFrom.setInStorageItemId(0);
				sLocationFrom.setUpdateTime(LocalDateTime.now());
				//更新库位信息
				sLocationService.updateById(sLocationFrom);
				log.info("不需要返库,已释放" + sLocationFrom.getSLocationNo() + "库位");

				//检查库区情况是否释放库区
				StockArea stockArea = stockAreaService.getById(sLocationFrom.getStockAreaNo());

				log.info("检查" + stockArea.getStockAreaNo() + "库区是否可以释放");
				log.info("检查" + stockArea.getStockAreaNo() + "库区是否有location_status=4的情况");
				List<SLocation> sLocationList =
						sLocationService.list(Wrappers.<SLocation>query().lambda().eq(SLocation::getStockAreaNo,
										stockArea.getStockAreaNo())
								.eq(SLocation::getIsUsed, "1")
								.eq(SLocation::getLocationStatus, "4")
								.eq(SLocation::getStatus, "1"));
				log.info("检查结果:有" + sLocationList.size() + "个");

				log.info("检查" + stockArea.getStockAreaNo() + "库区是否有is_used=2的情况,正在使用中的");
				List<SLocation> sLocationListUsing =
						sLocationService.list(Wrappers.<SLocation>query().lambda().eq(SLocation::getStockAreaNo,
										stockArea.getStockAreaNo())
								.eq(SLocation::getIsUsed, "2")
								.eq(SLocation::getStatus, "1"));

				log.info("检查结果:有" + sLocationListUsing.size() + "个");

				sLocationList.addAll(sLocationListUsing);

				if (sLocationList.size() == 0) {

					stockArea.setInBillNumber("");
					stockArea.setBatchNo("");
					stockArea.setSku(null);
					stockArea.setStatus("0");
					stockArea.setLockStatus("0");
					stockArea.setUpdateTime(LocalDateTime.now());
					stockAreaService.updateById(stockArea);
					log.info(stockArea.getStockAreaNo() + "库区没有找到正在使用有托盘货物并激活的库位，已经释放库区");
				} else {
					log.info(stockArea.getStockAreaNo() + "库区找到正在使用有托盘货物并激活的库位，不可以释放");
				}

				//更新wmstrx信息
				wcsWmsTrx.setTrxStatus("1");
				this.updateById(wcsWmsTrx);

			}
			return true;

			//如果wcs返回失败
		} else if (wcsWmsCallBackDTO.getStatusCode() == 6) {

			//更新wmstrx信息
			wcsWmsTrx.setTrxStatus("2");
			this.updateById(wcsWmsTrx);

		}

		return false;
	}

	@Override
	public boolean handleWCSEmptyCtl(WCSEmptyCtlDTO wcsEmptyCtlDTO) {

		log.info("人工叠盘任务调用wcs接口地址是:"+emptyCtlUrl);
		RestTemplate restTemplate = new RestTemplate();

		MultiValueMap<String, String> paramMap = new LinkedMultiValueMap() {
			{
				add("taskId", wcsEmptyCtlDTO.getTaskId());
				add("stockNo", wcsEmptyCtlDTO.getStockNo());
			}
		};

		log.info("发送wcs人工叠盘请求的参数是:" + wcsEmptyCtlDTO);

		//封装请求头
		try {
			RequestEntity requestEntity = RequestEntity
					// 可以不填
					.post(new URI(""))
					// 参数传输类型 url编码格式 application/x-www-form-urlencoded
					.contentType(MediaType.APPLICATION_FORM_URLENCODED)
					.accept(MediaType.ALL).acceptCharset(StandardCharsets.UTF_8)
					.body(paramMap);
			//基于注解映射对应实体
			ResponseEntity<WCSResponseDTO> result = restTemplate.postForEntity(emptyCtlUrl,
					requestEntity, WCSResponseDTO.class);
			WCSResponseDTO wcsResponseDTO = result.getBody();

			log.info("WCS人工叠盘请求响应errorCode:" + wcsResponseDTO.getErrorCode());
			log.info("WCS人工叠盘请求响应errorMsg:" + wcsResponseDTO.getErrorMsg());

			if ("0".equals(wcsResponseDTO.getErrorCode())) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.error("调用wcs人工叠盘接口出现异常:" + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
}

