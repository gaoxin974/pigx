package com.pig4cloud.pigx.wms.controller;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.wms.dto.*;
import com.pig4cloud.pigx.wms.entity.SLocation;
import com.pig4cloud.pigx.wms.service.CheckSheetService;
import com.pig4cloud.pigx.wms.service.SLocationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/checkSheet" )
@Api(value = "checksheet", tags = "盘点单信息管理")
public class CheckSheetController {

    private final CheckSheetService checkSheetService;

	private final SLocationService sLocationService;

	@ApiOperation(value = "分页查询库区forcheck", notes = "分页查询库区forcheck")
	@GetMapping("/queryStockAreaForCheckPage" )
	public R queryStockAreaForCheckPage(Page page, QueryStockAreaForCheckPageDTO queryStockAreaForCheckPageDTO) {
		return R.ok(checkSheetService.getStockAreaForCheckPage(page, queryStockAreaForCheckPageDTO));
	}

	@ApiOperation(value = "PC获取盘点区位置", notes = "PC获取盘点区位置")
	@GetMapping("/listCheckLocationsForPC")
	public R listCXOutStorageWorkStationForPC() {

		List<SLocation> sLocationList = sLocationService.list(Wrappers.<SLocation>query().lambda()
				.eq(SLocation::getAreaId,"12")
				.eq(SLocation::getStatus,"1")
				.eq(SLocation::getLocationStatus,"1").orderByAsc(SLocation::getSOrder));

		return R.ok(sLocationList);

	}

	@ApiOperation(value = "创建盘点单", notes = "创建盘点单")
	@SysLog("创建盘点单")
	@PostMapping("/createCheckSheet")
	public R createPackage(@RequestBody List<StockAreaForCheckDTO> stockAreasList, String checkLocation) {

		log.info("收到创建盘点单请求库区为:"+ JSONUtil.toJsonStr(stockAreasList));
		log.info("收到创建盘点单请求盘点位置为:"+ JSONUtil.toJsonStr(checkLocation));

		if(null == stockAreasList || stockAreasList.size() == 0){
			return R.failed("库区信息为空 无法创建盘点单。。。");
		}
		if(StringUtils.isEmpty(checkLocation)){

			return R.failed("盘点区域为空 无法创建盘点单。。。");
		}
		Set<String> stockAreasSet = new HashSet();
		for(StockAreaForCheckDTO stockAreaForCheckDTO : stockAreasList){
			stockAreasSet.add(stockAreaForCheckDTO.getStockAreaNo());
		}

		checkSheetService.createCheckSheet(stockAreasSet,checkLocation);

		return R.ok(null,"创建盘点单成功。");

	}

	@ApiOperation(value = "分页查询盘点单", notes = "分页查询盘点单")
	@GetMapping("/queryCheckSheetsPage" )
	public R queryCheckSheetsPage(Page page, QueryCheckSheetsPageDTO queryStockAreaForCheckPageDTO) {
		return R.ok(checkSheetService.getCheckSheetsPage(page, queryStockAreaForCheckPageDTO));
	}

	@ApiOperation(value = "列表查询盘点单item", notes = "列表查询盘点单item")
	@GetMapping("/listCheckSheetItems/{checkSheetNo}" )
	public R getCheckSheetItems(@PathVariable("checkSheetNo") String checkSheetNo) {
		return R.ok(checkSheetService.getCheckSheetItems(checkSheetNo));
	}

	@ApiOperation(value = "盘点下发", notes = "盘点下发")
	@PostMapping("/startCheck")
	public R startCheck(String checkSheetNo) {

		WMSReturnInfo wmsReturnInfo = checkSheetService.startCheck(checkSheetNo);

		if(wmsReturnInfo.isSuccess()){
			return R.ok(wmsReturnInfo.getMessage()+"WMS处理成功~");
		}else{
			return R.failed("WMS处理失败!",wmsReturnInfo.getMessage());
		}
	}

	@ApiOperation(value = "盘点提交", notes = "盘点提交")
	@PostMapping("/submitCheckSheet")
	public R submitCheckSheet(String checkSheetNo) {

		WMSReturnInfo wmsReturnInfo = checkSheetService.submitCheckSheet(checkSheetNo);

		if(wmsReturnInfo.isSuccess()){
			return R.ok(wmsReturnInfo.getMessage()+"WMS处理成功~");
		}else{
			return R.failed("WMS处理失败!",wmsReturnInfo.getMessage());
		}
	}

	@ApiOperation(value = "复盘列表", notes = "复盘列表")
	@GetMapping("/listReCheck/{checkSheetNo}" )
	public R listReCheck(@PathVariable("checkSheetNo") String checkSheetNo) {
		return R.ok(checkSheetService.listReCheck(checkSheetNo));
	}

	@ApiOperation(value = "分页查询库区forlock", notes = "分页查询库区forlock")
	@GetMapping("/queryStockAreaForLockPage" )
	public R queryStockAreaForLockPage(Page page, QueryStockAreaForLockPageDTO queryStockAreaForLockPageDTO) {
		return R.ok(checkSheetService.getStockAreaForLockPage(page, queryStockAreaForLockPageDTO));
	}

	@ApiOperation(value = "锁定库区", notes = "锁定库区")
	@SysLog("锁定库区")
	@PostMapping("/lockStockAreas")
	public R lockStockAreas(@RequestBody List<StockAreaForCheckDTO> stockAreasList) {

		log.info("收到锁定库区请求库区为:"+ JSONUtil.toJsonStr(stockAreasList));

		if(null == stockAreasList || stockAreasList.size() == 0){
			return R.failed("库区信息为空 无法锁定库区。。。");
		}

		Set<String> stockAreasSet = new HashSet();
		for(StockAreaForCheckDTO stockAreaForCheckDTO : stockAreasList){
			log.info("循环添加要锁定的库区到set里:"+stockAreaForCheckDTO.getStockAreaNo());
			stockAreasSet.add(stockAreaForCheckDTO.getStockAreaNo());
		}

		checkSheetService.lockStockAreas(stockAreasSet);

		return R.ok(null,"锁定库区成功。");
	}

}
