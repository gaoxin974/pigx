/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pigx.wms.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.wms.dto.SLocationDTO;
import com.pig4cloud.pigx.wms.entity.SLocation;
import com.pig4cloud.pigx.wms.entity.TraySku;
import com.pig4cloud.pigx.wms.mapper.SLocationMapper;
import com.pig4cloud.pigx.wms.mapper.TraySkuMapper;
import com.pig4cloud.pigx.wms.service.SLocationService;
import com.pig4cloud.pigx.wms.service.TraySkuService;
import com.pig4cloud.pigx.wms.vo.SLocationPageVO;

import lombok.AllArgsConstructor;

/**
 * 
 *
 * @author gaoxin
 * @date 2021-04-08 09:50:38
 */
@Service
@AllArgsConstructor
public class TraySkuServiceImpl extends ServiceImpl<TraySkuMapper, TraySku> implements TraySkuService {

}
