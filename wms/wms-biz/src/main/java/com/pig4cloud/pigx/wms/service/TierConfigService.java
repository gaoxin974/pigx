

package com.pig4cloud.pigx.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pigx.wms.entity.TierConfig;

public interface TierConfigService extends IService<TierConfig> {

}
