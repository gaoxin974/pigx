/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 入库单行项目托盘信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:39
 */
@Data
@TableName("in_storage_tp")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "入库单行项目托盘信息")
public class InStorageTp extends Model<InStorageTp> {
private static final long serialVersionUID = 1L;

    /**
     * 入库单行项目托盘信息自增id
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value="入库单行项目托盘信息自增id")
    private Integer inStorageTpId;
	/**
	 * wms入库单编号
	 */
	@ApiModelProperty(value="wms入库单编号")
	private String wmsBillNo;
    /**
     * 入库单行项目id
     */
    @ApiModelProperty(value="入库单行项目id")
    private Integer inStorageItemId;
    /**
     * 托盘编号
     */
    @ApiModelProperty(value="托盘编号")
    private String uniqueCode;
    /**
     * sku
     */
    @ApiModelProperty(value="sku")
    private String sku;

	@ApiModelProperty(value="amount")
	private int amount;
    /**
     * 批次号
     */
    @ApiModelProperty(value="批次号")
    private String itemId;
    /**
     * 上架库位编号
     */
    @ApiModelProperty(value="上架库位编号")
    private String location;
    /**
     * 操作员
     */
    @ApiModelProperty(value="操作员")
    private String operator;
    /**
     * 0:未上架 1:正在上架 2:已完成
     */
    @ApiModelProperty(value="0:未上架 1:正在上架 2:已完成")
    private String status;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private LocalDateTime updateTime;
    }
