package com.pig4cloud.pigx.wms.webtools;

import java.util.Map;
import java.util.TreeMap;

/**
 * @Author: 高欣
 * @Date: 2021/10/24 16:54
 */
public class TestMethod {

	public static void main(String[] args) {

		//1、使用默认的TreeMap 构造函数，其中key值需要有比较规则

		TreeMap<Integer, String> map = new TreeMap<>();

		map.put(new Integer(2), "BB");

		map.put(new Integer(1), "AA");

		map.put(new Integer(5), "EE");

		map.put(new Integer(3), "CC");

		map.put(new Integer(4), "DD");

		map.put(new Integer(2), "AA");   //验证重复key是否能够插入

		//使用遍历EntrySet方式

		for (Map.Entry<Integer, String> entry : map.entrySet()) {

			System.out.println("Key:" + entry.getKey() + " --- value:" + entry.getValue());

		}
	}
}
