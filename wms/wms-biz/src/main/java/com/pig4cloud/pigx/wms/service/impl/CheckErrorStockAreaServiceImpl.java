
package com.pig4cloud.pigx.wms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.wms.entity.CheckErrorStockArea;
import com.pig4cloud.pigx.wms.mapper.CheckErrorStockAreaMapper;
import com.pig4cloud.pigx.wms.service.CheckErrorStockAreaService;
import org.springframework.stereotype.Service;

@Service
public class CheckErrorStockAreaServiceImpl extends ServiceImpl<CheckErrorStockAreaMapper, CheckErrorStockArea> implements CheckErrorStockAreaService {

}
