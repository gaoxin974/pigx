package com.pig4cloud.pigx.wms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.wms.entity.AgvOutQueue;
import com.pig4cloud.pigx.wms.mapper.AgvOutQueueMapper;
import com.pig4cloud.pigx.wms.service.AgvOutQueueService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AgvOutQueueServiceImpl extends ServiceImpl<AgvOutQueueMapper, AgvOutQueue> implements AgvOutQueueService {

}
