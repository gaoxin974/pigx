/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 入库单信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:32
 */
@Data
@TableName("bh_tmp")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "补货信息临时表")
public class BhTMP extends Model<BhTMP> {
private static final long serialVersionUID = 1L;


    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value="主键")
    private String id;

    @ApiModelProperty(value="波次单号")
    private String packageNo;

    @ApiModelProperty(value="托盘号")
    private String tpNo;

    @ApiModelProperty(value="sku")
    private String sku;

    @ApiModelProperty(value="商品名称")
    private String skuName;

    @ApiModelProperty(value="补货提交状态")
    private String status;

    @ApiModelProperty(value="操作人")
    private String operator;

	@ApiModelProperty(value="删除标识")
	private String del;

    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value="修改时间")
    private LocalDateTime updateTime;
    }
