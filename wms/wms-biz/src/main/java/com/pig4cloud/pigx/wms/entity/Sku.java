/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 商品信息表
 *
 * @author gaoxin
 * @date 2021-04-08 11:34:31
 */
@Data
@TableName("sku")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "商品信息表")
public class Sku extends Model<Sku> {
private static final long serialVersionUID = 1L;

    /**
     * 商品sku 主键
     */
    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value="商品sku 主键")
    private String sku;
    /**
     * 物流中心编码 外键
     */
    @ApiModelProperty(value="物流中心编码 外键")
    private String wlzxCode;
    /**
     * 货主ID 外键
     */
    @ApiModelProperty(value="货主ID 外键")
    private String huozId;
    /**
     * 商品名称
     */
    @ApiModelProperty(value="商品名称")
    private String gdname;
    /**
     * 助记码
     */
    @ApiModelProperty(value="助记码")
    private String zjm;
    /**
     * 商品通用名/简称
     */
    @ApiModelProperty(value="商品通用名/简称")
    private String namejc;
    /**
     * 英文名
     */
    @ApiModelProperty(value="英文名")
    private String nameen;
    /**
     * 商品规格
     */
    @ApiModelProperty(value="商品规格")
    private String gdspec;
    /**
     * 包装数量
     */
    @ApiModelProperty(value="包装数量")
    private Integer baozNum;
    /**
     * 包装单位
     */
    @ApiModelProperty(value="包装单位")
    private String baozDanw;
    /**
     * 拆分粒度 （1可拆零；2不拆中包装；3不拆大包装；4可以小数）
     */
    @ApiModelProperty(value="拆分粒度 （1可拆零；2不拆中包装；3不拆大包装；4可以小数）")
    private String chaifLid;
    /**
     * 最小开票单位
     */
    @ApiModelProperty(value="最小开票单位")
    private String kaipdwMin;
    /**
     * 中包装数量
     */
    @ApiModelProperty(value="中包装数量")
    private String zhongbz;
    /**
     * 型号/尺码，如果没有可传空
     */
    @ApiModelProperty(value="型号/尺码，如果没有可传空")
    private String gdmode;
    /**
     * 主要构成，如果没有可传空
     */
    @ApiModelProperty(value="主要构成，如果没有可传空")
    private String struct;
    /**
     * 缺省单位包装含量
     */
    @ApiModelProperty(value="缺省单位包装含量")
    private BigDecimal bzhl;
    /**
     * 缺省单位(最小单位)
     */
    @ApiModelProperty(value="缺省单位(最小单位)")
    private String unit;
    /**
     * 单位名称
     */
    @ApiModelProperty(value="单位名称")
    private String unitname;
    /**
     * 状态(Y正常/T停购/S停销/N新品/E淘汰)
     */
    @ApiModelProperty(value="状态(Y正常/T停购/S停销/N新品/E淘汰)")
    private String flag;
    /**
     * 系统类别
     */
    @ApiModelProperty(value="系统类别")
    private String catid;
    /**
     * 注册商标
     */
    @ApiModelProperty(value="注册商标")
    private String loginlabel;
    /**
     * 生产商
     */
    @ApiModelProperty(value="生产商")
    private String producer;
    /**
     * 生产商名称
     */
    @ApiModelProperty(value="生产商名称")
    private String producerName;
    /**
     * 制品编号
     */
    @ApiModelProperty(value="制品编号")
    private String zpbh;
    /**
     * 品牌
     */
    @ApiModelProperty(value="品牌")
    private String ppid;
    /**
     * 产地
     */
    @ApiModelProperty(value="产地")
    private String cdid;
    /**
     * 含税进价
     */
    @ApiModelProperty(value="含税进价")
    private BigDecimal hsjj;
    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String memo;
    /**
     * 版本
     */
    @ApiModelProperty(value="版本")
    private String version;
    /**
     * 长度--mm/毫米
     */
    @ApiModelProperty(value="长度--mm/毫米")
    private BigDecimal length;
    /**
     * 宽度--mm/毫米
     */
    @ApiModelProperty(value="宽度--mm/毫米")
    private BigDecimal width;
    /**
     * 高度--mm/毫米
     */
    @ApiModelProperty(value="高度--mm/毫米")
    private BigDecimal heigh;
    /**
     * 毛重--KG/公斤
     */
    @ApiModelProperty(value="毛重--KG/公斤")
    private BigDecimal weightgross;
    /**
     * 净重--mm3/立方毫米
     */
    @ApiModelProperty(value="净重--mm3/立方毫米")
    private BigDecimal weightnet;
    /**
     * 体积--mm3/立方毫米
     */
    @ApiModelProperty(value="体积--mm3/立方毫米")
    private BigDecimal volume;
    /**
     * 批次管理(0 - 不进行/1 - 只有入库/2 - 全部)
     */
    @ApiModelProperty(value="批次管理(0 - 不进行/1 - 只有入库/2 - 全部)")
    private String islot;
    /**
     * 高值
     */
    @ApiModelProperty(value="高值")
    private String isgz;
    /**
     * 是否复用
     */
    @ApiModelProperty(value="是否复用")
    private String isflag2;
    /**
     * 直送商品，Y-直送商品，N-非直送
     */
    @ApiModelProperty(value="直送商品，Y-直送商品，N-非直送")
    private String isflag3;
    /**
     * 是否有图片
     */
    @ApiModelProperty(value="是否有图片")
    private String isflag4;
    /**
     * 是否小数
     */
    @ApiModelProperty(value="是否小数")
    private String isflag5;
    /**
     * 是否强制扫描高值码
     */
    @ApiModelProperty(value="是否强制扫描高值码")
    private String isflag6;
    /**
     * 复用次数
     */
    @ApiModelProperty(value="复用次数")
    private Integer num2;
    /**
     * 大包装单位
     */
    @ApiModelProperty(value="大包装单位")
    private String unitDabz;
    /**
     * 中包装单位
     */
    @ApiModelProperty(value="中包装单位")
    private String unitZhongbz;
    /**
     * 大包装系数
     */
    @ApiModelProperty(value="大包装系数")
    private BigDecimal numDabz;
    /**
     * 中包装系数
     */
    @ApiModelProperty(value="中包装系数")
    private BigDecimal numZhongbz;
    /**
     * 订货单位（默认小包装D大/Z中/X小）
     */
    @ApiModelProperty(value="订货单位（默认小包装D大/Z中/X小）")
    private String unitOrder;
    /**
     * 出库单位（默认小包装D大/Z中/X小）
     */
    @ApiModelProperty(value="出库单位（默认小包装D大/Z中/X小）")
    private String unitSell;
    /**
     * 物料WMS 代码
     */
    @ApiModelProperty(value="物料WMS 代码")
    private String hiscode;
    /**
     * 物料WMS名称
     */
    @ApiModelProperty(value="物料WMS名称")
    private String hisname;
    /**
     * 商品类别总类--药品、耗材、器械、试剂、办公用品 
     */
    @ApiModelProperty(value="商品类别总类--药品、耗材、器械、试剂、办公用品 ")
    private String catid0;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value="创建时间")
	private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private LocalDateTime updateTime;
    /**
     * 供应商id
     */
    @ApiModelProperty(value="供应商id")
    private String supid;
    /**
     * 供应商名称
     */
    @ApiModelProperty(value="供应商名称")
    private String supname;
    /**
     * 配送商id
     */
    @ApiModelProperty(value="配送商id")
    private String pssid;
    /**
     * 配送商名称
     */
    @ApiModelProperty(value="配送商名称")
    private String pssname;
    }
