/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.pig4cloud.pigx.wms.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.wms.entity.InStorageItem;
import com.pig4cloud.pigx.wms.entity.OutStorageItem;
import com.pig4cloud.pigx.wms.mapper.InStorageItemMapper;
import com.pig4cloud.pigx.wms.mapper.OutStorageItemMapper;
import com.pig4cloud.pigx.wms.service.InStorageItemService;
import com.pig4cloud.pigx.wms.service.OutStorageItemService;

/**
 * 出库单行项目信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:36
 */
@Service
public class OutStorageItemServiceImpl extends ServiceImpl<OutStorageItemMapper, OutStorageItem> implements
		OutStorageItemService {

}
