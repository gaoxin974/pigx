package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;


@Data
@TableName("agv_out_queue")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AGV到月台的队列")
public class AgvOutQueue extends Model<AgvOutQueue> {
private static final long serialVersionUID = 1L;


	@TableId(type = IdType.AUTO)
	@ApiModelProperty(value="id")
	private String id;

    @ApiModelProperty(value="托盘编码")
    private String trayNo;

    @ApiModelProperty(value="批号")
    private String batchNo;

    @ApiModelProperty(value="批次")
    private String batchCi;

    @ApiModelProperty(value="商品sku")
    private String sku;

    @ApiModelProperty(value="货主ID")
    private String huozId;

    @ApiModelProperty(value="真实货主ID")
    private String realHuoz;

    @ApiModelProperty(value="库存")
	private Integer stockNum;

	@ApiModelProperty(value="应出数量")
	private Integer mustNum;

	@ApiModelProperty(value="返库位置")
	private String sLocationNo;

	@ApiModelProperty(value="AGV位置")
	private String agvLocationNo;

    @ApiModelProperty(value="所属区域")
    private Integer areaId;

	@ApiModelProperty(value="入库单编号")
	private String inPageNo;

	@ApiModelProperty(value="入库单行项目id")
	private Integer inStorageItemId;

	@ApiModelProperty(value="出库单编号")
	private String outPageNo;

	@ApiModelProperty(value="出库单行项目id")
	private Integer outStorageItemId;

	@ApiModelProperty(value="入库单行项目id")
	private Integer outStorageTpId;

	@ApiModelProperty(value="出库月台位置")
	private String outLocation;

	@ApiModelProperty(value="备货类型")
	private String bhlx;

	@ApiModelProperty(value="队列状态")
	private String queueStatus;

    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value="更新时间")
    private LocalDateTime updateTime;

}
