package com.pig4cloud.pigx.wms.jobservice;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.wms.dto.ERPOutStorageDTO;
import com.pig4cloud.pigx.wms.service.OutStorageService;
import com.pig4cloud.pigx.wms.webtools.GuanYiWebToolsService;
import com.pig4cloud.pigx.wms.webtools.TJWebToolsService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.log.XxlJobLogger;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author: 高欣
 * @Date: 2021/7/30 15:18
 */
@Service
@Slf4j
@RefreshScope
@RequiredArgsConstructor
public class WmsJobService {

	private final OutStorageService outStorageService;

	private final GuanYiWebToolsService guanYiWebToolsService;

	private final TJWebToolsService tjWebToolsService;

	@Value("${test.peizhi}")
	private String peizhi;

	@XxlJob("demoJob")
	public ReturnT demoJob(String param){

		LocalDateTime timeNow = LocalDateTime.now();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss");

		String time = dtf.format(timeNow);

		XxlJobLogger.log("hello xxl-job 任务执行时间是:"+time+"配置文件中属性值:"+peizhi);

		return ReturnT.SUCCESS;
	}

	@XxlJob("autoGetGYOUTsJob")
	public ReturnT autoGetGYOUTsJob(String param){

		LocalDateTime timeNow = LocalDateTime.now();

		LocalDateTime beforeOneHour = timeNow.minusHours(1);

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

		String endTime = dtf.format(timeNow);

		String startTime = dtf.format(beforeOneHour);

		XxlJobLogger.log("autoGetGYOUTsJob xxl-job 任务执行开始时间是:"+startTime+"结束时间是:"+endTime);

		String resopnseJson = guanYiWebToolsService.getDeliverys(startTime,endTime);

		JSONObject jsonObject = JSONUtil.parseObj(resopnseJson);

		boolean success = jsonObject.getBool("success");

		if(success){

			int total = jsonObject.getInt("total");

			if(total > 0){

				outStorageService.handleDSOuts(jsonObject);

				XxlJobLogger.log("autoGetGYOUTsJob xxl-job 成功获取管易出库单 出库单数量:"+total);

				return ReturnT.SUCCESS;

			}else{
				XxlJobLogger.log("autoGetGYOUTsJob xxl-job 成功获取管易出库单但出库单数量为0");
				return ReturnT.SUCCESS;
			}

		}else{

			return ReturnT.FAIL;

		}

	}

	@XxlJob("updateTokenWithDB")
	public ReturnT updateTokenWithDB(String param){

		LocalDateTime timeNow = LocalDateTime.now();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss");

		String time = dtf.format(timeNow);

		XxlJobLogger.log("xxl-job updateTokenWithDB 任务执行时间是:"+time);

		if(tjWebToolsService.updateToken()){
			XxlJobLogger.log("xxl-job updateTokenWithDB 任务执行成功");
			return ReturnT.SUCCESS;

		}else{
			XxlJobLogger.log("xxl-job updateTokenWithDB 任务执行失败");
			return ReturnT.FAIL;
		}


	}

	@XxlJob("autoGetTJOUTsJob")
	public ReturnT autoGetTJOUTsJob(String param){

		LocalDateTime timeNow = LocalDateTime.now();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		String endTime = dtf.format(timeNow);

		XxlJobLogger.log("autoGetGYOUTsJob xxl-job 任务执行开始时间是:"+endTime+"结束时间是:"+endTime);

		Map<String, ERPOutStorageDTO> erpOutStorageDTOMap =  tjWebToolsService.getTJOuts(endTime,endTime);

		if(erpOutStorageDTOMap == null || erpOutStorageDTOMap.size() == 0){
			XxlJobLogger.log("获取T+出库单成功,但数量为0.");
			return ReturnT.SUCCESS;
		}else{

			outStorageService.handleTJOuts(erpOutStorageDTOMap);

			XxlJobLogger.log("获取T+出库单成功.数量是:"+erpOutStorageDTOMap.keySet().size());
			return ReturnT.SUCCESS;
		}
	}

}
