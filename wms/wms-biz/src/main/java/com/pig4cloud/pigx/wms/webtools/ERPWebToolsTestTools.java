package com.pig4cloud.pigx.wms.webtools;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.pig4cloud.pigx.wms.dto.ERPOutStorageDTO;
import com.pig4cloud.pigx.wms.dto.ERPOutStorageItemDTO;
import com.pig4cloud.pigx.wms.dto.ERPOutStorageTpDTO;
import com.pig4cloud.pigx.wms.entity.ERPInStorageDTO;
import com.pig4cloud.pigx.wms.entity.ERPInStorageItemDTO;
import com.pig4cloud.pigx.wms.entity.ERPInStorageTpDTO;

import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ERPWebToolsTestTools {

	private static String inUrl = "http://1706s35y40.imwork.net/unmannedWarehouse/inStorage/callBack";

	private static String outUrl = "http://1706s35y40.imwork.net/unmannedWarehouse/outStorage/callBack";

	public static void main(String[] args) {

		inStorageCallBackTest();
		outStorageCallBackTest();

	}

	private static void inStorageCallBackTest() {
		RestTemplate restTemplate = new RestTemplate();

		ERPInStorageDTO erpInStorageDTO = new ERPInStorageDTO();
		List<ERPInStorageItemDTO> erpInStorageItemDTOList = new ArrayList<>();

		ERPInStorageItemDTO erpInStorageItemDTO = new ERPInStorageItemDTO();
		erpInStorageItemDTO.setSku("H0000000001");
		erpInStorageItemDTO.setProductDate(LocalDateTime.now().toString());
		erpInStorageItemDTO.setNote("入库单关闭原因wmstest");
		erpInStorageItemDTO.setMinUnitName("袋");
		erpInStorageItemDTO.setLineNumber("ERP0000000001");
		erpInStorageItemDTO.setAmount(100);
		erpInStorageItemDTO.setBatchNo("P00001");
		erpInStorageItemDTO.setEffDate(LocalDateTime.now().toString());

		List<ERPInStorageTpDTO> erpInStorageTpDTOS = new ArrayList<>();
		ERPInStorageTpDTO erpInStorageTpDTO = new ERPInStorageTpDTO();
		erpInStorageTpDTO.setAmount(100);
		erpInStorageTpDTO.setItemId("1");
		erpInStorageTpDTO.setLocation("KW10001");
		erpInStorageTpDTO.setSku("H0000000001");
		erpInStorageTpDTO.setUniqueCode("TP000001");
		erpInStorageTpDTOS.add(erpInStorageTpDTO);
		erpInStorageItemDTO.setErpInStorageTpDTOList(erpInStorageTpDTOS);

		erpInStorageItemDTOList.add(erpInStorageItemDTO);

		erpInStorageDTO.setWlzxCode("6001");
		erpInStorageDTO.setHuozId("1001");
		erpInStorageDTO.setInPageNo("FB-I202104100001");
		erpInStorageDTO.setInBillNumber("ERP202104100001");
		erpInStorageDTO.setInType("1");
		erpInStorageDTO.setInDate(LocalDateTime.now().toString());
		erpInStorageDTO.setErpInStorageItemDTOList(erpInStorageItemDTOList);

		String body = JSONUtil.toJsonStr(erpInStorageDTO);

		log.info("ERP上架回调请求体是:"+body);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(body, headers);
		ResponseEntity<String> result = restTemplate.exchange(inUrl, HttpMethod.POST,entity,String.class);

		log.info("ERP上架回调结果的JSON是:"+result.getBody());
	}

	private static void outStorageCallBackTest() {

		RestTemplate restTemplate = new RestTemplate();

		ERPOutStorageDTO erpOutStorageDTO = new ERPOutStorageDTO();
		List<ERPOutStorageItemDTO> erpOutStorageItemDTOList = new ArrayList<>();
		ERPOutStorageItemDTO erpOutStorageItemDTO = new ERPOutStorageItemDTO();
		List<ERPOutStorageTpDTO> erpOutStorageTpDTOS = new ArrayList<>();
		ERPOutStorageTpDTO erpOutStorageTpDTO = new ERPOutStorageTpDTO();

		erpOutStorageTpDTO.setAmount(100);
		erpOutStorageTpDTO.setItemId("1");
		erpOutStorageTpDTO.setLocation("KW10001");
		erpOutStorageTpDTO.setSku("TP000001");
		erpOutStorageTpDTO.setUniqueCode("TP000001");
		erpOutStorageTpDTOS.add(erpOutStorageTpDTO);

		erpOutStorageItemDTO.setSku("H0000000001");
		erpOutStorageItemDTO.setMinUnitName("KG");
		erpOutStorageItemDTO.setLineNumber("ERP0000000001");
		erpOutStorageItemDTO.setAmount(100);
		erpOutStorageItemDTO.setBatchNo("P00001");
		erpOutStorageItemDTO.setErpOutStorageTpDTOList(erpOutStorageTpDTOS);
		erpOutStorageItemDTOList.add(erpOutStorageItemDTO);

		erpOutStorageDTO.setWlzxCode("6001");
		erpOutStorageDTO.setHuozId("1001");
		erpOutStorageDTO.setOutPageNo("FB-O202104100001");
		erpOutStorageDTO.setOutBillNumber("ERP202104100001");
		erpOutStorageDTO.setOutType("2");
		erpOutStorageDTO.setStorageId(1);
		erpOutStorageDTO.setLicenceNum("沪A12345");
		erpOutStorageDTO.setLocationNo("T01");
		erpOutStorageDTO.setDriverName("张三");

		erpOutStorageDTO.setErpOutStorageItemDTOList(erpOutStorageItemDTOList);

		String body = JSONUtil.toJsonStr(erpOutStorageDTO);

		log.info("ERP下架回调请求体是:"+body);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(body, headers);
		ResponseEntity<String> result = restTemplate.exchange(outUrl, HttpMethod.POST,entity,String.class);

		log.info("ERP下架回调结果的JSON是:"+result.getBody());
	}
}
