/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.data.datascope.PigxBaseMapper;
import com.pig4cloud.pigx.wms.dto.SLocationDTO;
import com.pig4cloud.pigx.wms.dto.StockQueryDTO;
import com.pig4cloud.pigx.wms.entity.SLocation;
import com.pig4cloud.pigx.wms.vo.SLocationPageVO;

import com.pig4cloud.pigx.wms.vo.StockQueryExcelVO;
import com.pig4cloud.pigx.wms.vo.StockQueryPageVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 
 *
 * @author gaoxin
 * @date 2021-04-08 09:50:38
 */
@Mapper
public interface SLocationMapper extends PigxBaseMapper<SLocation> {

	IPage<SLocationPageVO> pageSLocationForVO(Page page, @Param("sLocationDTO") SLocationDTO sLocationDTO);

	IPage<StockQueryPageVO> stockQueryPage(Page page, @Param("stockQueryDTO") StockQueryDTO stockQueryDTO);

	List<StockQueryExcelVO> stockQueryExcel(@Param("stockQueryDTO")StockQueryDTO stockQueryDTO);
}
