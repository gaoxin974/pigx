package com.pig4cloud.pigx.wms.controller;

import java.time.LocalDateTime;
import java.util.List;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.excel.annotation.ResponseExcel;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.wms.dto.SLocationDTO;
import com.pig4cloud.pigx.wms.dto.StockQueryDTO;
import com.pig4cloud.pigx.wms.entity.SLocation;
import com.pig4cloud.pigx.wms.entity.StockArea;
import com.pig4cloud.pigx.wms.service.StockAreaService;
import com.pig4cloud.pigx.wms.vo.*;
import com.pig4cloud.pigx.wms.service.OwnerService;
import com.pig4cloud.pigx.wms.service.SLocationService;
import com.pig4cloud.pigx.wms.service.SkuService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 
 *
 * @author gaoxin
 * @date 2021-04-08 09:50:38
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/slocation" )
@Api(value = "slocation", tags = "库位管理")
public class SLocationController {

    private final SLocationService sLocationService;

	private final OwnerService ownerService;

	private final StockAreaService stockAreaService;

	private final SkuService skuService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param sLocation 
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    //@PreAuthorize("@pms.hasPermission('wms_slocation_view')" )
    public R getSLocationPage(Page page, SLocation sLocation) {
        return R.ok(sLocationService.page(page, Wrappers.query(sLocation)));
    }

	@ApiOperation(value = "根据库区查询", notes = "根据库区查询")
	@GetMapping("/listBySAID/{saId}" )
	//@PreAuthorize("@pms.hasPermission('wms_slocation_view')" )
	public R getSLocationBySAID(@PathVariable int saId) {

		return R.ok(sLocationService.list(Wrappers.<SLocation>lambdaQuery().eq(SLocation::getAreaId,saId)));
	}

	@ApiOperation(value = "按层查询库区情况", notes = "按层查询库区情况")
	@GetMapping("/listStockAreaForVO/{tier}" )
	public R listStockAreaForVO(@PathVariable int tier) {

		if(StringUtils.isEmpty(String.valueOf(tier))){

			return R.failed("参数为空!wms无法处理...");

		}

		List<StockAreaForVO> stockAreaListForVO =
				stockAreaService.listStockAreaForVO(tier);

		return R.ok(stockAreaListForVO);
	}

	@ApiOperation(value = "按区域和层级查询库区情况", notes = "按区域和层级查询库区情况")
	@GetMapping("/listStockAreaByAreaIdForVO/{areaId}/{tier}" )
	public R listStockAreaByAreaIdForVO(@PathVariable int areaId,@PathVariable int tier) {

    	if(StringUtils.isEmpty(String.valueOf(areaId))){

    		return R.failed("区域参数为空!wms无法处理...");

		}

		if(StringUtils.isEmpty(String.valueOf(tier))){

			return R.failed("层级参数为空!wms无法处理...");

		}

		List<StockArea> stockAreaList =
				stockAreaService.listStockAreaByAreaIdForVO(areaId,tier);

		return R.ok(stockAreaList);
	}

	@ApiOperation(value = "查询库区详情", notes = "查询库区详情")
	@GetMapping("/getStockAreaDetailVO/{stockAreaNo}" )
	public R getStockAreaDetailVO(@PathVariable String stockAreaNo) {

		if(StringUtils.isEmpty(String.valueOf(stockAreaNo))){

			return R.failed("参数为空!wms无法处理...");

		}

		StockAreaDetailForVO stockAreaDetailForVO =
				stockAreaService.getStockAreaDetailVO(stockAreaNo);

		return R.ok(stockAreaDetailForVO);
	}

	@ApiOperation(value = "分页查询小库区", notes = "分页查询小库区")
	@GetMapping("/stockArea/page" )
	public R getStockAreaPage(Page page, StockArea stockArea) {
		return R.ok(stockAreaService.page(page, Wrappers.query(stockArea)));
	}

	@ApiOperation(value = "修改小库区激活状态", notes = "修改小库区激活状态")
	@PutMapping("/putStockAreaStatus/{stockAreaNo}/{activatedStatus}" )
	public R putStockAreaStatus(@PathVariable String stockAreaNo, @PathVariable String activatedStatus) {
		return R.ok(stockAreaService.activatedStatus(stockAreaNo,activatedStatus));
	}

	@ApiOperation(value = "根据库区ID和货主ID查询", notes = "根据库区ID和货主ID查询")
	@GetMapping("/listBySAID/{saId}/{huozId}" )
	//@PreAuthorize("@pms.hasPermission('wms_slocation_view')" )
	public R getSLocationBySAIDandHZ(@PathVariable int saId,@PathVariable int huozId) {

		return R.ok(
				sLocationService.list(Wrappers.<SLocation>lambdaQuery().eq(SLocation::getAreaId,saId)
						.eq(SLocation::getRealHuoz,huozId))
		);
	}

    /**
     * 通过id查询
     * @param sLocationNo id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{sLocationNo}" )
    //@PreAuthorize("@pms.hasPermission('wms_slocation_view')" )
    public R getById(@PathVariable("sLocationNo" ) String sLocationNo) {
        return R.ok(sLocationService.getById(sLocationNo));
    }

	/**
	 * 通过id查询
	 * @param sLocationNo id
	 * @return R
	 */
	@ApiOperation(value = "通过id查询详情", notes = "通过id查询详情")
	@GetMapping("/view/{sLocationNo}" )
	//@PreAuthorize("@pms.hasPermission('wms_slocation_view')" )
	public R getViewById(@PathVariable("sLocationNo" ) String sLocationNo) {

		SLocation sLocation = sLocationService.getById(sLocationNo);

		SLocationVO sLocationVO = new SLocationVO();

		BeanUtils.copyProperties(sLocation, sLocationVO);

		sLocationVO.setHuozName(ownerService.getById(sLocation.getHuozId()).getHuozName());

		sLocationVO.setRealHuozName(ownerService.getById(sLocation.getRealHuoz()).getHuozName());

		sLocationVO.setMinUnit(skuService.getById(sLocation.getSku()).getUnit());

		return R.ok(sLocationVO);
	}

    /**
     * 新增
     * @param sLocation 
     * @return R
     */
    @ApiOperation(value = "新增", notes = "新增")
    @SysLog("新增" )
    @PostMapping
    //@PreAuthorize("@pms.hasPermission('wms_slocation_add')" )
    public R save(@RequestBody SLocation sLocation) {
        return R.ok(sLocationService.save(sLocation));
    }

    /**
     * 修改
     * @param sLocation 
     * @return R
     */
    @ApiOperation(value = "修改", notes = "修改")
    @SysLog("修改" )
    @PutMapping
    //@PreAuthorize("@pms.hasPermission('wms_slocation_edit')" )
    public R updateById(@RequestBody SLocation sLocation) {
		sLocation.setUpdateTime(LocalDateTime.now());
        return R.ok(sLocationService.updateById(sLocation));
    }

    /**
     * 通过id删除
     * @param sLocationNo id
     * @return R
     */
    @ApiOperation(value = "通过id删除", notes = "通过id删除")
    @SysLog("通过id删除" )
    @DeleteMapping("/{sLocationNo}" )
    //@PreAuthorize("@pms.hasPermission('wms_slocation_del')" )
    public R removeById(@PathVariable String sLocationNo) {
        return R.ok(sLocationService.removeById(sLocationNo));
    }

	@ApiOperation(value = "分页查询ForVO", notes = "分页查询ForVO")
	@GetMapping("/pageForVO" )
	//@PreAuthorize("@pms.hasPermission('wms_slocation_view')" )
	public R pageSLocationForVO(Page page, SLocationDTO sLocationDTO) {

		IPage<SLocationPageVO> sLocationPageVOPage = sLocationService.pageSLocationForVO(page, sLocationDTO);

		return R.ok(sLocationPageVOPage);
	}

	@ApiOperation(value = "分页查询库存信息", notes = "分页查询库存信息")
	@GetMapping("/pageStocks" )
	public R pageStockForVO(Page page, StockQueryDTO stockQueryDTO) {

		IPage<StockQueryPageVO> stockQueryPageVO = sLocationService.stockQueryPage(page, stockQueryDTO);

		return R.ok(stockQueryPageVO);
	}

	@ResponseExcel
	@ApiOperation(value = "导出库存信息", notes = "导出库存信息")
	@GetMapping("/export" )
	public List export(StockQueryDTO stockQueryDTO) {

		log.info("收到前端导出库存信息请求:"+ JSONUtil.toJsonStr(stockQueryDTO));

		List<StockQueryExcelVO> stockQueryExcelVOs = sLocationService.stockQueryExcel(stockQueryDTO);

		log.info("返回库存信息为:"+ JSONUtil.toJsonStr(stockQueryExcelVOs));

		return stockQueryExcelVOs;
	}

}
