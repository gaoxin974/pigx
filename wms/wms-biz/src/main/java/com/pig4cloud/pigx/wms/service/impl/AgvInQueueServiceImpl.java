package com.pig4cloud.pigx.wms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.wms.entity.AgvInQueue;
import com.pig4cloud.pigx.wms.mapper.AgvInQueueMapper;
import com.pig4cloud.pigx.wms.service.AgvInQueueService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AgvInQueueServiceImpl extends ServiceImpl<AgvInQueueMapper, AgvInQueue> implements AgvInQueueService {

}
