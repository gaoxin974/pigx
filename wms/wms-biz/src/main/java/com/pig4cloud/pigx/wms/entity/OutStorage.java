/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 出库单信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:32
 */
@Data
@TableName("out_storage")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "出库单信息")
public class OutStorage extends Model<OutStorage> {
private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value="出库单主键 外部出库单编号")
    private String outPageNo;

    @ApiModelProperty(value="wms单据号")
    private String wmsBillNo;

	@ApiModelProperty(value="ERP上游出库单编号")
	private String outBillNumber;

	@ApiModelProperty(value="波次单号")
	private String packageNo;

	@ApiModelProperty(value="波次单状态")
	private String packageStatus;

    @ApiModelProperty(value="库区ID")
    private Integer storageId;

    @ApiModelProperty(value="货主ID")
    private String huozId;

    @ApiModelProperty(value="物流中心ID")
    private String wlzxCode;

    @ApiModelProperty(value="车辆信息编码")
    private String vehicleInformation;

	@ApiModelProperty(value="车牌照号")
	private String licenceNum;

	@ApiModelProperty(value="司机姓名")
	private String driverName;

    @ApiModelProperty(value="出库时间")
    private LocalDateTime outDate;

	@ApiModelProperty(value="站台编号，出库位置")
	private String locationNo;

    @ApiModelProperty(value="1 电商出库 2 ERP产线出库 3 T2C出库")
    private String outType;

    @ApiModelProperty(value="0:未出库 1:待出库 2:出库中 3:已完成 4:已取消 5:补货中")
    private String status;

	@ApiModelProperty(value="是否是单品规 0:不是 1:是")
	private String isOneItem;

	@ApiModelProperty(value="领取人")
	private String operator;

	@ApiModelProperty(value="任务id")
	private String taskId;

    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value="修改时间")
    private LocalDateTime updateTime;
}
