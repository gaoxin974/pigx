package com.pig4cloud.pigx.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pigx.wms.entity.CheckErrorStockArea;

public interface CheckErrorStockAreaService extends IService<CheckErrorStockArea> {

}
