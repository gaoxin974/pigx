package com.pig4cloud.pigx.wms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.wms.entity.CheckSheetItem;
import com.pig4cloud.pigx.wms.mapper.CheckSheetItemMapper;
import com.pig4cloud.pigx.wms.service.CheckSheetItemService;
import org.springframework.stereotype.Service;


@Service
public class CheckSheetItemServiceImpl extends ServiceImpl<CheckSheetItemMapper, CheckSheetItem> implements CheckSheetItemService {

}
