
package com.pig4cloud.pigx.wms.service.impl;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.common.security.util.SecurityUtils;
import com.pig4cloud.pigx.wms.dto.ERPCancelDTO;
import com.pig4cloud.pigx.wms.dto.ERPSystemReturnDTO;
import com.pig4cloud.pigx.wms.dto.InStoragePageDTO;
import com.pig4cloud.pigx.wms.dto.WCSInvInDTO;
import com.pig4cloud.pigx.wms.dto.WCSInvOutDTO;
import com.pig4cloud.pigx.wms.dto.WGInStorageForPDADTO;
import com.pig4cloud.pigx.wms.entity.*;
import com.pig4cloud.pigx.wms.mapper.InStorageMapper;
import com.pig4cloud.pigx.wms.mapper.WcsWmsTrxMapper;
import com.pig4cloud.pigx.wms.service.ERPCallBackService;
import com.pig4cloud.pigx.wms.service.GoodSkuService;
import com.pig4cloud.pigx.wms.service.InStorageItemService;
import com.pig4cloud.pigx.wms.service.InStorageService;
import com.pig4cloud.pigx.wms.service.InStorageTpService;
import com.pig4cloud.pigx.wms.service.SLocationService;
import com.pig4cloud.pigx.wms.service.StockAreaService;
import com.pig4cloud.pigx.wms.service.TierConfigService;
import com.pig4cloud.pigx.wms.service.WcsWmsTrxService;
import com.pig4cloud.pigx.wms.vo.InStorageItemVO;
import com.pig4cloud.pigx.wms.vo.InStoragePageVO;
import com.pig4cloud.pigx.wms.vo.InStorageTPVO;
import com.pig4cloud.pigx.wms.vo.PDADSInStorageDetailVO;
import com.pig4cloud.pigx.wms.vo.PDADSInStorageVO;
import com.pig4cloud.pigx.wms.vo.PDAWGInStorageDetailVO;
import com.pig4cloud.pigx.wms.vo.PDAWGInStorageVO;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.util.RandomUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 入库单信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:32
 */
@Slf4j
@Service
@AllArgsConstructor
public class InStorageServiceImpl extends ServiceImpl<InStorageMapper, InStorage> implements InStorageService {

	private OutStorageServiceImpl outStorageService;

	private InStorageItemService inStorageItemService;

	private InStorageTpService inStorageTpService;

	private SLocationService sLocationService;

	private GoodSkuService goodSkuService;

	private WcsWmsTrxMapper wcsWmsTrxMapper;

	private WcsWmsTrxService wcsWmsTrxService;

	private StockAreaService stockAreaService;

	private TierConfigService tierConfigService;

	private final ERPCallBackService erpCallBackService;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveALL(ERPInStorageDTO erpInStorageDTO) {

//		ERPSystemReturnDTO erpSystemReturnDTO = new ERPSystemReturnDTO();
//		erpSystemReturnDTO.setSuccess(true);
//		erpSystemReturnDTO.setMessage("ERP下发WMS请求处理成功.");

		InStorage inStorage = new InStorage();

		BeanUtils.copyProperties(erpInStorageDTO, inStorage);

		String inDateStr = erpInStorageDTO.getInDate();
		DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime inDateLD = LocalDateTime.parse(inDateStr, dtFormatter);

		inStorage.setInDate(inDateLD);

		LocalDateTime timeNow = LocalDateTime.now();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

		String ymdhms = dtf.format(timeNow);

//		String sku = erpInStorageDTO.getErpInStorageItemDTOList().get(0).getSku();

		int randomInt = RandomUtil.randomInt(1000, 9999);

		String preWmsNo = "未知";

		if ("1".equals(inStorage.getInType())) {

			preWmsNo = "RKA";

		} else if ("3".equals(inStorage.getInType())) {
			preWmsNo = "RKB";

		} else if ("2".equals(inStorage.getInType())) {
			preWmsNo = "RKC";

		} else if ("4".equals(inStorage.getInType())) {
			preWmsNo = "RKD";

		} else if ("5".equals(inStorage.getInType())) {
			preWmsNo = "RKE";

		}

		String wmsNO = preWmsNo + ymdhms + randomInt;

		inStorage.setWmsBillNo(wmsNO);

		this.save(inStorage);

		List<ERPInStorageItemDTO> erpInStorageItemDTOList = erpInStorageDTO.getErpInStorageItemDTOList();

		for (int i = 0; i < erpInStorageItemDTOList.size(); i++) {

			InStorageItem inStorageItem = new InStorageItem();

			ERPInStorageItemDTO erpInStorageItemDTO = erpInStorageItemDTOList.get(i);

			BeanUtils.copyProperties(erpInStorageItemDTO, inStorageItem);

			String productDateStr = erpInStorageItemDTO.getProductDate();
			LocalDateTime productDateLD = LocalDateTime.parse(productDateStr, dtFormatter);

			inStorageItem.setProductDate(productDateLD);

			String effDateStr = erpInStorageItemDTO.getEffDate();
			LocalDateTime effDateLD = LocalDateTime.parse(effDateStr, dtFormatter);

			inStorageItem.setEffDate(effDateLD);

			inStorageItem.setWmsBillNo(wmsNO);

			inStorageItem.setWmsItemNo(wmsNO + "_" + i);

			inStorageItemService.save(inStorageItem);

			List<ERPInStorageTpDTO> erpInStorageTpDTOList = erpInStorageItemDTO.getErpInStorageTpDTOList();

			//如果不是外购入库就保存托盘信息
			if (!"3".equals(inStorage.getInType())) {

				for (int j = 0; j < erpInStorageTpDTOList.size(); j++) {

					InStorageTp inStorageTp = new InStorageTp();

					ERPInStorageTpDTO erpInStorageTpDTO = erpInStorageTpDTOList.get(i);

					BeanUtils.copyProperties(erpInStorageTpDTO, inStorageTp);

					int itemId = inStorageItem.getInStorageItemId();

					inStorageTp.setInStorageItemId(itemId);

					inStorageTp.setSku(erpInStorageTpDTO.getSku());

					inStorageTp.setWmsBillNo(wmsNO);

					inStorageTp.setAmount(erpInStorageTpDTO.getAmount());

					inStorageTpService.save(inStorageTp);

				}
			}
		}

	}

	@Override
	public IPage<PDADSInStorageVO> pageDSInStorageForPDA(Page page, String order) {

		return baseMapper.pageDSInStorageForPDA(page, order);

	}

	@Override
	public PDADSInStorageDetailVO getDSInStorageDetailByTPForPDA(String uniqueCode) {

		return baseMapper.getDSInStorageDetailByTPForPDA(uniqueCode);

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void postDSInStorageDetailForPDA(String fromLocation, String uniqueCode) throws Exception {

		synchronized (InStorageServiceImpl.class){
			log.info("开始处理电商入库单,获取并锁定入库事务锁。。。");


		InStorageTp inStorageTp =
				inStorageTpService.getOne(Wrappers.<InStorageTp>query().lambda()
						.eq(InStorageTp::getUniqueCode, uniqueCode).eq(InStorageTp::getStatus, "0"));

		InStorageItem inStorageItem = inStorageItemService.getOne(Wrappers.<InStorageItem>query().lambda()
				.eq(InStorageItem::getInStorageItemId, inStorageTp.getInStorageItemId()));

		InStorage inStorage = this.getOne(Wrappers.<InStorage>query().lambda()
				.eq(InStorage::getWmsBillNo, inStorageItem.getWmsBillNo()));

		//获取库区
		StockArea stockArea = getStockArea(inStorageItem, inStorage,"4");


		//查询入库单中sku在不在畅销sku中
		GoodSku goodSku = goodSkuService.getOne(Wrappers.<GoodSku>query().lambda()
				.eq(GoodSku::getSku, inStorageItem.getSku()));

		//查询AGV库区中该电商入库单中sku所有库位信息
		List<SLocation> sLocationList =
				sLocationService.list(Wrappers.<SLocation>query().lambda().eq(SLocation::getAreaId, "2")
						.eq(SLocation::getIsUsed, "1")
						.eq(SLocation::getLocationStatus, "5")
						.eq(SLocation::getSku, inStorageItem.getSku()));
		int minAmount = 0;
		if (goodSku != null) {
			minAmount = goodSku.getMinStock();
		}
		int agvSum = sLocationList.stream().mapToInt(SLocation::getStockNum).sum();
		//如果畅销sku中没有此sku或者AGV库区中该sku库存数大于畅销sku最小库存数，就从立库区找位置
		if (goodSku == null || agvSum > minAmount) {

			//从立库区找位置
			SLocation toLocation = sLocationService.getOne(Wrappers.<SLocation>query().lambda()
					.eq(SLocation::getAreaId, "1").eq(SLocation::getIsUsed, "0").eq(SLocation::getLocationStatus, "1")
					.eq(SLocation::getStockAreaNo, stockArea.getStockAreaNo())
					.eq(SLocation::getStatus, "1").orderByAsc(SLocation::getSOrder).last("limit 1"));

			if (toLocation == null) {

				throw new Exception("未找到合适电商入库的库位信息");
			}

			DSinStorageHandler(fromLocation, toLocation.getSLocationNo(), inStorage, inStorageTp, stockArea.getStockAreaNo());

		} else if (agvSum < minAmount) {

			//从AGV区域找库位
			SLocation toLocation = sLocationService.getOne(Wrappers.<SLocation>query().lambda()
					.eq(SLocation::getAreaId, "2").eq(SLocation::getIsUsed, "0").eq(SLocation::getLocationStatus, "1")
					.eq(SLocation::getStatus, "1").orderByAsc(SLocation::getSOrder).last("limit 1"));
			if (toLocation == null) {

				throw new Exception("未找到合适电商入库的库位信息");
			}

			DSinStorageHandler(fromLocation, toLocation.getSLocationNo(), inStorage, inStorageTp, null);

		}
		log.info("处理电商入库单完成，正在释放入库事务锁。。。");
		}
	}

	@Override
	public IPage<PDAWGInStorageVO> pageWGInStorageForPDA(Page page, String wmsBillNo, String sort) {

		return baseMapper.pageWGInStorageForPDA(page, wmsBillNo, sort);

	}

	@Override
	public IPage<InStoragePageVO> getInStoragePageForVO(Page page, InStoragePageDTO inStoragePageDTO) {
		return baseMapper.getInStoragePageForVO(page, inStoragePageDTO);
	}

	@Override
	public List<InStorageItemVO> listItemForVO(String wmsBillNo) {
		return baseMapper.listItemForVO(wmsBillNo);
	}

	@Override
	public List<InStorageTPVO> listInStorageTP(String wmsBillNo) {
		return baseMapper.listInStorageTP(wmsBillNo);
	}

	@Override
	public List<PDAWGInStorageDetailVO> listWGInStorageDetailForPDA(String wmsBillNo, String inStorageItemId) {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();

		return baseMapper.pageWGInStorageDetailForPDA(username, wmsBillNo, inStorageItemId);

	}

	@Override
	public PDADSInStorageDetailVO getWGInStorageDetailForPDA(String wmsBillNo, String inStorageItemId) {

		return baseMapper.getWGInStorageDetailForPDA(wmsBillNo, inStorageItemId);

	}

	@Override
	public boolean cancelBill(ERPCancelDTO erpCancelDTO) {

		InStorage inStorage = this.getOne(Wrappers.<InStorage>query().lambda()
				.eq(InStorage::getInPageNo, erpCancelDTO.getPageNo()).eq(InStorage::getStatus, "0"));

		//获取时分秒
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
		LocalTime localTime = LocalTime.now();
		String hms = formatter.format(localTime);

		if (inStorage != null) {
			//如果找到符合条件的入库单，就把入库单置为已取消 "3"
			//修改主键后缀加入canceled+时分秒
			this.update(Wrappers.<InStorage>update().lambda().set(InStorage::getInPageNo, inStorage.getInPageNo() + "_canceled" + hms)
					.set(InStorage::getStatus, "3").eq(InStorage::getInPageNo, inStorage.getInPageNo()));
			//把入库单下所有托盘状态变为3已取消
			log.info("把此wms入库单下所有托盘状态置为已取消 wms:" + inStorage.getWmsBillNo());
			List<InStorageTp> inStorageTpList = inStorageTpService.list(Wrappers.<InStorageTp>query().lambda()
					.eq(InStorageTp::getWmsBillNo, inStorage.getWmsBillNo()));

			for (InStorageTp inStorageTp : inStorageTpList) {

				inStorageTp.setStatus("3");
				inStorageTpService.updateById(inStorageTp);

			}

			return true;
		} else {
			//入库单如果没找到就查找出库单
			OutStorage outStorage = outStorageService.getOne(Wrappers.<OutStorage>query().lambda()
					.eq(OutStorage::getOutPageNo, erpCancelDTO.getPageNo()).eq(OutStorage::getStatus, "0"));
			if (outStorage != null) {

				//修改主键后缀加入canceled+时分秒
				outStorageService.update(Wrappers.<OutStorage>update().lambda()
						.set(OutStorage::getOutPageNo, outStorage.getOutPageNo() + "_canceled" + hms)
						.set(OutStorage::getStatus, "4").eq(OutStorage::getOutPageNo, outStorage.getOutPageNo()));
				return true;
			} else {
				//如果没找到此编号的出库单返回false
				return false;
			}
		}
	}

	@Override
	public boolean getKuangForDS(String toLocation) {

		StockArea stockArea = stockAreaService.getStockAreaForTrayOut();

		//获取启用的托盘区已使用的已放置托盘的排序级别最高的库位编号
		SLocation fromSLocation =
				sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
						.eq(SLocation::getAreaId, 3)
						.eq(SLocation::getIsUsed, 1)
						.eq(SLocation::getLocationStatus, "2")
						.eq(SLocation::getStockAreaNo, stockArea.getStockAreaNo())
						.orderByDesc(SLocation::getSOrder).last("limit 1"));
		if (fromSLocation == null) {
			log.error("ERP空托获取失败,没有可使用的托盘,无法运送空托盘...");
			return false;

		} else {

			log.info("托盘出库更新库位" + fromSLocation.getSLocationNo() + "为使用中状态");
			fromSLocation.setIsUsed("2");
			fromSLocation.setUpdateTime(LocalDateTime.now());
			sLocationService.updateById(fromSLocation);

			log.info("更新库区" + stockArea.getStockAreaNo() + "为锁定状态");
			stockArea.setStatus("1");
			stockArea.setLockStatus("1");
			stockArea.setUpdateTime(LocalDateTime.now());
			stockAreaService.updateById(stockArea);

			//"调用wcs出库接口,使空框出库到指定地点:"+toLocation
			WCSInvOutDTO wcsInvOutDTO = new WCSInvOutDTO();
			String taskId = "wmstrx" + LocalDateTime.now()
					.format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_MS_PATTERN));
			wcsInvOutDTO.setTaskId(taskId);
			wcsInvOutDTO.setInvOutType("7");
			wcsInvOutDTO.setOutLoc(fromSLocation.getSLocationNo());
			wcsInvOutDTO.setStockNo(fromSLocation.getTrayNo());
			wcsInvOutDTO.setLocDesti(toLocation);
			wcsInvOutDTO.setMemoInfo1("erp调用wcs出库接口,使空框(托)出库到指定地点:" + toLocation);


			if (!wcsWmsTrxService.handleWCSInvOut(wcsInvOutDTO)) {
				throw new RuntimeException("调用WCS出库接口异常!!!");
			}

			WcsWmsTrx wcsWmsTrx = new WcsWmsTrx();
			wcsWmsTrx.setWmsTrxId(taskId);
			wcsWmsTrx.setTrxType("7");
			wcsWmsTrx.setFromLocation(fromSLocation.getSLocationNo());
			wcsWmsTrx.setToLocation(toLocation);
			wcsWmsTrx.setTrayNo(fromSLocation.getTrayNo());
			wcsWmsTrxMapper.insert(wcsWmsTrx);

			return true;
		}
	}

	@Override
	public void delForSave(String inPageNo) {

		InStorage inStorage = this.getById(inPageNo);
		this.removeById(inStorage.getInPageNo());

		List<InStorageItem> inStorageItems = inStorageItemService.list(Wrappers.<InStorageItem>query().lambda()
				.eq(InStorageItem::getWmsBillNo, inStorage.getWmsBillNo()));
		for (InStorageItem inStorageItem : inStorageItems) {

			List<InStorageTp> inStorageTpList = inStorageTpService.list(Wrappers.<InStorageTp>query().lambda()
					.eq(InStorageTp::getWmsBillNo, inStorage.getWmsBillNo()));
			inStorageItemService.removeById(inStorageItem.getInStorageItemId());
			for (InStorageTp inStorageTp : inStorageTpList) {

				inStorageTpService.removeById(inStorageTp.getInStorageTpId());

			}

		}

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void postWGInStorageDetailForPDA(WGInStorageForPDADTO wgInStorageForPDADTO) throws Exception {

		synchronized (InStorageServiceImpl.class){
			log.info("开始处理外购入库单,获取并锁定入库事务锁,wms_bill_no:" + wgInStorageForPDADTO.getWmsBillNo());

		InStorageTp inStorageTpCheck = inStorageTpService.getOne(Wrappers.<InStorageTp>query().lambda()
				.eq(InStorageTp::getInStorageItemId, wgInStorageForPDADTO.getInStorageItemId())
				.eq(InStorageTp::getUniqueCode, wgInStorageForPDADTO.getUniqueCode()));

		if (null != inStorageTpCheck) {

			throw new Exception("找到同一外购入库单提交重复托盘的情况,托盘号:" + wgInStorageForPDADTO.getUniqueCode());

		}

		InStorageItem inStorageItem = inStorageItemService.getOne(Wrappers.<InStorageItem>query().lambda()
				.eq(InStorageItem::getInStorageItemId, wgInStorageForPDADTO.getInStorageItemId()));

		InStorage inStorage = this.getOne(Wrappers.<InStorage>query().lambda()
				.eq(InStorage::getWmsBillNo, inStorageItem.getWmsBillNo()));


		StockArea stockArea = getStockArea(inStorageItem, inStorage,"8");

		//从立库区找位置
		SLocation toLocation = sLocationService.getOne(Wrappers.<SLocation>query().lambda()
				.eq(SLocation::getAreaId, "1").eq(SLocation::getIsUsed, "0").eq(SLocation::getLocationStatus, "1")
				.eq(SLocation::getStockAreaNo, stockArea.getStockAreaNo())
				.eq(SLocation::getStatus, "1").orderByAsc(SLocation::getSOrder).last("limit 1"));

		if (toLocation == null) {

			throw new Exception("未找到合适外购入库的库位信息");
		}

		InStorageTp inStorageTp = new InStorageTp();

		int itemId = inStorageItem.getInStorageItemId();

		inStorageTp.setInStorageItemId(itemId);
		inStorageTp.setSku(inStorageItem.getSku());
		inStorageTp.setWmsBillNo(inStorageItem.getWmsBillNo());
		inStorageTp.setUniqueCode(wgInStorageForPDADTO.getUniqueCode());
		inStorageTp.setSku(wgInStorageForPDADTO.getSku());
		inStorageTp.setAmount(wgInStorageForPDADTO.getAmount());
		inStorageTp.setLocation(toLocation.getSLocationNo());
		inStorageTp.setOperator(SecurityUtils.getUser().getUsername());
		inStorageTp.setStatus("1");

		inStorageTpService.save(inStorageTp);

		WGinStorageHandler(wgInStorageForPDADTO.getFromLocation(), toLocation.getSLocationNo(), inStorage,
				inStorageTp, stockArea.getStockAreaNo());

			log.info("外购入库单处理完成，正在释放入库事务锁。。。");
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updatetoDoneById(String inStorageNo) {

		log.info("通过入库单主键获取入库单.in_page_no:" + inStorageNo);

		InStorage inStorage = this.getById(inStorageNo);

		if (!"3".equals(inStorage.getInType())) {

			log.error("此入库单不是外购入库单无法执行更新入库完成操作.");
			throw new RuntimeException("入库单类型异常,入库单类型是:" + inStorage.getInType());

		}
		//校验外购入库单托盘项目里是否有正在上架的托盘 如果有就不能置为完成
		List<InStorageTp> inStorageTps = inStorageTpService.list(Wrappers.<InStorageTp>query().lambda().eq(InStorageTp::getWmsBillNo,
				inStorage.getWmsBillNo()).eq(InStorageTp::getStatus, "1"));

		if(inStorageTps != null && inStorageTps.size() > 0){
			log.error("此外购入库单托盘项目里有正在上架托盘，无法执行更新入库完成操作.");
			throw new RuntimeException("此外购入库单托盘项目里有正在上架托盘，无法执行更新入库完成操作,入库单类型是:" + inStorage.getInType());
		}

		inStorage.setStatus("2");
		this.updateById(inStorage);
		log.info("原始入库单" + inStorageNo + "已更新为入库完成");

		log.info("查询" + inStorage.getInBillNumber() + "入库批次下未锁定的库区更新为锁定状态");
		List<StockArea> stockAreaList = stockAreaService.list(Wrappers.<StockArea>query().lambda()
				.eq(StockArea::getInBillNumber, inStorage.getInBillNumber()).eq(StockArea::getLockStatus, "0"));

		if (stockAreaList.size() == 0) {

			log.info("没查询到相对应的库区,无需锁定");

		} else {

			List<String> stockAreaNos = new ArrayList<>();

			for (StockArea stockArea : stockAreaList) {
				log.info("查询到" + inStorage.getInBillNumber() + "入库批次下未锁定的库区:" + stockArea.getStockAreaNo());
				stockAreaNos.add(stockArea.getStockAreaNo());
			}

			//更新库区为锁定状态
			stockAreaService.update(Wrappers.<StockArea>update().lambda().set(StockArea::getLockStatus, "1")
					.in(StockArea::getStockAreaNo, stockAreaNos));
			log.info("已更新以上库区锁定状态为锁定");

		}

		log.info("外购入库调用ERP上架回调 入库完成 只包含入库单头信息和完成状态。");

//		List<InStorageItem> inStorageItems =
//				inStorageItemService
//						.list(Wrappers.<InStorageItem>query().lambda().eq(InStorageItem::getWmsBillNo,
//								inStorage.getWmsBillNo()));

//		List<ERPInStorageItemDTO> erpInStorageItemDTOS = new ArrayList<>();

//		for (InStorageItem isi : inStorageItems) {
//
//			List<InStorageTp> inStorageTps =
//					inStorageTpService.list(Wrappers.<InStorageTp>query().lambda().eq(InStorageTp::getWmsBillNo,
//							isi.getWmsBillNo()).eq(InStorageTp::getInStorageItemId, isi.getInStorageItemId()));
//
//			List<ERPInStorageTpDTO> erpInStorageTpDTOList = new ArrayList<>();
//
//			for (InStorageTp ist : inStorageTps) {
//
//				if (StringUtils.isBlank(ist.getItemId())) {
//					log.error("此入库单" + ist.getUniqueCode() + "托盘itemid为空,wcs还没有正常入库完成，无法执行更新入库完成操作.");
//					throw new RuntimeException("此入库单" + ist.getUniqueCode() + "托盘itemid为空,wcs还没有正常入库完成，无法执行更新入库完成操作.");
//				}
//
//				ERPInStorageTpDTO erpInStorageTpDTO = new ERPInStorageTpDTO();
//				BeanUtils.copyProperties(ist, erpInStorageTpDTO);
//				erpInStorageTpDTOList.add(erpInStorageTpDTO);
//
//			}
//			ERPInStorageItemDTO erpInStorageItemDTO = new ERPInStorageItemDTO();
//			BeanUtils.copyProperties(isi, erpInStorageItemDTO);
//			erpInStorageItemDTO.setErpInStorageTpDTOList(erpInStorageTpDTOList);
//
//			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//			String pdStr = isi.getProductDate().format(formatter);
//			erpInStorageItemDTO.setProductDate(pdStr);
//
//			String effStr = isi.getEffDate().format(formatter);
//			erpInStorageItemDTO.setEffDate(effStr);
//
//			erpInStorageItemDTOS.add(erpInStorageItemDTO);
//		}

		ERPInStorageDTO erpInStorageDTO = new ERPInStorageDTO();
		BeanUtils.copyProperties(inStorage, erpInStorageDTO);
//		erpInStorageDTO.setErpInStorageItemDTOList(erpInStorageItemDTOS);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String inDateStr = inStorage.getInDate().format(formatter);
		erpInStorageDTO.setInDate(inDateStr);
		erpInStorageDTO.setOrderStatus("1");

		erpCallBackService.callBackIn(erpInStorageDTO);

		log.info("外购入库单，调用ERP上架回调完成");

	}

	/**
	 * 处理电商入库
	 *
	 * @param fromLocation
	 * @param toLocation
	 */
	private void DSinStorageHandler(String fromLocation, String toLocation, InStorage inStorage,
									InStorageTp inStorageTp, String stockAreaNo) {

		//调用WCS AtoB的接口  fromLocation  toLocation
		WCSInvInDTO wcsInvInDTO = new WCSInvInDTO();
		String taskId = "wmstrx" + LocalDateTime.now()
				.format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_MS_PATTERN));
		wcsInvInDTO.setTaskId(taskId);
		wcsInvInDTO.setTaskSource("4");
		wcsInvInDTO.setLocatorFrom(fromLocation);
		wcsInvInDTO.setInloc(toLocation);
		wcsInvInDTO.setStockNo(inStorageTp.getUniqueCode());
		wcsInvInDTO.setMemoInfo1("wms调用wcs入库接口,电商入库");
		wcsInvInDTO.setMemoInfo2(stockAreaNo);

		if (!wcsWmsTrxService.handleWCSInvIn(wcsInvInDTO)) {
			throw new RuntimeException("调用WCS入库接口异常!!!");
		}

		WcsWmsTrx wcsWmsTrx = new WcsWmsTrx();
		wcsWmsTrx.setWmsTrxId(taskId);
		wcsWmsTrx.setTrxType("4");
		wcsWmsTrx.setFromLocation(fromLocation);
		wcsWmsTrx.setToLocation(toLocation);
		wcsWmsTrx.setTrayNo(inStorageTp.getUniqueCode());
		wcsWmsTrxMapper.insert(wcsWmsTrx);

		//更新目标库位为使用中
		SLocation toLocationPO = sLocationService.getById(toLocation);
		toLocationPO.setIsUsed("2");
		toLocationPO.setUpdateTime(LocalDateTime.now());
		sLocationService.updateById(toLocationPO);

		//更新库区状态 先检查是否符合预占用状态

		boolean yzyStatus = stockAreaService.checkStockAreaYuZhanYong(stockAreaNo);

		if (yzyStatus) {

			//更新库区为预占用状态
			StockArea stockAreaForCheck = stockAreaService.getById(stockAreaNo);
			stockAreaForCheck.setSku(stockAreaForCheck.getSku());
			stockAreaForCheck.setLockStatus("2");
			stockAreaService.updateById(stockAreaForCheck);
			log.info("更新库区为预占用状态,库区:" + stockAreaNo);

		} else {

			//查找此库位所属库区是否有可使用的库位
			log.info("电商入库检查库区状态,查找此库位所属库区是否有可使用的库位,库区:" + toLocationPO.getStockAreaNo());
			SLocation sLocation =
					sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
							.eq(SLocation::getAreaId, 1).eq(SLocation::getIsUsed, 0)
							.eq(SLocation::getStockAreaNo, toLocationPO.getStockAreaNo())
							.eq(SLocation::getLocationStatus, "1")
							.orderByAsc(SLocation::getSOrder).last("limit 1"));
			if (null == sLocation) {
				//如果没有可使用的库位，更新库区为锁定状态
				StockArea stockArea = stockAreaService.getById(toLocationPO.getStockAreaNo());
				stockArea.setSku(stockArea.getSku());
				stockArea.setStatus("2");
				stockArea.setLockStatus("1");
				stockAreaService.updateById(stockArea);
				log.info("没有可使用的库位，更新库区为锁定状态,库区:" + toLocationPO.getStockAreaNo());
			} else {

				//如果有可使用的库位，更新库区为未锁定状态
				StockArea stockArea = stockAreaService.getById(toLocationPO.getStockAreaNo());
				stockArea.setSku(stockArea.getSku());
				stockArea.setStatus("1");
				stockArea.setLockStatus("0");
				stockAreaService.updateById(stockArea);
				log.info("有可使用的库位，更新库区为未锁定状态,库区:" + toLocationPO.getStockAreaNo());

			}

		}

		//更新入库单为入库中
		inStorage.setStatus("1");
		this.updateById(inStorage);

		//更新入库单托盘维度信息
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		inStorageTp.setOperator(username);
		inStorageTp.setStatus("1");
		inStorageTpService.updateById(inStorageTp);
	}

	/**
	 * 处理外购入库
	 *
	 * @param fromLocation
	 * @param toLocation
	 */
	private void WGinStorageHandler(String fromLocation, String toLocation, InStorage inStorage,
									InStorageTp inStorageTp, String stockAreaNo) {

		//调用WCS AtoB的接口  fromLocation  toLocation
		WCSInvInDTO wcsInvInDTO = new WCSInvInDTO();
		String taskId = "wmstrx" + LocalDateTime.now()
				.format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_MS_PATTERN));
		wcsInvInDTO.setTaskId(taskId);
		wcsInvInDTO.setTaskSource("8");
		wcsInvInDTO.setLocatorFrom(fromLocation);
		wcsInvInDTO.setInloc(toLocation);
		wcsInvInDTO.setStockNo(inStorageTp.getUniqueCode());
		wcsInvInDTO.setMemoInfo1("wms调用wcs入库接口,外购入库");
		wcsInvInDTO.setMemoInfo2(stockAreaNo);

		if (!wcsWmsTrxService.handleWCSInvIn(wcsInvInDTO)) {
			throw new RuntimeException("调用WCS入库接口异常!!!");
		}

		WcsWmsTrx wcsWmsTrx = new WcsWmsTrx();
		wcsWmsTrx.setWmsTrxId(taskId);
		wcsWmsTrx.setTrxType("8");
		wcsWmsTrx.setFromLocation(fromLocation);
		wcsWmsTrx.setToLocation(toLocation);
		wcsWmsTrx.setTrayNo(inStorageTp.getUniqueCode());
		wcsWmsTrxMapper.insert(wcsWmsTrx);

		//更新目标库位为使用中
		SLocation toLocationPO = sLocationService.getById(toLocation);
		toLocationPO.setIsUsed("2");
		toLocationPO.setUpdateTime(LocalDateTime.now());
		sLocationService.updateById(toLocationPO);

		//更新库区状态 先检查是否符合预占用状态
		boolean yzyStatus = stockAreaService.checkStockAreaYuZhanYong(stockAreaNo);

		if (yzyStatus) {

			//更新库区为预占用状态
			StockArea stockAreaForCheck = stockAreaService.getById(stockAreaNo);
			stockAreaForCheck.setSku(stockAreaForCheck.getSku());
			stockAreaForCheck.setLockStatus("2");
			stockAreaService.updateById(stockAreaForCheck);
			log.info("更新库区为预占用状态,库区:" + stockAreaNo);

		} else {

			//查找此库位所属库区是否有可使用的库位
			log.info("外购入库检查库区状态,查找此库位所属库区是否有可使用的库位,库区:" + toLocationPO.getStockAreaNo());
			SLocation sLocation =
					sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
							.eq(SLocation::getAreaId, 1).eq(SLocation::getIsUsed, 0)
							.eq(SLocation::getStockAreaNo, toLocationPO.getStockAreaNo())
							.eq(SLocation::getLocationStatus, "1")
							.orderByAsc(SLocation::getSOrder).last("limit 1"));
			if (null == sLocation) {
				//如果没有可使用的库位，更新库区为锁定状态
				StockArea stockArea = stockAreaService.getById(toLocationPO.getStockAreaNo());
				stockArea.setSku(stockArea.getSku());
				stockArea.setStatus("2");
				stockArea.setLockStatus("1");
				stockAreaService.updateById(stockArea);
				log.info("没有可使用的库位，更新库区为锁定状态,库区:" + toLocationPO.getStockAreaNo());
			} else {

				//如果有可使用的库位，更新库区为未锁定状态
				StockArea stockArea = stockAreaService.getById(toLocationPO.getStockAreaNo());
				stockArea.setSku(stockArea.getSku());
				stockArea.setStatus("1");
				stockArea.setLockStatus("0");
				stockAreaService.updateById(stockArea);
				log.info("有可使用的库位，更新库区为未锁定状态,库区:" + toLocationPO.getStockAreaNo());

			}

		}

		//更新入库单为入库中
		inStorage.setStatus("1");
		this.updateById(inStorage);

		//更新入库单托盘维度信息
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String username = authentication.getName();
		inStorageTp.setOperator(username);
		inStorageTp.setStatus("1");
		inStorageTpService.updateById(inStorageTp);
	}

	@Override
	@Transactional(rollbackFor = Exception.class,isolation = Isolation.READ_UNCOMMITTED)
	public synchronized StockArea getStockArea(InStorageItem inStorageItem, InStorage instorage,String trxType) throws Exception {

		log.info("访问同步方法并开启新的事务getStockArea获取库区并更新库区信息..");

		//获取楼层信息
		TierConfig tierConfig = tierConfigService.getById(1);

		StockArea stockArea;
		//如果是空表示第一次使用，如果入库单此托盘inbillnumber和sku相同可以放入同一层库区
		if (null == tierConfig.getInBillNumber()
				|| (instorage.getInBillNumber().equals(tierConfig.getInBillNumber())
				&& inStorageItem.getSku().equals(tierConfig.getSku()))) {
			log.info("在当前层:"+tierConfig.getCurrentTier()+"查找使用中库区。。。");
			int currentLayer = tierConfig.getCurrentTier();

			StockArea stockAreaUsing = this.getUsingStockArea(inStorageItem, instorage, currentLayer,trxType);

			if (null == stockAreaUsing) {
				log.info("在当前层:"+currentLayer+"未找到使用中库区。。。wms开始查找空库区。。。");

				StockArea stockAreaEmpty = this.getEmptyStockAreaAndExceptSix(currentLayer,instorage,trxType);

				if(null == stockAreaEmpty){

					log.info("在当前层:"+currentLayer+"未找到空库区。。。wms开始尝试查找下一层的使用中的或者空库区。。。");
					int tryCount = 5;

					for (int i = 1; i <= tryCount; i++) {
						if (currentLayer == tierConfig.getEndTier()) {

							currentLayer = tierConfig.getStartTier();

						} else {
							currentLayer = currentLayer + 1;
						}
						log.info("在下一层:"+currentLayer+",wms开始查找使用中的库区。。。尝试第"+i+"次");

						stockAreaUsing = this.getUsingStockArea(inStorageItem, instorage, currentLayer,trxType);

						if(null != stockAreaUsing){
							log.info("在第"+currentLayer+"层中找到使用中的库区。。。退出循环尝试...");
							stockAreaEmpty = stockAreaUsing;
							break;
						}

						log.info("在下一层:"+currentLayer+",wms开始查找空库区。。。尝试第"+i+"次");

						//在下一层查找空库区
						 stockAreaEmpty = this.getEmptyStockAreaAndExceptSix(currentLayer,instorage,trxType);

						 if(null != stockAreaEmpty){
							 log.info("在第"+currentLayer+"层中找到空库区。。。退出循环尝试...");
							 break;
						 }

					}
				}

				stockArea = stockAreaEmpty;

			} else {

				log.info("找到使用中库区。stockArea:" + stockAreaUsing);
				stockArea = stockAreaUsing;

			}

			tierConfig.setCurrentTier(currentLayer);
			tierConfig.setInBillNumber(instorage.getInBillNumber());
			tierConfig.setSku(inStorageItem.getSku());
			tierConfig.setUpdateTime(LocalDateTime.now());
			//更新当前楼层
			tierConfigService.updateById(tierConfig);

		} else {
			int currentLayer = tierConfig.getCurrentTier();
			if (currentLayer == tierConfig.getEndTier()) {

				currentLayer = tierConfig.getStartTier();

			} else {
				currentLayer = currentLayer + 1;
			}

			//在当前楼层中查找使用中库区
			StockArea stockAreaUsing = this.getUsingStockArea(inStorageItem, instorage, currentLayer,trxType);

			if (null == stockAreaUsing) {
				log.info("在" + currentLayer + "未找到使用中库区。。。wms开始查找空库区。。。");
				//查找空库区
				StockArea stockAreaEmpty = this.getEmptyStockAreaAndExceptSix(currentLayer,instorage,trxType);
				if (null == stockAreaEmpty) {
					log.info("在当前层:"+currentLayer+"未找到空库区。。。wms开始尝试查找下一层的使用中或者空库区。。。");
					int tryCount = 5;

					for (int i = 1; i <= tryCount; i++) {
						if (currentLayer == tierConfig.getEndTier()) {

							currentLayer = tierConfig.getStartTier();

						} else {
							currentLayer = currentLayer + 1;
						}
						log.info("在下一层:"+currentLayer+",wms开始查找使用中库区。。。尝试第"+i+"次");

						stockAreaUsing = this.getUsingStockArea(inStorageItem, instorage, currentLayer,trxType);

						if(null != stockAreaUsing){
							log.info("在第"+currentLayer+"层中找到使用中的库区。。。退出循环尝试...");
							stockAreaEmpty = stockAreaUsing;
							break;
						}

						log.info("在下一层:"+currentLayer+",wms开始查找空库区。。。尝试第"+i+"次");
						//在下一层查找空库区
						stockAreaEmpty = this.getEmptyStockAreaAndExceptSix(currentLayer,instorage,trxType);

						if(null != stockAreaEmpty){
							log.info("在第"+currentLayer+"层中找到空库区。。。退出循环尝试...");
							break;
						}

					}
				}
				log.info("已找到空库区:"+stockAreaEmpty.getStockAreaNo());
				stockArea = stockAreaEmpty;

			} else {
				log.info("已找到使用中的库区:"+stockAreaUsing.getStockAreaNo());
				stockArea = stockAreaUsing;
			}

			log.info("更新楼层信息:"+currentLayer+"层,inbillnumber:"+instorage.getInBillNumber()+",sku:"+inStorageItem.getSku());
			tierConfig.setCurrentTier(currentLayer);
			tierConfig.setInBillNumber(instorage.getInBillNumber());
			tierConfig.setSku(inStorageItem.getSku());
			tierConfig.setUpdateTime(LocalDateTime.now());
			//更新当前楼层
			tierConfigService.updateById(tierConfig);

		}

		if (null == stockArea) {
			log.error("整个立库区都未找到合适的库区。。。wms处理失败。。。");
			throw new Exception("整个立库区都未找到合适的库区,wms处理失败!");

		}
		log.info("更新库区信息为库区编号:"+stockArea.getStockAreaNo()+",inbillnumber:"+instorage.getInBillNumber()
				+",sku:"+inStorageItem.getSku()+",BatchNo:"+inStorageItem.getBatchNo()+"status更新为1");
		//更新库区状态
		stockArea.setInBillNumber(instorage.getInBillNumber());
		stockArea.setBatchNo(inStorageItem.getBatchNo());
		stockArea.setSku(inStorageItem.getSku());
		stockArea.setStatus("1");
		stockArea.setUpdateTime(LocalDateTime.now());
		stockAreaService.updateById(stockArea);

		log.info("访问同步方法getStockArea结束并提交事务.");
		return stockArea;
	}

	private StockArea getUsingStockArea(InStorageItem inStorageItem, InStorage instorage, int currentLayer,String trxType) {
		if(("2".equals(instorage.getInType())
				|| "3".equals(instorage.getInType())
				|| "10".equals(trxType)
				|| "15".equals(trxType)
				|| "17".equals(trxType))
				&& currentLayer == 6){
			log.info("查找到入库单类型是电商入库、外购入库或操作类型是返库、回库、盘点回库其中一种，就不在6层查找使用中的库区.返回空库区。。。");
			return null;
		}
		//在当前楼层中查找库区
		return stockAreaService.getOne(Wrappers.<StockArea>query().lambda()
				.eq(StockArea::getAreaId, 1)
				.eq(StockArea::getTier, currentLayer)
				.eq(StockArea::getInBillNumber, instorage.getInBillNumber())
				.eq(StockArea::getSku, inStorageItem.getSku())
				//增加批号搜索条件
				.eq(StockArea::getBatchNo, inStorageItem.getBatchNo())
				//优先查找使用中的库区
				.eq(StockArea::getStatus, "1")
				.eq(StockArea::getLockStatus, "0")
				//查找启用的库区
				.eq(StockArea::getActivatedStatus, "1")
				.orderByAsc(StockArea::getSaOrder).last("limit 1"));
	}

	private StockArea getEmptyStockAreaAndExceptSix(int currentLayer,InStorage instorage,String trxType) {
		if(("2".equals(instorage.getInType())
				|| "3".equals(instorage.getInType())
				|| "10".equals(trxType)
				|| "15".equals(trxType)
				|| "17".equals(trxType))
				&& currentLayer == 6){
			log.info("查找到入库单类型是电商入库、外购入库或操作类型是返库、回库、盘点回库其中一种，就不在6层查找库区.返回空库区。。。");
			return null;
		}
		//查找空库区
		return stockAreaService.getOne(Wrappers.<StockArea>query().lambda()
				.eq(StockArea::getAreaId, 1)
				.eq(StockArea::getTier, currentLayer)
				.eq(StockArea::getStatus, "0")
				.eq(StockArea::getLockStatus, "0")
				//查找启用的库区
				.eq(StockArea::getActivatedStatus, "1")
				.orderByAsc(StockArea::getSaOrder).last("limit 1"));
	}

	@Override
	public synchronized StockArea getCheckErrorStockArea(InStorageItem inStorageItem, InStorage instorage) throws Exception {
		log.info("访问同步方法getCheckErrorStockArea获取盘点异常区并更新库区信息..");
		StockArea stockArea;

		//查找使用中的盘点异常区库区
		StockArea stockAreaUsing = stockAreaService.getOne(Wrappers.<StockArea>query().lambda()
				.eq(StockArea::getAreaId, 13)
				.eq(StockArea::getInBillNumber, instorage.getInBillNumber())
//				.eq(StockArea::getSku, inStorageItem.getSku())
				//增加批号搜索条件
//				.eq(StockArea::getBatchNo, inStorageItem.getBatchNo())
				//优先查找使用中的库区
				.eq(StockArea::getStatus, "1")
				.eq(StockArea::getLockStatus, "0")
				//查找启用的库区
				.eq(StockArea::getActivatedStatus, "1")
				.orderByAsc(StockArea::getSaOrder).last("limit 1"));

		if (null == stockAreaUsing) {
			log.info("未找到使用中库区。。。wms开始查找空库区。。。");
			//如果没找到就查找空库区
			StockArea stockAreaEmpty = stockAreaService.getOne(Wrappers.<StockArea>query().lambda()
					.eq(StockArea::getAreaId, 13)
					.eq(StockArea::getStatus, "0")
					.eq(StockArea::getLockStatus, "0")
					//查找启用的库区
					.eq(StockArea::getActivatedStatus, "1")
					.orderByAsc(StockArea::getSaOrder).last("limit 1"));

			stockArea = stockAreaEmpty;

		} else {

			log.info("找到使用中库区。stockArea:" + stockAreaUsing);
			stockArea = stockAreaUsing;

		}


		if (null == stockArea) {
			log.error("未找到合适的库区。。。wms处理失败。。。");
			throw new Exception("未找到合适的盘点异常库区");

		}
		//更新库区状态
		stockArea.setInBillNumber(instorage.getInBillNumber());
//		stockArea.setBatchNo(inStorageItem.getBatchNo());
//		stockArea.setSku(inStorageItem.getSku());
		stockArea.setStatus("1");
		stockAreaService.updateById(stockArea);

		log.info("访问同步方法getStockArea结束.");
		return stockArea;
	}

	@Override
	public void inBillDone(String inBillNumber) {

		List<StockArea> stockAreaList = stockAreaService.list(Wrappers.<StockArea>query().lambda()
				.eq(StockArea::getInBillNumber, inBillNumber).eq(StockArea::getLockStatus, "0"));

		if (null == stockAreaList || stockAreaList.size() == 0) {

			log.error("没找到可以锁定的库区。inBillNumber：" + inBillNumber);

		} else {

			List<String> stockAreaNoList = new ArrayList<>();

			for (StockArea stockArea : stockAreaList) {
				stockAreaNoList.add(stockArea.getStockAreaNo());
			}

			//更新库区为锁定状态
			stockAreaService.update(Wrappers.<StockArea>update().lambda().set(StockArea::getLockStatus, "1")
					.in(StockArea::getStockAreaNo, stockAreaNoList));

		}
	}

	@Override
	public boolean ERPlockStockAreas(List<ERPLockStockDTO> erpLockStockDTOS) {

		List<ERPLockStockDTO> erpLockStockDTOSForReturn = new ArrayList<>();

		for (ERPLockStockDTO erpLockStockDTO : erpLockStockDTOS) {
			//校验数据
			SLocation sLocation = sLocationService.getOne(Wrappers.<SLocation>query().lambda()
					.eq(SLocation::getStatus, "1")
					.eq(SLocation::getTrayNo, erpLockStockDTO.getUniqueCode())
					.eq(SLocation::getBatchCi, erpLockStockDTO.getItemId())
					.eq(SLocation::getAreaId, 1));
			if (null == sLocation) {
				log.warn("没有找到匹配的立库区库位 继续下一个,流水号:" + erpLockStockDTO.getItemId() + "托盘号：" + erpLockStockDTO.getUniqueCode());
				continue;
			} else {
				if (sLocation.getSku().equals(erpLockStockDTO.getSku())
						&& sLocation.getBatchNo().equals(erpLockStockDTO.getBatchNo())
						&& sLocation.getStockNum().equals(Integer.valueOf(erpLockStockDTO.getAmount()))) {

					if ("0".equals(erpLockStockDTO.getOperation())) {
						//查找锁定库区 for冻结
						StockArea stockArea = stockAreaService.getOne(Wrappers.<StockArea>query().lambda()
								.eq(StockArea::getAreaId, 1)
								.eq(StockArea::getStockAreaNo, sLocation.getStockAreaNo())
								.eq(StockArea::getLockStatus, "1")
								//查找启用的库区
								.eq(StockArea::getActivatedStatus, "1"));
						if (null == stockArea) {
							log.error("没找到相关库区信息。库区编号为：" + sLocation.getStockAreaNo());
							continue;
						}
						log.info("更新库区" + stockArea.getStockAreaNo() + "为冻结状态");
						stockArea.setLockStatus("3");
						stockArea.setUpdateTime(LocalDateTime.now());
						stockAreaService.updateById(stockArea);

						log.info("查找该库区下所有库位信息添加到回传ERP DTO中。");
						List<SLocation> sLocationListForLock = sLocationService.list(Wrappers.<SLocation>query().lambda()
								.eq(SLocation::getStockAreaNo, stockArea.getStockAreaNo())
								.isNotNull(SLocation::getTrayNo));

						for(SLocation slocation : sLocationListForLock){
							ERPLockStockDTO erpLockStockDTOForRe = new ERPLockStockDTO();
							erpLockStockDTOForRe.setItemId(slocation.getBatchCi());
							erpLockStockDTOForRe.setUniqueCode(slocation.getTrayNo());
							erpLockStockDTOForRe.setSku(slocation.getSku());
							erpLockStockDTOForRe.setBatchNo(slocation.getBatchNo());
							erpLockStockDTOForRe.setAmount(slocation.getStockNum().toString());
							erpLockStockDTOForRe.setLocation(slocation.getSLocationNo());
							erpLockStockDTOForRe.setOperation("0");
							erpLockStockDTOSForReturn.add(erpLockStockDTOForRe);
						}

					} else {
						//查找冻结库区 for解冻
						StockArea stockArea = stockAreaService.getOne(Wrappers.<StockArea>query().lambda()
								.eq(StockArea::getAreaId, 1)
								.eq(StockArea::getStockAreaNo, sLocation.getStockAreaNo())
								.eq(StockArea::getLockStatus, "3")
								//查找启用的库区
								.eq(StockArea::getActivatedStatus, "1"));
						if (null == stockArea) {
							log.error("没找到相关库区信息。库区编号为：" + sLocation.getStockAreaNo());
							continue;
						}

						log.info("更新库区" + stockArea.getStockAreaNo() + "为锁定状态");
						stockArea.setLockStatus("1");
						stockArea.setUpdateTime(LocalDateTime.now());
						stockAreaService.updateById(stockArea);

						log.info("查找该库区下所有库位信息 添加回传ERP DTO。");
						List<SLocation> sLocationListForLock = sLocationService.list(Wrappers.<SLocation>query().lambda()
								.eq(SLocation::getStockAreaNo, stockArea.getStockAreaNo())
								.isNotNull(SLocation::getTrayNo));

						for(SLocation slocation : sLocationListForLock){
							ERPLockStockDTO erpLockStockDTOForRe = new ERPLockStockDTO();
							erpLockStockDTOForRe.setItemId(slocation.getBatchCi());
							erpLockStockDTOForRe.setUniqueCode(slocation.getTrayNo());
							erpLockStockDTOForRe.setSku(slocation.getSku());
							erpLockStockDTOForRe.setBatchNo(slocation.getBatchNo());
							erpLockStockDTOForRe.setAmount(slocation.getStockNum().toString());
							erpLockStockDTOForRe.setLocation(slocation.getSLocationNo());
							erpLockStockDTOForRe.setOperation("1");
							erpLockStockDTOSForReturn.add(erpLockStockDTOForRe);
						}

					}

				}else{
					log.warn("ERP托盘信息和wms库位信息不匹配,此托盘无法冻结/解冻 继续下一个,匹配信息是:"
							+ "原sku:"+sLocation.getSku() +"传sku:" + erpLockStockDTO.getSku()
							+ "原batchNo:"+sLocation.getBatchNo() + "传batchNo:" + erpLockStockDTO.getBatchNo()
							+ "原库存数:" +sLocation.getStockNum()+"传库存数:"+ erpLockStockDTO.getAmount());
					if(!sLocation.getSku().equals(erpLockStockDTO.getSku())){
						log.warn("sku不匹配");
					}
					if(!sLocation.getBatchNo().equals(erpLockStockDTO.getBatchNo())){
						log.warn("batchNo不匹配");
					}
					if(!sLocation.getStockNum().equals(Integer.valueOf(erpLockStockDTO.getAmount()))){
						log.warn("库存数不匹配");
					}

					continue;
				}
			}
		}
		if(erpLockStockDTOSForReturn.size() > 0){
			//另起线程回调ERP
			ERPLockCallBackThread erpLockCallBackThread = new ERPLockCallBackThread(erpLockStockDTOSForReturn,erpCallBackService);
			erpLockCallBackThread.start();
		}

		return true;
	}

}
