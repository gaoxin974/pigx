/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 库位信息
 *
 * @author gaoxin
 * @date 2021-04-13 09:16:42
 */
@Data
@TableName("stock_area")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "库区信息")
public class StockArea extends Model<StockArea> {
private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value="库区编号")
    private String stockAreaNo;

    @ApiModelProperty(value="物理编号")
    private String stockWNo;

    @ApiModelProperty(value="库区优先级序号")
    private Integer saOrder;

	@ApiModelProperty(value="所属区域")
	private Integer areaId;

//	@TableField(updateStrategy = FieldStrategy.IGNORED )
    @ApiModelProperty(value="层级")
    private Integer tier;

	@ApiModelProperty(value="上游系统业务单号")
	private String inBillNumber;

    @ApiModelProperty(value="批号")
    private String batchNo;

	@TableField(updateStrategy = FieldStrategy.IGNORED )
    @ApiModelProperty(value="商品sku")
    private String sku;

    @ApiModelProperty(value="使用状态 0：空库区 1：使用中 2：满库区")
    private String status;

	@ApiModelProperty(value="库区锁定状态:0：未锁定状态 1锁定状态")
	private String lockStatus;

	@ApiModelProperty(value="激活状态:1启用 0停用")
	private String activatedStatus;

    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value="更新时间")
    private LocalDateTime updateTime;
    }
