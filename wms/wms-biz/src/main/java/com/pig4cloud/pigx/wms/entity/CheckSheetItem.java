package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@TableName("check_sheet_item")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "盘点单行项目信息")
public class CheckSheetItem extends Model<CheckSheetItem> {

	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.AUTO)
	@ApiModelProperty(value = "盘点单行项目自增主键")
	private int checkSheetItemId;

	@ApiModelProperty(value = "盘点单号")
	private String checkSheetNo;

	@ApiModelProperty(value = "盘点单行项目编号")
	private String checkSheetItemNo;

	@ApiModelProperty(value = "库区编号")
	private String stockAreaNo;

	@ApiModelProperty(value = "原始库位")
	private String oriSLocationNo;

	@ApiModelProperty(value = "托盘编号")
	private String trayNo;

	@ApiModelProperty(value = "原始商品名称")
	private String oriSkuName;

	@ApiModelProperty(value = "原始SKU")
	private String oriSku;

	@ApiModelProperty(value = "原始批号")
	private String oriBatchNo;

	@ApiModelProperty(value = "原始库存数")
	private int oriStockNum;

	@ApiModelProperty(value = "实际商品名称")
	private String realSkuName;

	@ApiModelProperty(value = "实际SKU")
	private String realSku;

	@ApiModelProperty(value = "实际批号")
	private String realBatchNo;

	@ApiModelProperty(value = "实际库存数")
	private int realStockNum;

	@ApiModelProperty(value = "盘点结果")
	private String checkResult;

	@ApiModelProperty(value = "被盘点的托盘状态")
	private String submitStatus;

	@ApiModelProperty(value = "返回的库位")
	private String returnLocation;

	@ApiModelProperty(value = "盘点人")
	private String operator;

	@ApiModelProperty(value = "创建时间")
	private LocalDateTime createTime;

	@ApiModelProperty(value = "修改时间")
	private LocalDateTime updateTime;

}
