/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.wms.dto.InStoragePageDTO;
import com.pig4cloud.pigx.wms.entity.InStorage;
import com.pig4cloud.pigx.wms.service.InStorageService;
import org.springframework.security.access.prepost.PreAuthorize;

import cn.hutool.json.JSONUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;


/**
 * 入库单信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:32
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/instorage" )
@Api(value = "instorage", tags = "入库单信息管理")
public class InStorageController {

    private final  InStorageService inStorageService;

//    /**
//	 * 分页查询
//	 * @param page 分页对象
//	 * @param inStorage 入库单信息
//	 * @return
//	 */
//	@ApiOperation(value = "分页查询", notes = "分页查询")
//	@GetMapping("/page" )
//	//@PreAuthorize("@pms.hasPermission('wms_instorage_view')" )
//	public R getInStoragePage(Page page, InStorage inStorage) {
//		return R.ok(inStorageService.page(page, Wrappers.query(inStorage)));
//	}

	/**
	 * 分页查询
	 * @param page 分页对象
	 * @param inStoragePageDTO 入库单信息
	 * @return
	 */
	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/pageForVO" )
	//@PreAuthorize("@pms.hasPermission('wms_instorage_view')" )
	public R getInStoragePageForVO(Page page, InStoragePageDTO inStoragePageDTO) {
		return R.ok(inStorageService.getInStoragePageForVO(page, inStoragePageDTO));
	}

	/**
	 * 列表查询
	 * @param wmsBillNo 入库单信息
	 * @return
	 */
	@ApiOperation(value = "列表查询入库单item", notes = "列表查询入库单item")
	@GetMapping("/list/{wmsBillNo}" )
	//@PreAuthorize("@pms.hasPermission('wms_instorage_view')" )
	public R getInStoragePage(@PathVariable("wmsBillNo") String wmsBillNo) {
		return R.ok(inStorageService.listItemForVO(wmsBillNo));
	}

	/**
	 * 列表查询
	 * @param wmsBillNo 入库单信息
	 * @return
	 */
	@ApiOperation(value = "列表查询入库单托盘信息", notes = "列表查询入库单TP")
	@GetMapping("/listTP/{wmsBillNo}" )
	//@PreAuthorize("@pms.hasPermission('wms_instorage_view')" )
	public R listInStorageTP(@PathVariable("wmsBillNo") String wmsBillNo) {
		return R.ok(inStorageService.listInStorageTP(wmsBillNo));
	}


    /**
     * 通过id查询入库单信息
     * @param inPageNo id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{inPageNo}" )
    //@PreAuthorize("@pms.hasPermission('wms_instorage_view')" )
    public R getById(@PathVariable("inPageNo" ) String inPageNo) {
        return R.ok(inStorageService.getById(inPageNo));
    }

    /**
     * 新增入库单信息
     * @param inStorage 入库单信息
     * @return R
     */
    @ApiOperation(value = "新增入库单信息", notes = "新增入库单信息")
    @SysLog("新增入库单信息" )
    @PostMapping
    //@PreAuthorize("@pms.hasPermission('wms_instorage_add')" )
    public R save(@RequestBody InStorage inStorage) {
        return R.ok(inStorageService.save(inStorage));
    }

    /**
     * 修改入库单信息
     * @param inStorage 入库单信息
     * @return R
     */
    @ApiOperation(value = "修改入库单信息", notes = "修改入库单信息")
    @SysLog("修改入库单信息" )
    @PutMapping
    //@PreAuthorize("@pms.hasPermission('wms_instorage_edit')" )
    public R updateById(@RequestBody InStorage inStorage) {
        return R.ok(inStorageService.updateById(inStorage));
    }

	@ApiOperation(value = "修改入库单信息为入库完成", notes = "修改入库单信息为入库完成")
	@SysLog("修改入库单信息为入库完成" )
	@PutMapping("/updateToDone/{inStorageNo}")
	public R updateInToDoneById(@PathVariable("inStorageNo") String inStorageNo) {

		log.info("收到外购入库完成请求:入参inStorageNo是"+ inStorageNo);

		inStorageService.updatetoDoneById(inStorageNo);

		return R.ok("入库单状态已变更为已完成,相关库区已锁定");
	}

    /**
     * 通过id删除入库单信息
     * @param inPageNo id
     * @return R
     */
    @ApiOperation(value = "通过id删除入库单信息", notes = "通过id删除入库单信息")
    @SysLog("通过id删除入库单信息" )
    @DeleteMapping("/{inPageNo}" )
    //@PreAuthorize("@pms.hasPermission('wms_instorage_del')" )
    public R removeById(@PathVariable String inPageNo) {
        return R.ok(inStorageService.removeById(inPageNo));
    }

	@ApiOperation(value = "wms入库结单", notes = "wms入库结单")
	@GetMapping("/WMSinBillDone/{inBillNumber}" )
	public R WMSinBillDone(@PathVariable("inBillNumber") String inBillNumber) {

		log.info("收到WMS结单请求 inBillNumber:"+ inBillNumber);

		inStorageService.inBillDone(inBillNumber);

		return R.ok(null,"wms结单成功,已锁定相应库区");
	}

}
