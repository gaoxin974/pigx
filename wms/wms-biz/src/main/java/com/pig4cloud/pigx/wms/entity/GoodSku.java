/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 电商畅销SKU信息
 *
 * @author gaoxin
 * @date 2021-04-08 15:34:28
 */
@Data
@TableName("good_sku")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "电商畅销SKU信息")
public class GoodSku extends Model<GoodSku> {
private static final long serialVersionUID = 1L;

    /**
     * 畅销sku  主键
     */
    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value="畅销sku  主键")
    private String sku;
    /**
     * 最小库存量
     */
    @ApiModelProperty(value="最小库存量")
    private Integer minStock;
    /**
     * 1:启用 0:停用
     */
    @ApiModelProperty(value="1:启用 0:停用")
    private String status;
    /**
     * 
     */
    @ApiModelProperty(value="")
    private LocalDateTime createTime;
    /**
     * 
     */
    @ApiModelProperty(value="")
    private LocalDateTime updateTime;
    }
