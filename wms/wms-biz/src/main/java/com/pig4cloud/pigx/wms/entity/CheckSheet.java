package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@TableName("check_sheet")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "盘点单信息")
public class CheckSheet extends Model<CheckSheet> {
	private static final long serialVersionUID = 1L;

	@TableId(type = IdType.INPUT)
	@ApiModelProperty(value = "盘点单主键")
	private String checkSheetNo;

	@ApiModelProperty(value = "盘点位置")
	private String checkLocation;

	@ApiModelProperty(value = "盘点单状态")
	private String checkStatus;

	@ApiModelProperty(value = "盘点结果")
	private String checkResult;

	@ApiModelProperty(value = "创建时间")
	private LocalDateTime createTime;

	@ApiModelProperty(value = "修改时间")
	private LocalDateTime updateTime;

}
