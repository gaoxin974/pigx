/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 入库单行项目信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:36
 */
@Data
@TableName("in_storage_item")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "入库单行项目信息")
public class InStorageItem extends Model<InStorageItem> {
private static final long serialVersionUID = 1L;

    /**
     * 入库单行项目ID 自增
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value="入库单行项目ID 自增")
    private Integer inStorageItemId;
	/**
	 * wms单据号
	 */
	@ApiModelProperty(value="wms单据号")
	private String wmsBillNo;
    /**
     * wms行项目ID
     */
    @ApiModelProperty(value="wms行项目ID")
    private String wmsItemNo;

	@ApiModelProperty(value="erp行项目ID")
	private String lineNumber;

    /**
     * sku
     */
    @ApiModelProperty(value="sku")
    private String sku;
    /**
     * 批号
     */
    @ApiModelProperty(value="批号")
    private String batchNo;
    /**
     * 生产日期
     */
    @ApiModelProperty(value="生产日期")
    private LocalDateTime productDate;
    /**
     * 失效日期
     */
    @ApiModelProperty(value="失效日期")
    private LocalDateTime effDate;
    /**
     * 入库数量
     */
    @ApiModelProperty(value="入库数量")
    private Integer amount;

	@ApiModelProperty(value="实际入库数量")
	private Integer realAmount;

    /**
     * 最小单位
     */
    @ApiModelProperty(value="最小单位")
    private String minUnitName;
    /**
     * 供应商ID
     */
    @ApiModelProperty(value="供应商ID")
    private String supId;
    /**
     * 配送商ID，如果没有可传空
     */
    @ApiModelProperty(value="配送商ID，如果没有可传空")
    private String pssId;
    /**
     * 填写入库单关闭原因等
     */
    @ApiModelProperty(value="填写入库单关闭原因等")
    private String note;
    /**
     * 扩展字段1
     */
    @ApiModelProperty(value="扩展字段1")
    private String sckext1;
    /**
     * 扩展字段2
     */
    @ApiModelProperty(value="扩展字段2")
    private String sckext2;
    /**
     * 扩展字段3
     */
    @ApiModelProperty(value="扩展字段3")
    private String sckext3;
    /**
     * 扩展字段4
     */
    @ApiModelProperty(value="扩展字段4")
    private String sckext4;
    /**
     * 扩展字段5
     */
    @ApiModelProperty(value="扩展字段5")
    private String sckext5;
    /**
     * 扩展字段6
     */
    @ApiModelProperty(value="扩展字段6")
    private String sckext6;
    /**
     * 扩展字段7
     */
    @ApiModelProperty(value="扩展字段7")
    private String sckext7;
    /**
     * 扩展字段8
     */
    @ApiModelProperty(value="扩展字段8")
    private String sckext8;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private LocalDateTime updateTime;
    }
