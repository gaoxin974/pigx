/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 入库单行项目托盘信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:39
 */
@Data
@ApiModel(value = "ERP入库单行项目托盘信息")
public class ERPInStorageTpDTO{
private static final long serialVersionUID = 1L;

	@ApiModelProperty(value="sku")
	private String sku;

    @ApiModelProperty(value="托盘编号")
    private String uniqueCode;

    @ApiModelProperty(value="批次号")
    private String itemId;

    @ApiModelProperty(value="上架库位编号")
    private String location;

	@ApiModelProperty(value="托盘入库数量")
	private Integer amount;

}
