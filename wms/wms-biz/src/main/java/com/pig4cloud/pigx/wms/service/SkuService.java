package com.pig4cloud.pigx.wms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pigx.wms.dto.SKUDTO;
import com.pig4cloud.pigx.wms.entity.Sku;
import com.pig4cloud.pigx.wms.vo.SKUPageVO;

/**
 * 商品信息表
 *
 * @author gaoxin
 * @date 2021-04-08 11:34:31
 */
public interface SkuService extends IService<Sku> {

	IPage<SKUPageVO> pageSKUsForVO(Page page, SKUDTO skuDTO);

}
