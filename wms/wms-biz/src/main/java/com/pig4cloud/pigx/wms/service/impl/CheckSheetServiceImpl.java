package com.pig4cloud.pigx.wms.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.common.security.util.SecurityUtils;
import com.pig4cloud.pigx.wms.dto.*;
import com.pig4cloud.pigx.wms.entity.*;
import com.pig4cloud.pigx.wms.mapper.CheckSheetMapper;
import com.pig4cloud.pigx.wms.service.*;
import com.pig4cloud.pigx.wms.vo.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
@AllArgsConstructor
public class CheckSheetServiceImpl extends ServiceImpl<CheckSheetMapper, CheckSheet> implements CheckSheetService {

	private final CheckSheetItemService checkSheetItemService;

	private final SLocationService sLocationService;

	private final SkuService skuService;

	private final StockAreaService stockAreaService;

	private final WcsWmsTrxService wcsWmsTrxService;

	private final TraySkuService traySkuService;

	private final InStorageService inStorageService;

	private final InStorageItemService inStorageItemService;

	private final CheckErrorStockAreaService checkErrorStockAreaService;

	private final ERPCallBackService erpCallBackService;

	private final ERPCheckFeedbackService erpCheckFeedbackService;


	@Override
	public IPage<StockAreaForCheckPageVO> getStockAreaForCheckPage(Page page, QueryStockAreaForCheckPageDTO queryStockAreaForCheckPageDTO) {
		return baseMapper.getStockAreaForCheckPage(page, queryStockAreaForCheckPageDTO);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createCheckSheet(Set<String> stockAreasSet, String checkLocation) {

		CheckSheet cs = new CheckSheet();

		LocalDateTime timeNow = LocalDateTime.now();

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

		String ymdhms = dtf.format(timeNow);

		int randomInt = RandomUtil.randomInt(1000, 9999);

		String preWmsNo = "PD";

		String wmsPDNO = preWmsNo + ymdhms + randomInt;

		cs.setCheckSheetNo(wmsPDNO);
		cs.setCheckLocation(checkLocation);

		this.save(cs);

		for (String stockArea : stockAreasSet) {

			//查询选中库区下有托盘的库位
			List<SLocation> sLocationList =
					sLocationService.list(Wrappers.<SLocation>query().lambda()
							.eq(SLocation::getStockAreaNo, stockArea)
							.isNotNull(SLocation::getTrayNo)
							.orderByDesc(SLocation::getSOrder));

			for (int i = 0; i < sLocationList.size(); i++) {

				CheckSheetItem csi = new CheckSheetItem();
				SLocation sLocation = sLocationList.get(i);
				Sku sku = skuService.getById(sLocation.getSku());
				csi.setCheckSheetNo(wmsPDNO);
				csi.setCheckSheetItemNo(wmsPDNO + "_" + stockArea + "_" + i);
				csi.setStockAreaNo(stockArea);
				csi.setOriSLocationNo(sLocation.getSLocationNo());
				csi.setTrayNo(sLocation.getTrayNo());
				csi.setOriSkuName(sku.getGdname());
				csi.setOriSku(sLocation.getSku());
				csi.setOriBatchNo(sLocation.getBatchNo());
				csi.setOriStockNum(sLocation.getStockNum());

				checkSheetItemService.save(csi);

			}

		}

	}

	@Override
	public IPage<CheckSheetPageVO> getCheckSheetsPage(Page page, QueryCheckSheetsPageDTO queryCheckSheetsPageDTO) {
		return baseMapper.getCheckSheetsPage(page, queryCheckSheetsPageDTO);
	}

	@Override
	public List<CheckSheetItem> getCheckSheetItems(String checkSheetNo) {

		List<CheckSheetItem> checkSheetItemList = checkSheetItemService.list(Wrappers.<CheckSheetItem>query().lambda()
				.eq(CheckSheetItem::getCheckSheetNo, checkSheetNo));

		return checkSheetItemList;
	}

	@Override
	public List<ErrorCheckSheetForPDAVO> listErrorCheckSheetForPDA() {

		List<ErrorCheckSheetForPDAVO> errorCheckSheetForPDAVOS = new ArrayList<>();

		List<CheckSheet> checkSheetList = this.list(Wrappers.<CheckSheet>query().lambda()
				.eq(CheckSheet::getCheckResult, "1")
				.eq(CheckSheet::getCheckStatus, "2"));

		for (CheckSheet cs : checkSheetList) {

			List<CheckSheetItem> checkSheetItemList = checkSheetItemService.list(Wrappers.<CheckSheetItem>query().lambda()
					.eq(CheckSheetItem::getCheckResult, "1"));

			for (CheckSheetItem csi : checkSheetItemList) {

				ErrorCheckSheetForPDAVO ecs = new ErrorCheckSheetForPDAVO();
				ecs.setCheckSheetItemId(csi.getCheckSheetItemId());
				ecs.setTrayNo(csi.getTrayNo());
				ecs.setSku(csi.getOriSku());
				ecs.setCheckSheetNo(csi.getCheckSheetNo());
				ecs.setCheckResult(csi.getCheckResult());
				errorCheckSheetForPDAVOS.add(ecs);
			}

		}

		return errorCheckSheetForPDAVOS;
	}

	@Override
	public CheckSheetDetailForPDAVO getCheckSheetDetails(String trayNo) {

		CheckSheetDetailForPDAVO csd = new CheckSheetDetailForPDAVO();
		CheckSheetItem csi = checkSheetItemService.getOne(Wrappers.<CheckSheetItem>query().lambda()
				.eq(CheckSheetItem::getTrayNo, trayNo)
				.eq(CheckSheetItem::getSubmitStatus, "1"));

		if(null == csi){
			return null;
		}

		csd.setCheckSheetItemId(csi.getCheckSheetItemId());
		csd.setOriSkuName(csi.getOriSkuName());
		csd.setOriSku(csi.getOriSku());
		csd.setOriBatchNo(csi.getOriBatchNo());
		csd.setOriTrayNo(csi.getTrayNo());
		csd.setOriStockNum(csi.getOriStockNum());
		csd.setRealSkuName(csi.getOriSkuName());
		csd.setRealSku(csi.getOriSku());
		csd.setRealBatchNo(csi.getOriBatchNo());
		csd.setRealTrayNo(csi.getTrayNo());
		csd.setRealStockNum(csi.getOriStockNum());

		return csd;
	}

	@Override
	public List<Sku> listSkuInfo(String skuName) {

		List<Sku> skus = skuService.list(Wrappers.<Sku>query().lambda()
				.like(Sku::getGdname, skuName).last("limit 5"));

		return skus;
	}

	@Override
	public CheckSheetDetailForPDAVO getCheckSheetDetailsForView(int checkSheetItemId) {
		CheckSheetDetailForPDAVO csd = new CheckSheetDetailForPDAVO();
		CheckSheetItem csi = checkSheetItemService.getById(checkSheetItemId);

		csd.setCheckSheetItemId(csi.getCheckSheetItemId());
		csd.setOriSkuName(csi.getOriSkuName());
		csd.setOriSku(csi.getOriSku());
		csd.setOriBatchNo(csi.getOriBatchNo());
		csd.setOriTrayNo(csi.getTrayNo());
		csd.setOriStockNum(csi.getOriStockNum());
		csd.setRealSkuName(csi.getRealSkuName());
		csd.setRealSku(csi.getRealSku());
		csd.setRealBatchNo(csi.getRealBatchNo());
		csd.setRealTrayNo(csi.getTrayNo());
		csd.setRealStockNum(csi.getRealStockNum());

		return csd;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public WMSReturnInfo startCheck(String checkSheetNo) {

		//保存多个wcs出库接口参数,最后用另外一个线程触发
		List<WCSInvOutDTO> wcsInvOutDTOList = new ArrayList<>();

		WMSReturnInfo wmsReturnInfo = new WMSReturnInfo();

		CheckSheet cs = this.getById(checkSheetNo);

		if (!"0".equals(cs.getCheckStatus())) {
			wmsReturnInfo.setSuccess(false);
			wmsReturnInfo.setMessage("盘点单不是待盘点状态 无法下发盘点单!");
			return wmsReturnInfo;
		}

		//更新盘点单为待盘点状态
		cs.setCheckStatus("1");
		cs.setUpdateTime(LocalDateTime.now());
		this.updateById(cs);

		//查找出盘点单下库位托盘信息
		List<CheckSheetItem> checkSheetItemList = checkSheetItemService.list(Wrappers.<CheckSheetItem>query().lambda()
				.eq(CheckSheetItem::getCheckSheetNo, checkSheetNo)
				.orderByAsc(CheckSheetItem::getCheckSheetItemId));

		for (CheckSheetItem csi : checkSheetItemList) {

			//更新库区锁定状态为盘点中
			StockArea sa = stockAreaService.getById(csi.getStockAreaNo());
			sa.setLockStatus("4");
			sa.setUpdateTime(LocalDateTime.now());
			stockAreaService.updateById(sa);

			SLocation sLocation = sLocationService.getById(csi.getOriSLocationNo());

			//更新库位状态为正在使用
			sLocation.setIsUsed("2");
			sLocation.setUpdateTime(LocalDateTime.now());
			sLocationService.updateById(sLocation);

			//发往盘点区
			WCSInvOutDTO wcsInvOutDTO = new WCSInvOutDTO();
			String taskId = "wmstrx" + LocalDateTime.now()
					.format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_MS_PATTERN));
			wcsInvOutDTO.setTaskId(taskId);
			wcsInvOutDTO.setInvOutType("16");
			wcsInvOutDTO.setOutLoc(csi.getOriSLocationNo());
			wcsInvOutDTO.setStockNo(sLocation.getTrayNo());
			wcsInvOutDTO.setLocDesti(cs.getCheckLocation());
			wcsInvOutDTO.setMemoInfo1("wms调用wcs出库接口,to盘点库位");
			wcsInvOutDTO.setMemoInfo2(sLocation.getStockAreaNo());

			wcsInvOutDTOList.add(wcsInvOutDTO);

			WcsWmsTrx wcsWmsTrx = new WcsWmsTrx();
			wcsWmsTrx.setWmsTrxId(taskId);
			wcsWmsTrx.setTrxType("16");
			wcsWmsTrx.setFromLocation(csi.getOriSLocationNo());
			wcsWmsTrx.setToLocation(cs.getCheckLocation());
			wcsWmsTrx.setTrayNo(sLocation.getTrayNo());
			wcsWmsTrxService.save(wcsWmsTrx);

			TraySku traySku = new TraySku();
			BeanUtils.copyProperties(sLocation, traySku);
			traySku.setMustNum(0);
			traySku.setCreateTime(null);
			traySku.setUpdateTime(null);
			traySkuService.save(traySku);

		}

		wmsReturnInfo.setSuccess(true);

		OutCallWcsThread outCallWcsThread = new OutCallWcsThread(wcsInvOutDTOList, wcsWmsTrxService);
		outCallWcsThread.start();

		return wmsReturnInfo;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void submitCheckInfoForPDA(SubmitCheckInfoForPDADTO submitCheckInfoForPDADTO) {

		int itemId = Integer.valueOf(submitCheckInfoForPDADTO.getCheckSheetItemId());

		CheckSheetItem csi = checkSheetItemService.getById(itemId);

		CheckSheet cs = this.getById(csi.getCheckSheetNo());
		cs.setCheckStatus("2");

		csi.setRealSkuName(submitCheckInfoForPDADTO.getRealSkuName());
		csi.setRealSku(submitCheckInfoForPDADTO.getRealSku());
		String str = submitCheckInfoForPDADTO.getRealStockNum();
//		String intStr = str.substring(0, str.lastIndexOf("."));
		csi.setRealStockNum(Integer.valueOf(str));
		csi.setRealBatchNo(submitCheckInfoForPDADTO.getRealBatchNo());
		if (csi.getOriSkuName().equals(csi.getRealSkuName())
				&& csi.getOriSku().equals(csi.getRealSku())
				&& csi.getOriStockNum() == csi.getRealStockNum()
				&& csi.getOriBatchNo().equals(csi.getRealBatchNo())) {
			csi.setCheckResult("0");
		} else {
			csi.setCheckResult("1");
			cs.setCheckResult("1");
		}
		csi.setSubmitStatus("2");
		csi.setOperator(SecurityUtils.getUser().getUsername());
		csi.setUpdateTime(LocalDateTime.now());

		boolean normal = "0".equals(csi.getCheckResult()) ? true : false;

		String returnLocation = checkTPBackForPDA(csi.getTrayNo(), cs.getCheckLocation(), normal,csi.getCheckSheetNo());
		csi.setReturnLocation(returnLocation);

		//更新盘点单和行项目状态
		this.updateById(cs);
		checkSheetItemService.updateById(csi);


	}

    @Override
	@Transactional(rollbackFor = Exception.class)
	public String checkTPBackForPDA(String trayNo, String fromLocation, boolean normal,String checkSheetNo) {

		TraySku traySku = traySkuService.getById(trayNo);

		ERPCheckFeedback erpCheckFeedback = erpCheckFeedbackService.getOne(Wrappers.<ERPCheckFeedback>query().lambda()
				.eq(ERPCheckFeedback::getItemId, traySku.getBatchCi())
				.eq(ERPCheckFeedback::getUniqueCode, traySku.getTrayNo())
				.orderByDesc(ERPCheckFeedback::getCreateTime).last("limit 1"));

		InStorageItem inStorageItem;
		InStorage instorage;

		if(null != erpCheckFeedback){
			log.info("查出在ERP盘点反馈中有此托盘信息，创建虚拟入库单保存inbillnumber");
			//虚拟入库单和行项目
			InStorage inStorageNew = new InStorage();
			inStorageNew.setInBillNumber(erpCheckFeedback.getInBillNumber());
			instorage = inStorageNew;

			InStorageItem inStorageItemNew = new InStorageItem();
			inStorageItemNew.setSku(erpCheckFeedback.getConfirmSku());
			inStorageItemNew.setBatchNo(erpCheckFeedback.getConfirmBatchNo());
			inStorageItem = inStorageItemNew;

		}else{
			//重新入库
			//获取入库单信息
			log.info("没有查出在ERP盘点反馈中有此托盘信息，查询原始入库单信息 获取库区");
			inStorageItem = inStorageItemService.getById(traySku.getInStorageItemId());

			instorage = inStorageService.getById(traySku.getInPageNo());
		}

		StockArea stockArea = null;
		try {
			if(normal){
				stockArea = inStorageService.getStockArea(inStorageItem, instorage,"17");
			}else{
				instorage.setInBillNumber(checkSheetNo);
				stockArea = inStorageService.getCheckErrorStockArea(inStorageItem, instorage);
			}
		} catch (Exception e) {
			log.error("获取库区出现异常。。。没找到库区");
			e.printStackTrace();
			throw new RuntimeException("回库异常!!!");
		}

		if (null == stockArea) {
			log.error("获取库区出现异常。。。没找到库区");
			throw new RuntimeException("回库异常!!!");
		}
		//获取启用的异常区未使用的排序级别最高的库位编号
		SLocation sLocation =
				sLocationService.getOne(Wrappers.<SLocation>query().lambda()
						.eq(SLocation::getStatus, 1)
						.eq(SLocation::getAreaId, stockArea.getAreaId())
						.eq(SLocation::getIsUsed, 0)
						.eq(SLocation::getLocationStatus, "1")
						.eq(SLocation::getStockAreaNo, stockArea.getStockAreaNo())
						.orderByAsc(SLocation::getSOrder).last("limit 1"));

		if (sLocation == null) {

			throw new RuntimeException("未找到合适的库位!!!");

		} else {

			String sNo = sLocation.getSLocationNo();
			sLocation.setIsUsed("2");
			sLocation.setUpdateTime(LocalDateTime.now());
			sLocationService.updateById(sLocation);

			//更新库区状态 先检查是否符合预占用状态
			boolean yzyStatus = stockAreaService.checkStockAreaYuZhanYong(stockArea.getStockAreaNo());

			if (yzyStatus) {

				//更新库区为预占用状态
				StockArea stockAreaForCheck = stockAreaService.getById(sLocation.getStockAreaNo());
				stockAreaForCheck.setSku(stockAreaForCheck.getSku());
				stockAreaForCheck.setLockStatus("2");
				stockAreaForCheck.setUpdateTime(LocalDateTime.now());
				stockAreaService.updateById(stockAreaForCheck);
				log.info("更新库区为预占用状态,库区:" + sLocation.getStockAreaNo());

			} else {
				log.info("库区不符合预占用状态,按原来逻辑更新库区信息:" + sLocation.getStockAreaNo());
				//查找此库位所属库区是否有可使用的库位 判断是否锁定库区
				log.info("检查库区状态,查找此库位所属库区是否有可使用的库位,库区:" + sLocation.getStockAreaNo());
				SLocation sLocationForCheck =
						sLocationService.getOne(Wrappers.<SLocation>query().lambda().eq(SLocation::getStatus, 1)
								.eq(SLocation::getIsUsed, 0)
								.eq(SLocation::getStockAreaNo, sLocation.getStockAreaNo())
								.eq(SLocation::getLocationStatus, "1")
								.orderByAsc(SLocation::getSOrder).last("limit 1"));
				if (null == sLocationForCheck) {
					//如果没有可使用的库位，更新库区为锁定状态
					StockArea stockAreaForCheck = stockAreaService.getById(sLocation.getStockAreaNo());
					stockAreaForCheck.setSku(stockAreaForCheck.getSku());
					stockAreaForCheck.setStatus("2");
					stockAreaForCheck.setLockStatus("1");
					stockAreaService.updateById(stockAreaForCheck);
					log.info("没有可使用的库位，更新库区为锁定状态,库区:" + sLocation.getStockAreaNo());
				} else {

					//如果有可使用的库位，更新库区为未锁定状态
					StockArea stockAreaForCheck = stockAreaService.getById(sLocation.getStockAreaNo());
					stockAreaForCheck.setSku(stockAreaForCheck.getSku());
					stockAreaForCheck.setStatus("1");
					stockAreaForCheck.setLockStatus("0");
					stockAreaService.updateById(stockAreaForCheck);
					log.info("有可使用的库位，更新库区为未锁定状态,库区:" + sLocation.getStockAreaNo());

				}
			}

			//调用wcs
			WCSInvInDTO wcsInvInDTO = new WCSInvInDTO();
			String taskId = "wmstrx" + LocalDateTime.now()
					.format(DateTimeFormatter.ofPattern(DatePattern.PURE_DATETIME_MS_PATTERN));
			wcsInvInDTO.setTaskId(taskId);
			wcsInvInDTO.setTaskSource("17");
			wcsInvInDTO.setLocatorFrom(fromLocation);
			wcsInvInDTO.setInloc(sNo);
			wcsInvInDTO.setStockNo(trayNo);
			wcsInvInDTO.setMemoInfo1("wms调用wcs做盘点回库");
			wcsInvInDTO.setMemoInfo2(stockArea.getStockAreaNo());

			if (!wcsWmsTrxService.handleWCSInvIn(wcsInvInDTO)) {
				throw new RuntimeException("调用WCS入库接口异常!!!");
			}

			WcsWmsTrx wcsWmsTrx = new WcsWmsTrx();
			wcsWmsTrx.setWmsTrxId(taskId);
			wcsWmsTrx.setTrxType("17");
			wcsWmsTrx.setTrayNo(trayNo);
			wcsWmsTrx.setFromLocation(fromLocation);
			wcsWmsTrx.setToLocation(sNo);

			wcsWmsTrxService.save(wcsWmsTrx);

			return sNo;
		}

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public WMSReturnInfo submitCheckSheet(String checkSheetNo) {

		WMSReturnInfo wmsReturnInfo = new WMSReturnInfo();

		//checkstatus
		CheckSheet cs = this.getById(checkSheetNo);
		if("3".equals(cs.getCheckStatus()) && "1".equals(cs.getCheckResult())){
			ERPCheckErrorDTO erpCheckErrorDTO = new ERPCheckErrorDTO();
			erpCheckErrorDTO.setTaskId(cs.getCheckSheetNo());

			LocalDateTime timeNow = LocalDateTime.now();
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			String ymdhms = dtf.format(timeNow);

			erpCheckErrorDTO.setIssueTime(ymdhms);
			List<ERPCheckErrorCheckInfoDTO> checkInfos = new ArrayList<>();

			List<CheckSheetItem> checkSheetItemListError = checkSheetItemService.list(Wrappers.<CheckSheetItem>query().lambda()
					.eq(CheckSheetItem::getCheckSheetNo,cs.getCheckSheetNo())
					.eq(CheckSheetItem::getCheckResult,"1"));
			Set<String> stockAreaSet = new HashSet<>();

			for(CheckSheetItem csi : checkSheetItemListError){
				SLocation sLocation = sLocationService.getById(csi.getReturnLocation());
				StockArea sa = stockAreaService.getById(sLocation.getStockAreaNo());
				String errorArea = sa.getStockAreaNo();

				stockAreaSet.add(errorArea);

				ERPCheckErrorCheckInfoDTO erpCheckErrorCheckInfoDTO = new ERPCheckErrorCheckInfoDTO();
				erpCheckErrorCheckInfoDTO.setItemId(sLocation.getBatchCi());
				erpCheckErrorCheckInfoDTO.setUniqueCode(csi.getTrayNo());
				erpCheckErrorCheckInfoDTO.setOriginSku(csi.getOriSku());
				erpCheckErrorCheckInfoDTO.setOriginBatchNo(csi.getOriBatchNo());
				erpCheckErrorCheckInfoDTO.setOriginAmount(String.valueOf(csi.getOriStockNum()));
				erpCheckErrorCheckInfoDTO.setCheckSku(csi.getRealSku());
				erpCheckErrorCheckInfoDTO.setCheckBatchNo(csi.getRealBatchNo());
				erpCheckErrorCheckInfoDTO.setCheckAmount(String.valueOf(csi.getRealStockNum()));
				erpCheckErrorCheckInfoDTO.setRePutLocation(csi.getReturnLocation());
				String time = dtf.format(csi.getUpdateTime());
				erpCheckErrorCheckInfoDTO.setCheckTime(time);
				erpCheckErrorCheckInfoDTO.setRemark("");

				checkInfos.add(erpCheckErrorCheckInfoDTO);

				if(!"1".equals(sa.getLockStatus())){
					//更新库区为锁定状态
					stockAreaService.update(Wrappers.<StockArea>update().lambda().set(StockArea::getLockStatus,"1")
							.eq(StockArea::getStockAreaNo,errorArea));
				}

			}

			for(String stockArea : stockAreaSet){
				CheckErrorStockArea checkErrorStockArea = new CheckErrorStockArea();
				checkErrorStockArea.setCheckSheetNo(cs.getCheckSheetNo());
				checkErrorStockArea.setStockAreaNo(stockArea);
				checkErrorStockAreaService.save(checkErrorStockArea);
			}


			erpCheckErrorDTO.setCheckInfo(checkInfos);
			erpCallBackService.checkErrorCallBack(erpCheckErrorDTO);

		} else {
			wmsReturnInfo.setSuccess(false);
			wmsReturnInfo.setMessage("盘点单状态不对 无法提交");
			return wmsReturnInfo;
		}
		cs.setCheckStatus("4");
		cs.setUpdateTime(LocalDateTime.now());
		this.updateById(cs);


		return wmsReturnInfo;
	}

	@Override
	public List<ReCheckListVO> listReCheck(String checkSheetNo) {

		List<ReCheckListVO> reCheckListVOS = new ArrayList<>();

		List<CheckErrorStockArea> checkErrorStockAreas = checkErrorStockAreaService.list(Wrappers.<CheckErrorStockArea>query().lambda()
				.eq(CheckErrorStockArea::getCheckSheetNo,checkSheetNo));
		for(CheckErrorStockArea cesa : checkErrorStockAreas){
			ReCheckListVO reCheckListVO = new ReCheckListVO();
			StockArea sa = stockAreaService.getById(cesa.getStockAreaNo());
			reCheckListVO.setStockAreaNo(sa.getStockAreaNo());
			reCheckListVO.setSku(sa.getSku());
			reCheckListVO.setBatchNo(sa.getBatchNo());
			reCheckListVO.setInBillNumber(sa.getInBillNumber());
			reCheckListVOS.add(reCheckListVO);
		}


		return reCheckListVOS;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void handleERPCheckFeedback(ERPCheckErrorFeedbackDTO erpCheckErrorFeedbackDTO) {

		for(ERPCheckErrorConfirmInfoDTO erpCheckErrorConfirmInfoDTO : erpCheckErrorFeedbackDTO.getConfirmInfo()){
			ERPCheckFeedback erpCheckFeedback = new ERPCheckFeedback();
			erpCheckFeedback.setTaskId(erpCheckErrorFeedbackDTO.getTaskId());
			BeanUtils.copyProperties(erpCheckErrorConfirmInfoDTO, erpCheckFeedback);
			log.info("先保存ERP盘点反馈信息。。。盘点单号:"+erpCheckFeedback.getTaskId()+"trayId:"+erpCheckFeedback.getUniqueCode());
			erpCheckFeedbackService.save(erpCheckFeedback);

			log.info("根据ERP盘点反馈信息更新入库单信息。。。");

			log.info("先查询对应盘点异常区的库位托盘信息是否存在 itemId号:"+erpCheckErrorConfirmInfoDTO.getItemId()+"trayId:"+erpCheckFeedback.getUniqueCode());
			SLocation sLocation = sLocationService.getOne(Wrappers.<SLocation>query().lambda()
					.eq(SLocation::getTrayNo, erpCheckFeedback.getUniqueCode())
					.eq(SLocation::getBatchCi, erpCheckFeedback.getItemId())
					.eq(SLocation::getAreaId, 13));

			if(null != sLocation){
				log.info("托盘在盘点异常区，更新异常区库位信息");
//				InStorageItem inStorageItem = inStorageItemService.getById(sLocation.getInStorageItemId());
//				inStorageItem.setSku(erpCheckFeedback.getConfirmSku());
//				inStorageItem.setBatchNo(erpCheckFeedback.getConfirmBatchNo());
//				inStorageItem.setAmount(Integer.valueOf(erpCheckFeedback.getConfirmAmount()));
//				inStorageItem.setUpdateTime(LocalDateTime.now());
//				inStorageItemService.updateById(inStorageItem);
//				log.info("更新入库单行项目信息为ERP反馈的盘点信息。。。id:"+inStorageItem.getInStorageItemId());

//				InStorage instorage = inStorageService.getById(sLocation.getInPageNo());
//				instorage.setInBillNumber(erpCheckFeedback.getInBillNumber());
//				instorage.setUpdateTime(LocalDateTime.now());
//				inStorageService.updateById(instorage);
//				log.info("更新入库单信息为ERP反馈的盘点信息。。。id:"+instorage.getInPageNo());

				sLocation.setSku(erpCheckErrorConfirmInfoDTO.getConfirmSku());
				sLocation.setBatchNo(erpCheckErrorConfirmInfoDTO.getConfirmBatchNo());
				sLocation.setStockNum(Integer.valueOf(erpCheckErrorConfirmInfoDTO.getConfirmAmount()));
				sLocation.setUpdateTime(LocalDateTime.now());
				sLocationService.updateById(sLocation);
				log.info("更新库位信息为ERP反馈的盘点信息。。。"+sLocation.getSLocationNo());

				log.info("查询临时表对应的库位托盘信息是否存在 itemId号:"+erpCheckErrorConfirmInfoDTO.getItemId()+"trayId:"+erpCheckFeedback.getUniqueCode());
				TraySku traySku = traySkuService.getById(erpCheckFeedback.getUniqueCode());

				if(null != traySku){
					log.info("托盘也在临时区，更新临时区库位信息");

					traySku.setSku(erpCheckErrorConfirmInfoDTO.getConfirmSku());
					traySku.setBatchNo(erpCheckErrorConfirmInfoDTO.getConfirmBatchNo());
					traySku.setStockNum(Integer.valueOf(erpCheckErrorConfirmInfoDTO.getConfirmAmount()));
					traySku.setUpdateTime(LocalDateTime.now());
					traySkuService.updateById(traySku);
					log.info("更新临时表信息为ERP反馈的盘点信息。。。"+traySku.getTrayNo());
				}

			}else{

				log.info("查询临时表对应的库位托盘信息是否存在 itemId号:"+erpCheckErrorConfirmInfoDTO.getItemId()+"trayId:"+erpCheckFeedback.getUniqueCode());
				TraySku traySku = traySkuService.getById(erpCheckFeedback.getUniqueCode());

				log.info("托盘在临时区，更新临时区库位信息");
//				InStorageItem inStorageItem = inStorageItemService.getById(traySku.getInStorageItemId());
//				inStorageItem.setSku(erpCheckFeedback.getConfirmSku());
//				inStorageItem.setBatchNo(erpCheckFeedback.getConfirmBatchNo());
//				inStorageItem.setAmount(Integer.valueOf(erpCheckFeedback.getConfirmAmount()));
//				inStorageItem.setUpdateTime(LocalDateTime.now());
//				inStorageItemService.updateById(inStorageItem);
//				log.info("更新入库单行项目信息为ERP反馈的盘点信息。。。id:"+inStorageItem.getInStorageItemId());

//				InStorage instorage = inStorageService.getById(traySku.getInPageNo());
//				instorage.setInBillNumber(erpCheckFeedback.getInBillNumber());
//				instorage.setUpdateTime(LocalDateTime.now());
//				inStorageService.updateById(instorage);
//				log.info("更新入库单信息为ERP反馈的盘点信息。。。id:"+instorage.getInPageNo());

				traySku.setSku(erpCheckErrorConfirmInfoDTO.getConfirmSku());
				traySku.setBatchNo(erpCheckErrorConfirmInfoDTO.getConfirmBatchNo());
				traySku.setStockNum(Integer.valueOf(erpCheckErrorConfirmInfoDTO.getConfirmAmount()));
				traySku.setUpdateTime(LocalDateTime.now());
				traySkuService.updateById(traySku);
				log.info("更新临时表信息为ERP反馈的盘点信息。。。"+traySku.getTrayNo());

			}
			log.info("查询盘点单行项目 如果存在就更新。。。");
			CheckSheetItem csi = checkSheetItemService.getOne(Wrappers.<CheckSheetItem>query().lambda()
					.eq(CheckSheetItem::getTrayNo, erpCheckFeedback.getUniqueCode())
					.and(tmp -> tmp.eq(CheckSheetItem::getSubmitStatus, "0")
							.or().eq(CheckSheetItem::getSubmitStatus, "1")));
			if(null != csi){
				log.info("找到盘点行项目，更新ERP反馈信息。。。");
				csi.setOriSku(erpCheckErrorConfirmInfoDTO.getConfirmSku());
				Sku sku = skuService.getById(erpCheckErrorConfirmInfoDTO.getConfirmSku());
				csi.setOriSkuName(sku.getGdname());
				csi.setOriBatchNo(erpCheckErrorConfirmInfoDTO.getConfirmBatchNo());
				csi.setOriStockNum(Integer.valueOf(erpCheckErrorConfirmInfoDTO.getConfirmAmount()));
				csi.setUpdateTime(LocalDateTime.now());
				checkSheetItemService.updateById(csi);
			}

		}

	}
	@Override
	public IPage<StockAreaForCheckPageVO> getStockAreaForLockPage(Page page, QueryStockAreaForLockPageDTO queryStockAreaForLockPageDTO) {
		return baseMapper.getStockAreaForLockPage(page, queryStockAreaForLockPageDTO);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void lockStockAreas(Set<String> stockAreasSet) {

		for (String stockArea : stockAreasSet) {
			log.info("要处理锁定的库区，库区编号为:"+stockArea);
			StockArea sa = stockAreaService.getById(stockArea);
			sa.setLockStatus("1");
			sa.setUpdateTime(LocalDateTime.now());
			stockAreaService.updateById(sa);
			log.info("已锁定库区，库区编号为:"+stockArea);
		}

	}

}
