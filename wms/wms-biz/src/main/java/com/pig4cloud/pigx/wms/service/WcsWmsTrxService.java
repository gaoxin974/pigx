package com.pig4cloud.pigx.wms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pigx.wms.dto.*;
import com.pig4cloud.pigx.wms.entity.WcsWmsTrx;

/**
 * wcs和wms交互记录
 *
 * @author gaoxin
 * @date 2021-04-15 09:50:40
 */
public interface WcsWmsTrxService extends IService<WcsWmsTrx> {

	boolean getTestWCS();

	boolean handleWCSInvIn(WCSInvInDTO wcsInvInDTO);

	boolean handleWCSInvOut(WCSInvOutDTO wcsInvOutDTO);

	String putEmptyTrayLocation(WmsDpInvDTO wmsDpInvDTO);

	String getEmptyTrayLocation(WmsKpInvDTO wmsKpInvDTO);

	boolean wcsCallBack(WcsWmsCallBackDTO wcsWmsCallBackDTO);

	boolean getEmptyTrayLocationForProduction(String trayNo,WCSInvInDTO wcsInvInDTO);

	void getWGInStorageTpForPDA();

	boolean handleWCSEmptyCtl(WCSEmptyCtlDTO wcsEmptyCtlDTO);
}
