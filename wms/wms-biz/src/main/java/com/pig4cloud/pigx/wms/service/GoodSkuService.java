/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pigx.wms.dto.GoodSkuDTO;
import com.pig4cloud.pigx.wms.entity.GoodSku;
import com.pig4cloud.pigx.wms.vo.GoodSkuVO;

/**
 * 电商畅销SKU信息
 *
 * @author gaoxin
 * @date 2021-04-08 15:34:28
 */
public interface GoodSkuService extends IService<GoodSku> {

	IPage<GoodSkuVO> getGoodSkuVoPage(Page page, GoodSkuDTO goodSkuDTO);
}
