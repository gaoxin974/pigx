
package com.pig4cloud.pigx.wms.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.wms.entity.TierConfig;
import com.pig4cloud.pigx.wms.mapper.TierConfigMapper;
import com.pig4cloud.pigx.wms.service.TierConfigService;

@Service
public class TierConfigServiceImpl extends ServiceImpl<TierConfigMapper, TierConfig> implements TierConfigService {

}
