package com.pig4cloud.pigx.wms.service;

import com.pig4cloud.pigx.wms.dto.ERPOutStorageDTO;
import com.pig4cloud.pigx.wms.entity.ERPCheckErrorDTO;
import com.pig4cloud.pigx.wms.entity.ERPInStorageDTO;
import com.pig4cloud.pigx.wms.entity.ERPLockStockDTO;

import java.util.List;

public interface ERPCallBackService {

	void callBackIn(ERPInStorageDTO erpInStorageDTO);

	void callBackOut(ERPOutStorageDTO erpOutStorageDTO);

	void lockOperationCallBack(List<ERPLockStockDTO> erpLockStockDTOSForReturn);

	void checkErrorCallBack(ERPCheckErrorDTO erpCheckErrorDTO);

}
