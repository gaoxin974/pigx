

package com.pig4cloud.pigx.wms.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pigx.wms.entity.StockArea;
import com.pig4cloud.pigx.wms.vo.StockAreaDetailForVO;
import com.pig4cloud.pigx.wms.vo.StockAreaForVO;

/**
 * @author gaoxin
 */
public interface StockAreaService extends IService<StockArea> {

	List<StockAreaForVO> listStockAreaForVO(int tier);

	List<StockArea> listStockAreaByAreaIdForVO(int areaId,int tier);

	StockAreaDetailForVO getStockAreaDetailVO(String stockAreaNo);

	boolean activatedStatus(String stockAreaNo, String activatedStatus);

	StockArea getStockAreaForTrayIn();

	StockArea getStockAreaForTrayOut();

    boolean checkStockAreaYuZhanYong(String stockAreaNo);
}
