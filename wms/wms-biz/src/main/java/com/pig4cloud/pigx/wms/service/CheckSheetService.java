package com.pig4cloud.pigx.wms.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pigx.wms.dto.*;
import com.pig4cloud.pigx.wms.entity.CheckSheet;
import com.pig4cloud.pigx.wms.entity.CheckSheetItem;
import com.pig4cloud.pigx.wms.entity.ERPCheckErrorFeedbackDTO;
import com.pig4cloud.pigx.wms.entity.Sku;
import com.pig4cloud.pigx.wms.vo.*;
import java.util.List;
import java.util.Set;

public interface CheckSheetService extends IService<CheckSheet> {

	IPage<StockAreaForCheckPageVO> getStockAreaForCheckPage(Page page, QueryStockAreaForCheckPageDTO queryStockAreaForCheckPageDTO);

	void createCheckSheet(Set<String> stockAreasSet, String checkLocation);

	IPage<CheckSheetPageVO> getCheckSheetsPage(Page page, QueryCheckSheetsPageDTO queryStockAreaForCheckPageDTO);

	List<CheckSheetItem> getCheckSheetItems(String checkSheetNo);

	List<ErrorCheckSheetForPDAVO> listErrorCheckSheetForPDA();

	CheckSheetDetailForPDAVO getCheckSheetDetails(String trayNo);

	List<Sku> listSkuInfo(String skuName);

	CheckSheetDetailForPDAVO getCheckSheetDetailsForView(int checkSheetItemId);

	WMSReturnInfo startCheck(String checkSheetNo);

    void submitCheckInfoForPDA(SubmitCheckInfoForPDADTO submitCheckInfoForPDADTO);

	String checkTPBackForPDA(String trayNo, String fromLocation, boolean normal,String checkSheetNo);

	WMSReturnInfo submitCheckSheet(String checkSheetNo);

	List<ReCheckListVO> listReCheck(String checkSheetNo);

	void handleERPCheckFeedback(ERPCheckErrorFeedbackDTO erpCheckErrorFeedbackDTO);

	IPage<StockAreaForCheckPageVO> getStockAreaForLockPage(Page page, QueryStockAreaForLockPageDTO queryStockAreaForLockPageDTO);

	void lockStockAreas(Set<String> stockAreasSet);
}
