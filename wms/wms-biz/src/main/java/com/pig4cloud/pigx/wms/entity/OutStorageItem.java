/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 入库单行项目信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:36
 */
@Data
@TableName("out_storage_item")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "入库单行项目信息")
public class OutStorageItem extends Model<OutStorageItem> {
private static final long serialVersionUID = 1L;

    /**
     * 出库单行项目ID 自增
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value="出库单行项目ID 自增")
    private Integer outStorageItemId;
	/**
	 * wms单据号
	 */
	@ApiModelProperty(value="wms单据号")
	private String wmsBillNo;
    /**
     * wms行项目ID
     */
    @ApiModelProperty(value="wms行项目ID")
    private String wmsItemNo;
    /**
     * sku
     */
    @ApiModelProperty(value="sku")
    private String sku;

	@ApiModelProperty(value="lineNumber行编号")
	private String lineNumber;
    /**
     * 批号
     */
    @ApiModelProperty(value="批号")
    private String batchNo;
    /**
     * 出库数量
     */
    @ApiModelProperty(value="出库数量")
    private Integer amount;
    /**
	 * 最小单位
	 */
	@ApiModelProperty(value="最小单位")
	private String minUnitName;
	/**
	 * 备注
	 */
	@ApiModelProperty(value="备注")
	private String comment;
    /**
     * 扩展字段1
     */
    @ApiModelProperty(value="扩展字段1")
    private String sckext1;
    /**
     * 扩展字段2
     */
    @ApiModelProperty(value="扩展字段2")
    private String sckext2;
    /**
     * 扩展字段3
     */
    @ApiModelProperty(value="扩展字段3")
    private String sckext3;
    /**
     * 扩展字段4
     */
    @ApiModelProperty(value="扩展字段4")
    private String sckext4;
    /**
     * 扩展字段5
     */
    @ApiModelProperty(value="扩展字段5")
    private String sckext5;
    /**
     * 扩展字段6
     */
    @ApiModelProperty(value="扩展字段6")
    private String sckext6;
    /**
     * 扩展字段7
     */
    @ApiModelProperty(value="扩展字段7")
    private String sckext7;
    /**
     * 扩展字段8
     */
    @ApiModelProperty(value="扩展字段8")
    private String sckext8;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private LocalDateTime updateTime;
    }
