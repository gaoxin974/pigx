package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@TableName("check_error_stock_area")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "盘点异常库区")
public class CheckErrorStockArea extends Model<CheckErrorStockArea> {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "盘点位置")
	private String checkSheetNo;

	@ApiModelProperty(value = "盘点单状态")
	private String stockAreaNo;

	@ApiModelProperty(value = "创建时间")
	private LocalDateTime createTime;

	@ApiModelProperty(value = "修改时间")
	private LocalDateTime updateTime;

}
