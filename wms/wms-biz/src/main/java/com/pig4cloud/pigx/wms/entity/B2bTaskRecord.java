/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * b2b任务记录表
 *
 * @author gaoxin
 * @date 2021-04-07 20:05:29
 */
@Data
@TableName("b2b_task_record")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "b2b任务记录表forPDA")
public class B2bTaskRecord extends Model<B2bTaskRecord> {
private static final long serialVersionUID = 1L;


    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty(value="自增id")
    private Integer id;

    @ApiModelProperty(value="wms出库单号")
    private String wmsBillNo;

	@ApiModelProperty(value="出库单托盘信息id")
	private int outTpId;

	@ApiModelProperty(value="托盘编号")
	private String tpNo;

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="商品名称")
	private String skuName;

	@ApiModelProperty(value="实际出库数量")
	private int realAmount;

	@ApiModelProperty(value="操作员")
	private String operator;

    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value="更新时间")
    private LocalDateTime updateTime;

}
