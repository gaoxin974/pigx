package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@TableName("erp_check_feedback")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "ERP盘点异常反馈")
public class ERPCheckFeedback extends Model<ERPCheckFeedback> {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "盘点单号")
	private String taskId;

	@ApiModelProperty(value = "batchci_id 流水号")
	private String itemId;

	@ApiModelProperty(value = "托盘编号")
	private String uniqueCode;

	@ApiModelProperty(value = "上游系统业务单号")
	private String inBillNumber;

	@ApiModelProperty(value = "入库单号")
	private String inPageNo;

	@ApiModelProperty(value = "ERP行项目编号")
	private String lineNumber;

	@ApiModelProperty(value = "1：产线入库 2：电商入库 3：外购入库 4：退货入库")
	private String inType;

	@ApiModelProperty(value = "确认Sku")
	private String confirmSku;

	@ApiModelProperty(value = "确认批次")
	private String confirmBatchNo;

	@ApiModelProperty(value = "确认数量")
	private String confirmAmount;

	@ApiModelProperty(value = "生产日期")
	private String productDate;

	@ApiModelProperty(value = "失效日期")
	private String effDate;

	@ApiModelProperty(value = "货主id")
	private String huozId;

	@ApiModelProperty(value = "创建时间")
	private String createTime;

	@ApiModelProperty(value = "修改时间")
	private String updateTime;

}
