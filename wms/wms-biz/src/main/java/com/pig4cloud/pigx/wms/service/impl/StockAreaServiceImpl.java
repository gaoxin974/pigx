
package com.pig4cloud.pigx.wms.service.impl;

import java.util.List;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.wms.entity.SLocation;
import com.pig4cloud.pigx.wms.entity.StockArea;
import com.pig4cloud.pigx.wms.mapper.StockAreaMapper;
import com.pig4cloud.pigx.wms.service.SLocationService;
import com.pig4cloud.pigx.wms.service.StockAreaService;
import com.pig4cloud.pigx.wms.vo.StockAreaDetailForVO;
import com.pig4cloud.pigx.wms.vo.StockAreaForVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class StockAreaServiceImpl extends ServiceImpl<StockAreaMapper, StockArea> implements StockAreaService {

	private  final StockAreaMapper stockAreaMapper;

	private final SLocationService sLocationService;


	@Override
	public List<StockAreaForVO> listStockAreaForVO(int tier){

		log.info("查询"+tier+"层库区状况");
		return stockAreaMapper.listStockAreaForVO(tier);
	}

	@Override
	public List<StockArea> listStockAreaByAreaIdForVO(int areaId,int tier) {

		log.info("查询"+areaId+"区域"+tier+"层级库区状况");
		return stockAreaMapper.listStockAreaByAreaIdForVO(areaId,tier);
	}

	@Override
	public StockAreaDetailForVO getStockAreaDetailVO(String stockAreaNo) {
		log.info("查询"+stockAreaNo+"库区详情");
		return stockAreaMapper.getStockAreaDetailVO(stockAreaNo);
	}

	@Override
	public boolean activatedStatus(String stockAreaNo, String activatedStatus) {

		if("1".equals(activatedStatus)){

			log.info("将小库区"+stockAreaNo+"变为启用状态.");

			StockArea stockArea = this.getById(stockAreaNo);
			stockArea.setActivatedStatus("1");
			this.updateById(stockArea);

			return true;

		}else{

			log.info("将小库区"+stockAreaNo+"变为停用状态.检查此小库区下所有库位是否都是空库位");
			List<SLocation> sLocations = sLocationService.list(Wrappers.<SLocation>query().lambda().eq(SLocation::getStockAreaNo,
					stockAreaNo).ne(SLocation::getLocationStatus,"1"));

			if(sLocations.size() != 0){

				log.warn("找到小库区"+stockAreaNo+"包含非空库位.不能停用此小库区");
				return false;
			}else{

				log.warn("没找到小库区"+stockAreaNo+"包含非空库位.可以停用此小库区");
				StockArea stockArea = this.getById(stockAreaNo);
				stockArea.setActivatedStatus("0");
				this.updateById(stockArea);
				return true;

			}

		}
	}

	@Override
	public StockArea getStockAreaForTrayIn() {

		// 筛选未锁定的（ 空库区+使用中） 库区 按照优先级顺序选择库区
		StockArea stockArea = this.getOne(Wrappers.<StockArea>query().lambda()
				.eq(StockArea::getAreaId, 3)
				//优先查找空的、使用中的库区
				.ne(StockArea::getStatus,"2")
				.eq(StockArea::getLockStatus,"0")
				//查找启用的库区
				.eq(StockArea::getActivatedStatus,"1")
				.orderByAsc(StockArea::getSaOrder).last("limit 1"));

		if(null == stockArea) {
			log.error("未找到空的、使用中库区。。。托盘入库查找库区失败。。。");
			throw new RuntimeException("未找到空的、使用中库区。。。托盘入库查找库区失败。。。");
		}

		return stockArea;
	}

	@Override
	public StockArea getStockAreaForTrayOut() {
		// 筛选未锁定的（ 空库区+使用中） 库区 按照优先级顺序选择库区
		StockArea stockArea = this.getOne(Wrappers.<StockArea>query().lambda()
				.eq(StockArea::getAreaId, 3)
				//优先查找空的、使用中的库区
				.ne(StockArea::getStatus,"0")
				.eq(StockArea::getLockStatus,"0")
				//查找启用的库区
				.eq(StockArea::getActivatedStatus,"1")
				.orderByAsc(StockArea::getSaOrder).last("limit 1"));

		if(null == stockArea) {
			log.error("未找到满的的、使用中库区。。。托盘出库库查找库区失败。。。");
			throw new RuntimeException("未找到满的、使用中库区。。。托盘出库查找库区失败。。。");
		}

		return stockArea;
	}

	@Override
	public boolean checkStockAreaYuZhanYong(String stockAreaNo) {

		boolean isYZY;

		log.info("检查小库区是否符合预占用状态:");
		log.info("先统计小库区"+stockAreaNo+"可用库位总数");
		List<SLocation> actSLocations = sLocationService.list(Wrappers.<SLocation>query().lambda().eq(SLocation::getStockAreaNo,
				stockAreaNo).eq(SLocation::getStatus,"1"));
		int activeTotal = actSLocations.size();
		log.info("小库区"+stockAreaNo+"可用库位总数是:"+activeTotal);

		log.info("继续统计小库区"+stockAreaNo+"已使用库位总数");
		List<SLocation> usedSLocations = sLocationService.list(Wrappers.<SLocation>query().lambda().eq(SLocation::getStockAreaNo,
				stockAreaNo).eq(SLocation::getStatus,"1").eq(SLocation::getIsUsed,"1"));
		int usedTotal = usedSLocations.size();
		log.info("小库区"+stockAreaNo+"已使用库位总数是:"+usedTotal);

		log.info("继续统计小库区"+stockAreaNo+"使用中库位总数");
		List<SLocation> usingSLocations = sLocationService.list(Wrappers.<SLocation>query().lambda().eq(SLocation::getStockAreaNo,
				stockAreaNo).eq(SLocation::getStatus,"1").eq(SLocation::getIsUsed,"2"));
		int usingTotal = usingSLocations.size();
		log.info("小库区"+stockAreaNo+"使用中库位总数是:"+usingTotal);

		if(usingTotal !=0 && activeTotal == (usedTotal+usingTotal)){
			isYZY = true;
		}else{
			isYZY = false;
		}

		return isYZY;
	}

}
