/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pigx.wms.dto.*;
import com.pig4cloud.pigx.wms.entity.*;
import com.pig4cloud.pigx.wms.vo.*;

import cn.hutool.json.JSONObject;

/**
 * 出库单service
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:32
 */
public interface OutStorageService extends IService<OutStorage> {

	void saveALLCX(ERPOutStorageDTO erpOutStorageDTO);

	void saveALLGY(List<ERPOutStorageDTO> erpOutStorageDTOs);

	void handleDSOuts(JSONObject jsonObject);

	IPage<OutStoragePageVO> getOutStoragePageForVO(Page page, OutStoragePageDTO outStoragePageDTO);

	CreatePackageReturnDTO createPackageOrStockUp(List<OutStorageForPackageDTO> outStorageForPackageList,String method,String bhlx);

	PackageDetailForVO getPackageDetail(String packageNo);

	OutItemForVO getOutDetail(String outPageNo);

	OutTPForPDAVO getOutTPDetailForPDA(String uniqueCode);

	void submitOutDetail(OutDetailForPDADTO outDetailForPDADTO);

	void emptyTPReturnForPDA(String fromLocation,String tpno);

	void tpReturnForPDA(TPReturnForPDADTO tpReturnForPDADTO);

	List<PackageListForDSVO> getPackageListForDS();

	boolean getPackageTaskForDS();

	List<OutStorage> listPackageTaskForDS();

	List<OutStorageTpForDS> listPackageTaskDetailForDS(String wmsBillNo);

	boolean submitPackageTaskDetailForDS(String wmsBillNo);

	OutBHTPForPDAVO getOutBHTPDetailForPDA(String subPackageNo,String uniqueCode);

	void submitBHOutDetail(OutDetailForPDABHDTO outBHDetailForPDADTO);

	void handleTJOuts(Map<String, ERPOutStorageDTO> erpOutStorageDTOMap);

	List<SubPackage> listSubPackageForDS();

	List<TiZongListForPDADTO> listTZDSSubPackageForPDA(String subPackageNo);

	List<SCart> listCartsSubPackageDSForPDA();

	List<PDADSOutStorageJHVO> listCartsDetailForPDA(String cartId);

	List<Sku> listCartSKUsForPDA(String cartId);

	boolean submitJHTaskForDS(String cartInfoId);

	boolean getNextTZForDS(String packageNo);

	String handleAgvOut(String outPageNo);

	void agvBHOut(OutStorage outStorageForBHOut, List<AgvOutQueue> agvOutQueueForBHOut);

	void agvInQueuetoWCS(List<AgvInQueue> agvInQueue);

    List<BHOutForPDAVO> bhOutForPDAVO(String outPageNo);

	void submitOutDetailAndBack(OutDetailAndBackForPDADTO outDetailAndBackForPDADTO);

	void tpBackForPDA(OutDetailAndBackForPDADTO outDetailAndBackForPDADTO);

	void emptyTPforRG(String tpno);
}
