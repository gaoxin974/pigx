/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 入库单信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:32
 */
@Data
@TableName("in_storage")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "入库单信息")
public class InStorage extends Model<InStorage> {
private static final long serialVersionUID = 1L;

    /**
     * 入库单主键
     */
    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value="入库单主键")
    private String inPageNo;
    /**
     * wms单据号
     */
    @ApiModelProperty(value="wms单据号")
    private String wmsBillNo;
    /**
     * 库区ID
     */
    @ApiModelProperty(value="库区ID")
    private Integer storageId;
    /**
     * 货主ID
     */
    @ApiModelProperty(value="货主ID")
    private String huozId;
    /**
     * 物流中心ID
     */
    @ApiModelProperty(value="物流中心ID")
    private String wlzxCode;
    /**
     * 上游系统业务单号，下发可能重复，WMS不做逻辑处理
     */
    @ApiModelProperty(value="上游系统业务单号，下发可能重复，WMS不做逻辑处理")
    private String inBillNumber;
    /**
     * 入库时间
     */
    @ApiModelProperty(value="入库时间")
    private LocalDateTime inDate;
    /**
     * 1：产线入库 2：电商入库 3：外购入库 4：退货入库
     */
    @ApiModelProperty(value="1：产线入库 2：电商入库 3：外购入库 4：退货入库")
    private String inType;
    /**
     * 0:待入库 1:入库中 2:已完成
     */
    @ApiModelProperty(value="0:待入库 1:入库中 2:已完成")
    private String status;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    @ApiModelProperty(value="修改时间")
    private LocalDateTime updateTime;
    }
