/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 库位信息
 *
 * @author gaoxin
 * @date 2021-04-13 09:16:42
 */
@Data
@TableName("s_location")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "库位信息")
public class SLocation extends Model<SLocation> {
private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value="库位号")
    private String sLocationNo;

    @ApiModelProperty(value="物理编号")
    private String wNo;

	@ApiModelProperty(value="库区编号")
	private String stockAreaNo;

    @ApiModelProperty(value="优先级序号")
    private Integer sOrder;

	@TableField(updateStrategy = FieldStrategy.IGNORED )
    @ApiModelProperty(value="托盘编码")
    private String trayNo;

    @ApiModelProperty(value="批号")
    private String batchNo;

    @ApiModelProperty(value="批次")
    private String batchCi;

	@TableField(updateStrategy = FieldStrategy.IGNORED )
    @ApiModelProperty(value="商品sku")
    private String sku;

	@TableField(updateStrategy = FieldStrategy.IGNORED )
    @ApiModelProperty(value="货主ID")
    private String huozId;

	@TableField(updateStrategy = FieldStrategy.IGNORED )
    @ApiModelProperty(value="真实货主ID")
    private String realHuoz;

    @ApiModelProperty(value="库存")
    private Integer stockNum;

	@ApiModelProperty(value="出库量")
	private Integer outAmount;

    @ApiModelProperty(value="所属区域")
    private Integer areaId;

    @ApiModelProperty(value="使用状态 1：已使用 0：未使用 2:正在使用")
    private String isUsed;

    @ApiModelProperty(value="激活状态 1：启用 0：停用")
    private String status;

	@ApiModelProperty(value="库位存储状态:1空库位 2已放置托盘 3已放置空框 4托盘和货物 5空框和货物")
	private String locationStatus;

	@ApiModelProperty(value="入库单id")
	private String inPageNo;

	@ApiModelProperty(value="入库单行项目id")
	private Integer inStorageItemId;

    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value="更新时间")
    private LocalDateTime updateTime;
    }
