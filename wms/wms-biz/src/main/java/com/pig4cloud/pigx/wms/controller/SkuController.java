/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.controller;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import com.pig4cloud.pigx.wms.dto.SKUDTO;
import com.pig4cloud.pigx.wms.dto.SLocationDTO;
import com.pig4cloud.pigx.wms.entity.Sku;
import com.pig4cloud.pigx.wms.service.SkuService;
import com.pig4cloud.pigx.wms.vo.SKUPageVO;
import com.pig4cloud.pigx.wms.vo.SLocationPageVO;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 商品信息表
 *
 * @author gaoxin
 * @date 2021-04-08 11:34:31
 */
@RestController
@AllArgsConstructor
@RequestMapping("/sku" )
@Api(value = "sku", tags = "商品信息表管理")
public class SkuController {

    private final  SkuService skuService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param sku 商品信息表
     * @return
     */
//    @ApiOperation(value = "分页查询", notes = "分页查询")
//    @GetMapping("/page" )
//    //@PreAuthorize("@pms.hasPermission('wms_sku_view')" )
//    public R getSkuPage(Page page, Sku sku) {
//        return R.ok(skuService.page(page, Wrappers.query(sku)));
//    }


    /**
     * 通过id查询商品信息表
     * @param sku id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{sku}" )
    //@PreAuthorize("@pms.hasPermission('wms_sku_view')" )
    public R getById(@PathVariable("sku" ) String sku) {
        return R.ok(skuService.getById(sku));
    }

    /**
     * 新增商品信息表
     * @param sku 商品信息表
     * @return R
     */
    @ApiOperation(value = "新增商品信息表", notes = "新增商品信息表")
    @SysLog("新增商品信息表" )
    @PostMapping
    //@PreAuthorize("@pms.hasPermission('wms_sku_add')" )
    public R save(@RequestBody Sku sku) {
        return R.ok(skuService.save(sku));
    }

	/**
	 * 批量新增商品信息表
	 * @param skus 商品信息表 批量
	 * @return R
	 */
	@ApiOperation(value = "批量新增商品信息表", notes = "批量新增商品信息表")
	@SysLog("批量新增商品信息表" )
	@PostMapping("/batch")
	@Inner(value = false)
	public R saveBatch(@RequestBody List<Sku> skus) {

		return R.ok(skuService.saveOrUpdateBatch(skus));
	}

    /**
     * 修改商品信息表
     * @param sku 商品信息表
     * @return R
     */
    @ApiOperation(value = "修改商品信息表", notes = "修改商品信息表")
    @SysLog("修改商品信息表" )
    @PutMapping
    //@PreAuthorize("@pms.hasPermission('wms_sku_edit')" )
    public R updateById(@RequestBody Sku sku) {
        return R.ok(skuService.updateById(sku));
    }

    /**
     * 通过id删除商品信息表
     * @param sku id
     * @return R
     */
    @ApiOperation(value = "通过id删除商品信息表", notes = "通过id删除商品信息表")
    @SysLog("通过id删除商品信息表" )
    @DeleteMapping("/{sku}" )
    //@PreAuthorize("@pms.hasPermission('wms_sku_del')" )
    public R removeById(@PathVariable String sku) {
        return R.ok(skuService.removeById(sku));
    }

	@ApiOperation(value = "分页查询商品(SKU)信息", notes = "分页查询商品(SKU)信息")
	@GetMapping("/pageSKUs" )
	public R pageSKUs(Page page, SKUDTO skuDTO) {

		IPage<SKUPageVO> skuPageVO = skuService.pageSKUsForVO(page, skuDTO);

		return R.ok(skuPageVO);
	}

}
