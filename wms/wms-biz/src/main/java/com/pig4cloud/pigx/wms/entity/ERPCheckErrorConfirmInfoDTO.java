package com.pig4cloud.pigx.wms.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "ERP盘点异常确认InfoDTO")
public class ERPCheckErrorConfirmInfoDTO {
private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="托盘流水号batchci")
    private String itemId;

	@ApiModelProperty(value="托盘编号")
	private String uniqueCode;

	@ApiModelProperty(value="上游系统业务单号")
	private String inBillNumber;

	@ApiModelProperty(value="入库单号")
	private String inPageNo;

	@ApiModelProperty(value="ERP行项目编号")
	private String lineNumber;

	@ApiModelProperty(value="1：产线入库 2：电商入库 3：外购入库 4：退货入库")
	private String inType;

	@ApiModelProperty(value="确认Sku")
	private String confirmSku;

	@ApiModelProperty(value="确认批次")
	private String confirmBatchNo;

	@ApiModelProperty(value="确认数量")
	private String confirmAmount;

	@ApiModelProperty(value="生成日期")
	private String productDate;

	@ApiModelProperty(value="失效日期")
	private String effDate;

	@ApiModelProperty(value="货主ID")
	private String huozId;

}
