/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import java.time.LocalDateTime;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 入库单行项目信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:36
 */
@Data
@TableName("in_storage_item")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "入库单行项目信息")
public class ERPInStorageItemDTO extends Model<ERPInStorageItemDTO> {
private static final long serialVersionUID = 1L;


    /**
     * sku
     */
    @ApiModelProperty(value="sku")
    private String sku;
    /**
     * 批号
     */
    @ApiModelProperty(value="批号")
    private String batchNo;

	@ApiModelProperty(value="行项目编号")
	private String lineNumber;

    /**
     * 生产日期
     */
    @ApiModelProperty(value="生产日期")
//	@JsonFormat(shape = JsonFormat.Shape.NUMBER)
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private String productDate;
    /**
     * 失效日期
     */
    @ApiModelProperty(value="失效日期")
//	@JsonFormat(shape = JsonFormat.Shape.NUMBER)
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private String effDate;
    /**
     * 入库数量
     */
    @ApiModelProperty(value="入库数量")
    private Integer amount;
    /**
     * 最小单位
     */
    @ApiModelProperty(value="最小单位")
    private String minUnitName;
	/**
	 * 托盘信息
	 */
	@ApiModelProperty(value="托盘信息")
	private List<ERPInStorageTpDTO> erpInStorageTpDTOList;
    /**
     * 供应商ID
     */
    @ApiModelProperty(value="供应商ID")
    private String supId;
    /**
     * 配送商ID，如果没有可传空
     */
    @ApiModelProperty(value="配送商ID，如果没有可传空")
    private String pssId;
    /**
     * 填写入库单关闭原因等
     */
    @ApiModelProperty(value="填写入库单关闭原因等")
    private String note;
    /**
     * 扩展字段1
     */
    @ApiModelProperty(value="扩展字段1")
    private String sckext1;
    /**
     * 扩展字段2
     */
    @ApiModelProperty(value="扩展字段2")
    private String sckext2;
    /**
     * 扩展字段3
     */
    @ApiModelProperty(value="扩展字段3")
    private String sckext3;
    /**
     * 扩展字段4
     */
    @ApiModelProperty(value="扩展字段4")
    private String sckext4;
    /**
     * 扩展字段5
     */
    @ApiModelProperty(value="扩展字段5")
    private String sckext5;
    /**
     * 扩展字段6
     */
    @ApiModelProperty(value="扩展字段6")
    private String sckext6;
    /**
     * 扩展字段7
     */
    @ApiModelProperty(value="扩展字段7")
    private String sckext7;
    /**
	 * 扩展字段8
	 */
	@ApiModelProperty(value="扩展字段8")
	private String sckext8;

    }
