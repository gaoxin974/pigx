/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 拣货小车表
 *
 * @author gaoxin
 * @date 2021-04-07 20:05:29
 */
@Data
@TableName("s_cart")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "拣货小车表")
public class SCart extends Model<SCart> {
private static final long serialVersionUID = 1L;

    @TableId
    @ApiModelProperty(value="小车id")
    private Integer id;

    @ApiModelProperty(value="库区名称")
    private String name;

    @ApiModelProperty(value="")
    private LocalDateTime createTime;

    @ApiModelProperty(value="")
    private LocalDateTime updateTime;
    }
