package com.pig4cloud.pigx.wms.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "ERP盘点异常checkInfoDTO")
public class ERPCheckErrorCheckInfoDTO {
private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="托盘流水号batchci")
    private String itemId;

	@ApiModelProperty(value="托盘编号")
	private String uniqueCode;

	@ApiModelProperty(value="sku(原)")
	private String originSku;

	@ApiModelProperty(value="批号(原)")
	private String originBatchNo;

	@ApiModelProperty(value="库存数量(原)")
	private String originAmount;

	@ApiModelProperty(value="sku(盘)")
	private String checkSku;

	@ApiModelProperty(value="批号(盘)")
	private String checkBatchNo;

	@ApiModelProperty(value="库存数量(盘)")
	private String checkAmount;

	@ApiModelProperty(value="盘库后暂存异常库位")
	private String rePutLocation;

	@ApiModelProperty(value="盘库时间")
	private String checkTime;

	@ApiModelProperty(value="备注")
	private String remark;

}
