package com.pig4cloud.pigx.wms.mapper;

import com.pig4cloud.pigx.common.data.datascope.PigxBaseMapper;
import com.pig4cloud.pigx.wms.entity.AgvInQueue;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface AgvInQueueMapper extends PigxBaseMapper<AgvInQueue> {

}
