/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import java.time.LocalDateTime;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 入库单信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:32
 */
@Data
@ApiModel(value = "ERP入库单实体")
public class ERPInStorageDTO {
private static final long serialVersionUID = 1L;

    /**
     * 入库单号 主键
     */
    @ApiModelProperty(value="入库单号 主键")
    private String inPageNo;
    /**
     * 库区ID
     */
    @ApiModelProperty(value="库区ID")
    private Integer storageId;
    /**
     * 货主ID
     */
    @ApiModelProperty(value="货主ID")
    private String huozId;
    /**
     * 物流中心ID
     */
    @ApiModelProperty(value="物流中心ID")
    private String wlzxCode;

    @ApiModelProperty(value="上游系统业务单号，下发可能重复，WMS入库库区判断使用")
    private String inBillNumber;
    /**
     * 入库时间
     */
    @ApiModelProperty(value="入库时间")
//	@JsonFormat(shape = JsonFormat.Shape.NUMBER)
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private String inDate;
    /**
     * 1：产线入库 2：电商入库 3：外购入库 4：退货入库
     */
    @ApiModelProperty(value="1：产线入库 2：电商入库 3：外购入库 4：退货入库")
    private String inType;

	@ApiModelProperty(value="0:未完成 1:已完成")
	private String orderStatus;

	@ApiModelProperty(value="入库单行项目信息")
	private List<ERPInStorageItemDTO> erpInStorageItemDTOList;

}
