
package com.pig4cloud.pigx.wms.service.impl;


import com.pig4cloud.pigx.wms.entity.ERPCheckErrorDTO;
import com.pig4cloud.pigx.wms.entity.ERPLockStockDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.pig4cloud.pigx.wms.dto.ERPOutStorageDTO;
import com.pig4cloud.pigx.wms.entity.ERPInStorageDTO;
import com.pig4cloud.pigx.wms.service.ERPCallBackService;

import cn.hutool.json.JSONUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Service
@RefreshScope
@RequiredArgsConstructor
@Slf4j
public class ERPCallBackServiceImpl implements ERPCallBackService {


	@Value("${erp.callback.in}")
	private String erpCallBackIn;

	@Value("${erp.callback.out}")
	private String erpCallBackOut;

	@Value("${erp.callback.lockOperation}")
	private String lockOperationCallBack;

	@Value("${erp.callback.checkError}")
	private String checkErrorCallBack;

	@Override
	public void callBackIn(ERPInStorageDTO erpInStorageDTO) {
		RestTemplate restTemplate = new RestTemplate();

		String body = JSONUtil.toJsonStr(erpInStorageDTO);

		log.info("ERP上架回调请求体是:"+body);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(body, headers);
		ResponseEntity<String> result;

		try{

			result	= restTemplate.exchange(erpCallBackIn, HttpMethod.POST,entity,String.class);
			log.info("ERP上架回调结果的JSON是:"+result.getBody());

		}catch (Exception e){
			log.error("ERP上架回调请求出现异常。",e);
			e.printStackTrace();
		}


	}

	@Override
//	@Transactional(rollbackFor = Exception.class)
	public void callBackOut(ERPOutStorageDTO erpOutStorageDTO) {


		RestTemplate restTemplate = new RestTemplate();

		String body = JSONUtil.toJsonStr(erpOutStorageDTO);

		log.info("ERP下架回调请求体是:"+body);

		// 开始时间
		long stime = System.currentTimeMillis();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(body, headers);
		ResponseEntity<String> result = restTemplate.exchange(erpCallBackOut, HttpMethod.POST,entity,String.class);

		// 结束时间
		long etime = System.currentTimeMillis();

		// 计算执⾏时间
		log.info("回调ERP出库接口callBackOut执⾏时长："+(etime - stime)+" 毫秒.");

		log.info("ERP下架回调结果的JSON是:"+result.getBody());

	}

	@Override
	public void lockOperationCallBack(List<ERPLockStockDTO> erpLockStockDTOSForReturn) {
		RestTemplate restTemplate = new RestTemplate();

		String body = JSONUtil.toJsonStr(erpLockStockDTOSForReturn);

		log.info("ERP库存锁定（解锁）反馈请求体是:"+body);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(body, headers);
		ResponseEntity<String> result;

		try{

			result	= restTemplate.exchange(lockOperationCallBack, HttpMethod.POST,entity,String.class);
			log.info("ERPERP库存锁定（解锁）反馈结果的JSON是:"+result.getBody());

		}catch (Exception e){
			log.error("ERPERP库存锁定（解锁）反馈请求出现异常。",e);
			e.printStackTrace();
		}
	}

	@Override
	public void checkErrorCallBack(ERPCheckErrorDTO erpCheckErrorDTO) {
		RestTemplate restTemplate = new RestTemplate();

		String body = JSONUtil.toJsonStr(erpCheckErrorDTO);

		log.info("ERP盘点异常回调请求体是:"+body);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<>(body, headers);
		ResponseEntity<String> result;

		try{

			result	= restTemplate.exchange(checkErrorCallBack, HttpMethod.POST,entity,String.class);
			log.info("ERP盘点异常回调结果的JSON是:"+result.getBody());

		}catch (Exception e){
			log.error("ERP盘点异常回调请求出现异常。",e);
			e.printStackTrace();
		}
	}

}
