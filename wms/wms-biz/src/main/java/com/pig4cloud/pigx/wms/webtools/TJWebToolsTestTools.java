package com.pig4cloud.pigx.wms.webtools;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TJWebToolsTestTools {

	private static String url = "https://openapi.chanjet.com";

	private static String appkey = "av4YP2De";

	private static String appSecret = "5EAF5695C3A441FDDB124AB06D378B8D";

	private static String openToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2Nlc3NfdG9rZW4iOiIwNTE3YWYwZC1mODNjLTQ0MmQtOTFmZC1iMTQ5Y2Y2ZjYxNjIiLCJzdWIiOiJpc3YiLCJhdWQiOiJpc3YiLCJuYmYiOjE2Mjk1OTg3MjAsImFwcElkIjoiNTgiLCJzY29wZSI6ImF1dGhfYWxsIiwiaXNzIjoiY2hhbmpldCIsImV4cCI6MTYzMDExNzEyMCwidXNlcklkIjoiNjAwMTUxMjMxNjYiLCJpYXQiOjE2Mjk1OTg3MjAsIm9yZ0lkIjoiOTAwMTY0MzcxOTgiLCJvcmdBY2NvdW50IjoidXYxNGJ0bmhzaTRpIn0.IWppltPcTXdxGiI2tOgRAPz6jE5Hy62XDTjY500DBAc";

	public static void main(String[] args) {

		String uri = "https://openapi.chanjet.com/accounting/cia/api/v1/user";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("openToken",openToken);
		headers.add("appKey",appkey);
		headers.add("appSecret",appSecret);
		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.GET,entity,String.class);

		log.info(result.getBody());

	}
}
