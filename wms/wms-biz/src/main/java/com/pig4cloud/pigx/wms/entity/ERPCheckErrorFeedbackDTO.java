package com.pig4cloud.pigx.wms.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "ERP盘点异常反馈DTO")
public class ERPCheckErrorFeedbackDTO {
private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="盘点单号")
    private String taskId;

	@ApiModelProperty(value="checkInfo")
	private List<ERPCheckErrorConfirmInfoDTO> confirmInfo;

}
