package com.pig4cloud.pigx.wms.webtools;

public class Constants {

	public static final String BCXF = "波次下发";
	public static final String BHXF = "备货下发";
	public static final String SBH = "单订单备货";
	public static final String DBH = "多订单备货";
}
