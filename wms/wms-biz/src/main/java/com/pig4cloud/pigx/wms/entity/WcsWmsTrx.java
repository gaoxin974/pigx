/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * wcs和wms交互记录
 *
 * @author gaoxin
 * @date 2021-04-15 09:50:40
 */
@Data
@TableName("wcs_wms_trx")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "wcs和wms交互记录")
public class WcsWmsTrx extends Model<WcsWmsTrx> {
private static final long serialVersionUID = 1L;

    /**
     * 与wcs交互trx的id
     */
    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value="与wcs交互trx的id")
    private String wmsTrxId;

    @ApiModelProperty(value="1:托盘入库 2:托盘出库 3:产线入库 4:电商入库 5:空托补给PDA 6空框入库 7空框出库 8外购入库 9产线出库 10产线返库 11空托返叠 12补货出库 13备货入库 14备货出库")
    private String trxType;
    /**
     * 起始库位编号
     */
    @ApiModelProperty(value="起始库位编号")
    private String fromLocation;
    /**
     * 目标库位编号
     */
    @ApiModelProperty(value="目标库位编号")
    private String toLocation;
    /**
     * 0:未完成 1:已完成 2:失败
     */
    @ApiModelProperty(value="0:未完成 1:已完成 2:失败")
    private String trxStatus;
	/**
	 * 托盘编号
	 */
	@ApiModelProperty(value="托盘编号")
	private String trayNo;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private LocalDateTime updateTime;
    }
