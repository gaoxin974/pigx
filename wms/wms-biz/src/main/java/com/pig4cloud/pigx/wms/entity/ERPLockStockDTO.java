package com.pig4cloud.pigx.wms.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "ERP锁定解锁DTO")
public class ERPLockStockDTO {
private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="托盘流水号batchci")
    private String itemId;

	@ApiModelProperty(value="托盘编号")
	private String uniqueCode;

	@ApiModelProperty(value="sku")
	private String sku;

	@ApiModelProperty(value="批号")
	private String batchNo;

	@ApiModelProperty(value="数量")
	private String amount;

	@ApiModelProperty(value="库位")
	private String location;

	@ApiModelProperty(value="操作类型 0：锁定  1：解锁")
	private String operation;

}
