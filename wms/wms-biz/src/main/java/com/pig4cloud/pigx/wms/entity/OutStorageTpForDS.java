/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 入库单行项目托盘信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:39
 */
@Data
@ApiModel(value = "入库单行项目托盘信息")
public class OutStorageTpForDS extends Model<OutStorageTpForDS> {
private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="wms出库单号")
    private String wmsBillNo;

    @ApiModelProperty(value="sku")
    private String sku;

	@ApiModelProperty(value="skuName")
	private String skuName;

	@ApiModelProperty(value="数量")
	private Integer amount;

	@ApiModelProperty(value="单位")
	private String unit;

    @ApiModelProperty(value="上架库位编号")
    private String location;

}
