/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.wms.entity.Wlzx;
import com.pig4cloud.pigx.wms.service.WlzxService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 物流中心信息
 *
 * @author gaoxin
 * @date 2021-04-08 10:33:27
 */
@RestController
@AllArgsConstructor
@RequestMapping("/wlzx" )
@Api(value = "wlzx", tags = "物流中心信息管理")
public class WlzxController {

    private final  WlzxService wlzxService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param wlzx 物流中心信息
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    //@PreAuthorize("@pms.hasPermission('wms_wlzx_view')" )
    public R getWlzxPage(Page page, Wlzx wlzx) {
        return R.ok(wlzxService.page(page, Wrappers.query(wlzx)));
    }


    /**
     * 通过id查询物流中心信息
     * @param wlzxCode id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{wlzxCode}" )
    //@PreAuthorize("@pms.hasPermission('wms_wlzx_view')" )
    public R getById(@PathVariable("wlzxCode" ) String wlzxCode) {
        return R.ok(wlzxService.getById(wlzxCode));
    }

    /**
     * 新增物流中心信息
     * @param wlzx 物流中心信息
     * @return R
     */
    @ApiOperation(value = "新增物流中心信息", notes = "新增物流中心信息")
    @SysLog("新增物流中心信息" )
    @PostMapping
    //@PreAuthorize("@pms.hasPermission('wms_wlzx_add')" )
    public R save(@RequestBody Wlzx wlzx) {
        return R.ok(wlzxService.save(wlzx));
    }

    /**
     * 修改物流中心信息
     * @param wlzx 物流中心信息
     * @return R
     */
    @ApiOperation(value = "修改物流中心信息", notes = "修改物流中心信息")
    @SysLog("修改物流中心信息" )
    @PutMapping
    //@PreAuthorize("@pms.hasPermission('wms_wlzx_edit')" )
    public R updateById(@RequestBody Wlzx wlzx) {
        return R.ok(wlzxService.updateById(wlzx));
    }

    /**
     * 通过id删除物流中心信息
     * @param wlzxCode id
     * @return R
     */
    @ApiOperation(value = "通过id删除物流中心信息", notes = "通过id删除物流中心信息")
    @SysLog("通过id删除物流中心信息" )
    @DeleteMapping("/{wlzxCode}" )
    //@PreAuthorize("@pms.hasPermission('wms_wlzx_del')" )
    public R removeById(@PathVariable String wlzxCode) {
        return R.ok(wlzxService.removeById(wlzxCode));
    }

}
