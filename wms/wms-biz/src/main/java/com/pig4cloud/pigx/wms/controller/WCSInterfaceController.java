/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.controller;

import com.pig4cloud.pigx.wms.dto.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import com.pig4cloud.pigx.wms.entity.InStorageTp;
import com.pig4cloud.pigx.wms.service.InStorageTpService;
import com.pig4cloud.pigx.wms.service.WcsWmsTrxService;
import cn.hutool.json.JSONUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * WCS接口信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:32
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/WCS/" )
@Api(value = "WCS接口信息", tags = "WCS接口信息")
public class WCSInterfaceController {

	private final WcsWmsTrxService wcsWmsTrxService;

	private final InStorageTpService inStorageTpService;


	@ApiOperation(value = "WCS接口测试方法", notes = "WCS接口测试方法")
	@GetMapping("/getTestWCS")
	@Inner(false)
	public R getTestWCS() {

		boolean isSuccess = wcsWmsTrxService.getTestWCS();

		if(isSuccess){

			return R.ok(null,"调用wcs接口测试通过.");

		}else{

			return R.failed("调用wcs接口测试失败");

		}

	}

    /**
     * WCS入库空托盘获取库位地址
     * @return R
     */
    @ApiOperation(value = "WCS空托盘入库", notes = "WCS空托盘入库")
    @SysLog("WCS空托盘入库" )
    @PostMapping("/putEmptyTrayLocation")
    @Inner(false)
    public R putEmptyTrayLocation(WmsDpInvDTO wmsDpInvDTO) {

		String toLocation = wcsWmsTrxService.putEmptyTrayLocation(wmsDpInvDTO);

		if(toLocation != null){

			WmsDpInvReTurnDTO wmsDpInvReTurnDTO = new WmsDpInvReTurnDTO();
			wmsDpInvReTurnDTO.setDpDevId(wmsDpInvDTO.getDpDevId());
			wmsDpInvReTurnDTO.setInLoc(toLocation);
			wmsDpInvReTurnDTO.setTaskId(wmsDpInvDTO.getTaskId());

			return R.ok(wmsDpInvReTurnDTO);

		}else{

			return R.failed("未找到可以放托盘的空库位,请求出现异常");

		}

    }

	/**
	 * WCS获取空托盘的库位地址
	 * @return R
	 */
	@ApiOperation(value = "WCS获取托盘区的空托盘", notes = "WCS获取托盘区的空托盘")
	@SysLog("WCS获取托盘区的空托盘" )
	@PostMapping("/getEmptyTrayLocation")
	@Inner(false)
	public R getEmptyTrayLocation(WmsKpInvDTO wmsKpInvDTO) {

		String toLocation = wcsWmsTrxService.getEmptyTrayLocation(wmsKpInvDTO);

		if(toLocation != null){

			WmsKpInvReTurnDTO wmsKpInvReTurnDTO = new WmsKpInvReTurnDTO();
			wmsKpInvReTurnDTO.setKpDevId(wmsKpInvDTO.getKpDevId());
			wmsKpInvReTurnDTO.setOutLoc(toLocation);
			wmsKpInvReTurnDTO.setTaskId(wmsKpInvDTO.getTaskId());
			return R.ok(wmsKpInvReTurnDTO,"已找到空托盘,已经通知wcs运输...");

		}else{

			return R.failed("未找到托盘区有空托盘...");
		}


	}

	/**
	 * WCS回调接口
	 * @return R
	 */
	@ApiOperation(value = "WCS回调接口", notes = "WCS回调接口")
	@SysLog("WCS回调接口" )
	@PostMapping("/wcsCallBack")
	@Inner(false)
	public R wcsCallBack(@RequestBody WcsWmsCallBackDTO wcsWmsCallBackDTO) {

		log.info("收到WCS回调请求:"+JSONUtil.toJsonStr(wcsWmsCallBackDTO));

		if(wcsWmsTrxService.wcsCallBack(wcsWmsCallBackDTO)){

			return R.ok(null,"WMS处理成功");

		}else {

			return R.failed("WMS处理失败...");

		}

	}

	@ApiOperation(value = "WCS产线扫描托盘入库", notes = "WCS产线扫描托盘入库")
	@SysLog("WCS产线扫描托盘入库" )
	@GetMapping("/getEmptyTrayLocationForProduction/{trayNo}")
	@Inner(false)
	public R getEmptyTrayLocationForProduction(@PathVariable String trayNo) {

		InStorageTp inStorageTp = inStorageTpService.getOne(Wrappers.<InStorageTp>query().lambda()
				.eq(InStorageTp::getUniqueCode, trayNo).eq(InStorageTp::getStatus, "0"));

		if(inStorageTp == null){
         	//TODO 调用ERP产线入库无上游单据WMS回馈接口 托盘编号 trayNo


			log.info("未找到产线入库单信息,已调用ERP无上游单据接口,请重新发送此托盘相关的入库单 托盘编号:"+trayNo);
			return R.failed("未找到产线入库单信息,已调用ERP无上游单据接口,请重新发送此托盘相关的入库单");
		}

		WCSInvInDTO wcsInvInDTO = new WCSInvInDTO();

		boolean isSuccess = wcsWmsTrxService.getEmptyTrayLocationForProduction(trayNo,wcsInvInDTO);

		if(isSuccess){

			return R.ok(wcsInvInDTO,"已找到立库区空库位信息,已通知wcs运送货物到指定库位...");

		}else{

			return R.failed("未找到有空库位信息,入库失败...");
		}


	}


}
