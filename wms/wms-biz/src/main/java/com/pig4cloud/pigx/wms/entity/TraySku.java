/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 库位信息
 *
 * @author gaoxin
 * @date 2021-04-13 09:16:42
 */
@Data
@TableName("tray_sku")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "托盘临时信息")
public class TraySku extends Model<TraySku> {
private static final long serialVersionUID = 1L;


	@TableId(type = IdType.INPUT)
    @ApiModelProperty(value="托盘编码")
    private String trayNo;
    /**
     * 批号
     */
    @ApiModelProperty(value="批号")
    private String batchNo;
    /**
     * 批次
     */
    @ApiModelProperty(value="批次")
    private String batchCi;
    /**
     * 商品sku
     */
    @ApiModelProperty(value="商品sku")
    private String sku;
    /**
     * 货主ID
     */
    @ApiModelProperty(value="货主ID")
    private String huozId;
    /**
     * 真实货主ID
     */
    @ApiModelProperty(value="真实货主ID")
    private String realHuoz;
    /**
     * 库存
     */
    @ApiModelProperty(value="库存")
	private Integer stockNum;

	@ApiModelProperty(value="应出数量")
	private Integer mustNum;

	@ApiModelProperty(value="返库位置")
	private String sLocationNo;
    /**
     * 所属区域
     */
    @ApiModelProperty(value="所属区域")
    private Integer areaId;

	@ApiModelProperty(value="入库单编号")
	private String inPageNo;

	@ApiModelProperty(value="入库单行项目id")
	private Integer inStorageItemId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty(value="更新时间")
    private LocalDateTime updateTime;
    }
