package com.pig4cloud.pigx.wms.service.impl;

import com.pig4cloud.pigx.wms.dto.WCSInvOutDTO;
import com.pig4cloud.pigx.wms.service.WcsWmsTrxService;
import lombok.extern.slf4j.Slf4j;
import java.util.List;


@Slf4j
public class OutCallWcsThread extends Thread{

	private List<WCSInvOutDTO> wcsInvOutDTOList;

	private WcsWmsTrxService wcsWmsTrxService;

	public OutCallWcsThread(List<WCSInvOutDTO> wcsInvOutDTOList,WcsWmsTrxService wcsWmsTrxService){
		this.wcsInvOutDTOList = wcsInvOutDTOList;
		this.wcsWmsTrxService = wcsWmsTrxService;
	}

	//启动新线程调用wcs出库接口
	public void run(){
		System.out.println("启动新的线程调用wcs出库接口,线程名为: "+Thread.currentThread().getName());

		for(WCSInvOutDTO wcsInvOutDTO : wcsInvOutDTOList){

			//多线程调用wcs接口
			log.info("wcstrx信息已保存，开始调用wcs出库接口...");
			if(!wcsWmsTrxService.handleWCSInvOut(wcsInvOutDTO)){
				throw new RuntimeException("调用WCS出库接口异常!!!");
			}

			log.info("当前线程睡眠1秒后再调用wcs出库接口...");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				log.error("当前线程睡眠发生被打断异常...");
				e.printStackTrace();
			}

		}

	}

}
