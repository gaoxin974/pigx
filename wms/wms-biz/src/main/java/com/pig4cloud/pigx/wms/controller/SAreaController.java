/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.wms.entity.SArea;
import com.pig4cloud.pigx.wms.service.SAreaService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 库区表
 *
 * @author gaoxin
 * @date 2021-04-07 20:05:29
 */
@RestController
@AllArgsConstructor
@RequestMapping("/sarea" )
@Api(value = "sarea", tags = "库区表管理")
public class SAreaController {

    private final  SAreaService sAreaService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param sArea 库区表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    //@PreAuthorize("@pms.hasPermission('wms_sarea_view')" )
    public R getSAreaPage(Page page, SArea sArea) {
        return R.ok(sAreaService.page(page, Wrappers.query(sArea)));
    }


    /**
     * 通过id查询库区表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    //@PreAuthorize("@pms.hasPermission('wms_sarea_view')" )
    public R getById(@PathVariable("id" ) Integer id) {
        return R.ok(sAreaService.getById(id));
    }

    /**
     * 新增库区表
     * @param sArea 库区表
     * @return R
     */
    @ApiOperation(value = "新增库区表", notes = "新增库区表")
    @SysLog("新增库区表" )
    @PostMapping
    //@PreAuthorize("@pms.hasPermission('wms_sarea_add')" )
    public R save(@RequestBody SArea sArea) {
        return R.ok(sAreaService.save(sArea));
    }

    /**
     * 修改库区表
     * @param sArea 库区表
     * @return R
     */
    @ApiOperation(value = "修改库区表", notes = "修改库区表")
    @SysLog("修改库区表" )
    @PutMapping
    //@PreAuthorize("@pms.hasPermission('wms_sarea_edit')" )
    public R updateById(@RequestBody SArea sArea) {
        return R.ok(sAreaService.updateById(sArea));
    }

    /**
     * 通过id删除库区表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除库区表", notes = "通过id删除库区表")
    @SysLog("通过id删除库区表" )
    @DeleteMapping("/{id}" )
    //@PreAuthorize("@pms.hasPermission('wms_sarea_del')" )
    public R removeById(@PathVariable Integer id) {
        return R.ok(sAreaService.removeById(id));
    }

}
