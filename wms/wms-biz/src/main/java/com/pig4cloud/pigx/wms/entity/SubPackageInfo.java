/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 库区表
 *
 * @author gaoxin
 * @date 2021-10-24 15:47:00
 */
@Data
@TableName("sub_package_info")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "子波次信息表")
public class SubPackageInfo extends Model<SubPackageInfo> {
private static final long serialVersionUID = 1L;

	@TableId
	@ApiModelProperty(value="ID")
	private String id;

    @ApiModelProperty(value="子波次ID")
    private String subPackageNo;

    @ApiModelProperty(value="L区提总库位")
    private String lLocation;

	@ApiModelProperty(value="电商作业区")
	private String toLocation;

	@ApiModelProperty(value="来自哪个库位")
	private String fromLocation;

	@ApiModelProperty(value="托盘编号")
	private String trayNo;

	@ApiModelProperty(value="商品ID")
	private String sku;

	@ApiModelProperty(value="提总状态  0未提总1已完成")
	private String status;

	@ApiModelProperty(value="库存数量")
	private int trayAmount;

	@ApiModelProperty(value="实际数量")
	private int realAmount;

    @ApiModelProperty(value="")
    private LocalDateTime createTime;

    @ApiModelProperty(value="")
    private LocalDateTime updateTime;
    }
