/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.service;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pig4cloud.pigx.wms.dto.ERPCancelDTO;
import com.pig4cloud.pigx.wms.dto.ERPSystemReturnDTO;
import com.pig4cloud.pigx.wms.dto.InStoragePageDTO;
import com.pig4cloud.pigx.wms.dto.WGInStorageForPDADTO;
import com.pig4cloud.pigx.wms.entity.*;
import com.pig4cloud.pigx.wms.vo.InStorageItemVO;
import com.pig4cloud.pigx.wms.vo.InStoragePageVO;
import com.pig4cloud.pigx.wms.vo.InStorageTPVO;
import com.pig4cloud.pigx.wms.vo.PDADSInStorageDetailVO;
import com.pig4cloud.pigx.wms.vo.PDAWGInStorageDetailVO;
import com.pig4cloud.pigx.wms.vo.PDAWGInStorageVO;

/**
 * 入库单信息
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:32
 */
public interface InStorageService extends IService<InStorage> {

	void saveALL(ERPInStorageDTO erpInStorageDTO);

	IPage pageDSInStorageForPDA(Page page, String order);

	PDADSInStorageDetailVO getDSInStorageDetailByTPForPDA(String uniqueCode);

	void postDSInStorageDetailForPDA(String fromLocation,String uniqueCode)throws Exception;

	IPage<PDAWGInStorageVO> pageWGInStorageForPDA(Page page, String wmsBillNo, String sort);

	IPage<InStoragePageVO> getInStoragePageForVO(Page page, InStoragePageDTO inStoragePageDTO);

	List<InStorageItemVO> listItemForVO(String wmsBillNo);

	List<InStorageTPVO> listInStorageTP(String wmsBillNo);

	List<PDAWGInStorageDetailVO> listWGInStorageDetailForPDA(String wmsBillNo, String inStorageItemId);

	PDADSInStorageDetailVO getWGInStorageDetailForPDA(String wmsBillNo, String in_storage_item_id);

	boolean cancelBill(ERPCancelDTO erpCancelDTO);

	boolean getKuangForDS(String toLocation);

	void delForSave(String inPageNo);

	void postWGInStorageDetailForPDA(WGInStorageForPDADTO wgInStorageForPDADTO) throws Exception;

	void updatetoDoneById(String inStorageNo);

	StockArea getStockArea(InStorageItem inStorageItem, InStorage instorage, String trxType) throws Exception;

	StockArea getCheckErrorStockArea(InStorageItem inStorageItem, InStorage instorage) throws Exception;

	void inBillDone(String inBillNumber);

    boolean ERPlockStockAreas(List<ERPLockStockDTO> erpLockStockDTOS);
}
