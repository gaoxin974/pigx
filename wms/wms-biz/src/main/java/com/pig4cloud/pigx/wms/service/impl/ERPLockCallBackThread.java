package com.pig4cloud.pigx.wms.service.impl;

import com.pig4cloud.pigx.wms.dto.WCSInvOutDTO;
import com.pig4cloud.pigx.wms.entity.ERPLockStockDTO;
import com.pig4cloud.pigx.wms.service.ERPCallBackService;
import com.pig4cloud.pigx.wms.service.WcsWmsTrxService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;


@Slf4j
public class ERPLockCallBackThread extends Thread{

	private List<ERPLockStockDTO> erpLockStockDTOSForReturn;

	private ERPCallBackService erpCallBackService;

	public ERPLockCallBackThread(List<ERPLockStockDTO> erpLockStockDTOSForReturn, ERPCallBackService erpCallBackService){
		this.erpLockStockDTOSForReturn = erpLockStockDTOSForReturn;
		this.erpCallBackService = erpCallBackService;
	}

	public void run(){
		System.out.println("启动新的线程调用erp接口,线程名为: "+Thread.currentThread().getName());
		erpCallBackService.lockOperationCallBack(erpLockStockDTOSForReturn);

	}

}
