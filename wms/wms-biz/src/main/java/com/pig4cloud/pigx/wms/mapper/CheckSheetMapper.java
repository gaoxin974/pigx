package com.pig4cloud.pigx.wms.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.data.datascope.PigxBaseMapper;
import com.pig4cloud.pigx.wms.dto.InStoragePageDTO;
import com.pig4cloud.pigx.wms.dto.QueryCheckSheetsPageDTO;
import com.pig4cloud.pigx.wms.dto.QueryStockAreaForCheckPageDTO;
import com.pig4cloud.pigx.wms.dto.QueryStockAreaForLockPageDTO;
import com.pig4cloud.pigx.wms.entity.CheckSheet;
import com.pig4cloud.pigx.wms.vo.CheckSheetPageVO;
import com.pig4cloud.pigx.wms.vo.InStoragePageVO;
import com.pig4cloud.pigx.wms.vo.StockAreaForCheckPageVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface CheckSheetMapper extends PigxBaseMapper<CheckSheet> {
	IPage<StockAreaForCheckPageVO> getStockAreaForCheckPage(Page page,
															@Param("queryStockAreaForCheckPageDTO") QueryStockAreaForCheckPageDTO queryStockAreaForCheckPageDTO);

    IPage<CheckSheetPageVO> getCheckSheetsPage(Page page,
											   @Param("queryCheckSheetsPageDTO")QueryCheckSheetsPageDTO queryCheckSheetsPageDTO);

    IPage<StockAreaForCheckPageVO> getStockAreaForLockPage(Page page,
														   @Param("queryStockAreaForLockPageDTO") QueryStockAreaForLockPageDTO queryStockAreaForLockPageDTO);
}
