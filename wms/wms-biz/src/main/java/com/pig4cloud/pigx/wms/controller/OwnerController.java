/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.wms.entity.Owner;
import com.pig4cloud.pigx.wms.service.OwnerService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 货主信息
 *
 * @author gaoxin
 * @date 2021-04-08 10:33:23
 */
@RestController
@AllArgsConstructor
@RequestMapping("/owner" )
@Api(value = "owner", tags = "货主信息管理")
public class OwnerController {

    private final  OwnerService ownerService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param owner 货主信息
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    //@PreAuthorize("@pms.hasPermission('wms_owner_view')" )
    public R getOwnerPage(Page page, Owner owner) {
        return R.ok(ownerService.page(page, Wrappers.query(owner)));
    }


    /**
     * 通过id查询货主信息
     * @param huozId id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{huozId}" )
    //@PreAuthorize("@pms.hasPermission('wms_owner_view')" )
    public R getById(@PathVariable("huozId" ) String huozId) {
        return R.ok(ownerService.getById(huozId));
    }

    /**
     * 新增货主信息
     * @param owner 货主信息
     * @return R
     */
    @ApiOperation(value = "新增货主信息", notes = "新增货主信息")
    @SysLog("新增货主信息" )
    @PostMapping
    //@PreAuthorize("@pms.hasPermission('wms_owner_add')" )
    public R save(@RequestBody Owner owner) {
        return R.ok(ownerService.save(owner));
    }

    /**
     * 修改货主信息
     * @param owner 货主信息
     * @return R
     */
    @ApiOperation(value = "修改货主信息", notes = "修改货主信息")
    @SysLog("修改货主信息" )
    @PutMapping
    //@PreAuthorize("@pms.hasPermission('wms_owner_edit')" )
    public R updateById(@RequestBody Owner owner) {
        return R.ok(ownerService.updateById(owner));
    }

    /**
     * 通过id删除货主信息
     * @param huozId id
     * @return R
     */
    @ApiOperation(value = "通过id删除货主信息", notes = "通过id删除货主信息")
    @SysLog("通过id删除货主信息" )
    @DeleteMapping("/{huozId}" )
    //@PreAuthorize("@pms.hasPermission('wms_owner_del')" )
    public R removeById(@PathVariable String huozId) {
        return R.ok(ownerService.removeById(huozId));
    }

}
