/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.controller;

import java.util.List;

import com.pig4cloud.pigx.wms.entity.*;
import com.pig4cloud.pigx.wms.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.common.security.annotation.Inner;
import com.pig4cloud.pigx.wms.dto.ERPCancelDTO;
import com.pig4cloud.pigx.wms.dto.ERPOutStorageDTO;
import com.pig4cloud.pigx.wms.dto.ERPOutStorageItemDTO;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * ERP接口
 *
 * @author gaoxin
 * @date 2021-04-13 14:48:32
 */
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/ERP/system")
@Api(value = "system", tags = "ERP接口信息")
public class ERPSystemController {

	private final InStorageService inStorageService;

	private final OutStorageService outStorageService;

	private final SkuService skuService;

	private final CheckSheetService checkSheetService;

	/**
	 * 批量新增商品信息表
	 *
	 * @param skus 商品信息表 批量
	 * @return R
	 */
	@ApiOperation(value = "批量新增商品信息表", notes = "批量新增商品信息表")
	@SysLog("批量新增商品信息表")
	@PostMapping("/createSkuBatch")
	@Inner(value = false)
	public R saveBatch(@RequestBody List<Sku> skus) {

		log.info("收到ERP批量新增商品信息请求:" + JSONUtil.toJsonStr(skus));

		return R.ok(skuService.saveOrUpdateBatch(skus));
	}

	/**
	 * ERP新增入库单信息
	 *
	 * @param erpInStorageDTO 入库单信息
	 * @return R
	 */
	@ApiOperation(value = "ERP新增入库单信息", notes = "ERP新增入库单信息")
	@SysLog("ERP新增入库单信息")
	@PostMapping("/ERPCreateInstorageBill")
	@Inner(false)
	public R ERPCreateInstorageBill(@RequestBody ERPInStorageDTO erpInStorageDTO) {

		log.info("收到ERP新增入库单请求:" + JSONUtil.toJsonStr(erpInStorageDTO));

		//检查单据参数
		if (StringUtils.isEmpty(erpInStorageDTO.getInBillNumber())) {
			return R.failed("入库单有空的InBillNumber数据!wms无法处理...入库单编号是:" + erpInStorageDTO.getInPageNo());
		}

		List<ERPInStorageItemDTO> erpInStorageItemDTOList = erpInStorageDTO.getErpInStorageItemDTOList();

		for (int i = 0; i < erpInStorageItemDTOList.size(); i++) {
			ERPInStorageItemDTO erpInStorageItemDTO = erpInStorageItemDTOList.get(i);

			if (StringUtils.isEmpty(erpInStorageItemDTO.getSku())) {

				return R.failed("入库单有空的商品主数据!wms无法处理...入库单编号是:" + erpInStorageDTO.getInPageNo());

			}

			Sku sku = skuService.getById(erpInStorageItemDTO.getSku());

			if (null == sku) {

				String msg = "入库单商品主数据wms不存在,wms无法处理...入库单编号是:" + erpInStorageDTO.getInPageNo()
						+ ",SKU是:" + erpInStorageItemDTO.getSku();

				return R.restResult(null, 9001, msg);
			}
		}

		InStorage inStorage = inStorageService.getById(erpInStorageDTO.getInPageNo());

		//如果没查到 表示新建入库单
		if (inStorage == null) {

			inStorageService.saveALL(erpInStorageDTO);
			return R.ok("入库单新建成功.入库单编号是:" + erpInStorageDTO.getInPageNo());

		} else if ("0".equals(inStorage.getStatus())) {

			inStorageService.delForSave(erpInStorageDTO.getInPageNo());
			inStorageService.saveALL(erpInStorageDTO);
			return R.ok("入库单更新成功.入库单编号是:" + erpInStorageDTO.getInPageNo());

		} else {
			return R.failed("入库单状态不是待入库无法更新.入库单编号是:" + erpInStorageDTO.getInPageNo());
		}


	}

	/**
	 * ERP出入库单取消接口
	 *
	 * @param erpCancelDTO ERP出入库单取消参数DTO
	 * @return R
	 */
	@ApiOperation(value = "ERP出入库单取消接口", notes = "ERP出入库单取消接口")
	@SysLog("ERP出入库单取消接口")
	@PostMapping("/ERPCancelBill")
	@Inner(false)
	public R ERPCancelBill(@RequestBody ERPCancelDTO erpCancelDTO) {

		log.info("收到ERP出入库单取消接口请求:" + JSONUtil.toJsonStr(erpCancelDTO));

		if (inStorageService.cancelBill(erpCancelDTO)) {
			return R.ok(Boolean.TRUE, "单据已取消,单据号:" + erpCancelDTO.getPageNo());

		} else {

			return R.failed(Boolean.FALSE, "单据取消失败,单据号:" + erpCancelDTO.getPageNo());
		}

	}

	/**
	 * ERP 电商入库空托调取接口
	 *
	 * @return R
	 */
	@ApiOperation(value = "ERP电商入库空框(托)获取", notes = "ERP电商入库空框(托)获取")
	@SysLog("ERP电商入库空框(托)获取")
	@GetMapping("/ERPgetKKforDS/{toLocation}")
	@Inner(false)
	public R ERPgetKuangForDS(@PathVariable("toLocation") String toLocation) {

		log.info("收到ERP电商入库空框(托)获取请求.");

		if (StringUtils.isEmpty(toLocation)) {

			return R.failed("toLocation不能为空");

		}

		boolean success = inStorageService.getKuangForDS(toLocation);

		if (success) {
			return R.ok("电商空框(托)已找到正在运往指定地点..." + toLocation);

		} else {

			return R.failed("电商空框(托)获取失败");
		}

	}

	/**
	 * ERP新增出库单信息
	 *
	 * @param erpOutStorageDTO 出库单信息
	 * @return R
	 */
	@ApiOperation(value = "ERP新增产线出库单信息", notes = "ERP新增产线出库单信息")
	@SysLog("ERP新增产线出库单信息")
	@PostMapping("/ERPCreateOutstorageBill")
	@Inner(false)
	public R ERPCreateOutstorageBill(@RequestBody ERPOutStorageDTO erpOutStorageDTO) {

		log.info("收到ERP产线出库单创建或更新请求:" + JSONUtil.toJsonStr(erpOutStorageDTO));

		//检查出库单参数
		List<ERPOutStorageItemDTO> erpOutStorageItemDTOList = erpOutStorageDTO.getErpOutStorageItemDTOList();

		for (int i = 0; i < erpOutStorageItemDTOList.size(); i++) {

			ERPOutStorageItemDTO erpOutStorageItemDTO = erpOutStorageItemDTOList.get(i);

			if (StringUtils.isEmpty(erpOutStorageItemDTO.getSku())) {

				return R.failed("出库单有空的商品主数据!wms无法处理...出库单编号是:" + erpOutStorageDTO.getOutPageNo());

			}

		}

		outStorageService.saveALLCX(erpOutStorageDTO);

		return R.ok(null, "出库单新建或更新成功！");

	}

	@ApiOperation(value = "ERP结单", notes = "ERP结单")
	@SysLog("ERP发送结单信息")
	@PostMapping("/ERPinBillDone")
	@Inner(false)
	public R ERPinBillDone(@RequestBody String inBillNumberJSON) {

		log.info("收到ERP结单请求 inBillNumberJSON:" + inBillNumberJSON);

		JSONObject jsonObject = JSONUtil.parseObj(inBillNumberJSON);

		String inBillNumber = jsonObject.getStr("inBillNumber");

		log.info("转换JSON对象抽取inBillNumber结果为:" + inBillNumber);

		//检查出库单参数
		if (StringUtils.isEmpty(inBillNumber)) {

			return R.failed("ERP结单请求出现异常!wms无法处理...inBillNumber为空！");

		}

		inStorageService.inBillDone(inBillNumber);

		return R.ok(null, "wms处理成功！");

	}

	@ApiOperation(value = "ERP锁定/冻结库区", notes = "ERP锁定/冻结库区")
	@SysLog("ERP锁定/冻结库区")
	@PostMapping("/lockOperation")
	@Inner(false)
	public R lockOperation(@RequestBody List<ERPLockStockDTO> erpLockStockDTOs) {

		log.info("收到ERP冻结/解冻请求 :" + JSONUtil.toJsonStr(erpLockStockDTOs));

		inStorageService.ERPlockStockAreas(erpLockStockDTOs);

		return R.ok(null, "wms冻结处理成功！");


	}

	@ApiOperation(value = "ERP盘点反馈", notes = "ERP盘点反馈")
	@SysLog("ERP盘点反馈")
	@PostMapping("/checkFeedback")
	@Inner(false)
	public R checkFeedback(@RequestBody ERPCheckErrorFeedbackDTO erpCheckErrorFeedbackDTO) {

		log.info("收到ERP盘点反馈请求 :" + JSONUtil.toJsonStr(erpCheckErrorFeedbackDTO));

		checkSheetService.handleERPCheckFeedback(erpCheckErrorFeedbackDTO);

		return R.ok(null, "wms盘点反馈处理成功！");


	}

}
