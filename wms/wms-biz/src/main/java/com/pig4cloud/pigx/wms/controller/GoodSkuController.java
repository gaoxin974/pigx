/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.pig4cloud.pigx.wms.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.pigx.common.core.util.R;
import com.pig4cloud.pigx.common.log.annotation.SysLog;
import com.pig4cloud.pigx.wms.dto.GoodSkuDTO;
import com.pig4cloud.pigx.wms.entity.GoodSku;
import com.pig4cloud.pigx.wms.service.GoodSkuService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 电商畅销SKU信息
 *
 * @author gaoxin
 * @date 2021-04-08 15:34:28
 */
@RestController
@AllArgsConstructor
@RequestMapping("/goodsku" )
@Api(value = "goodsku", tags = "电商畅销SKU信息管理")
public class GoodSkuController {

    private final  GoodSkuService goodSkuService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param goodSku 电商畅销SKU信息
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    //@PreAuthorize("@pms.hasPermission('wms_goodsku_view')" )
    public R getGoodSkuPage(Page page, GoodSku goodSku) {
        return R.ok(goodSkuService.page(page, Wrappers.query(goodSku)));
    }

	/**
	 * 分页查询
	 * @param page 分页对象
	 * @param goodSkuName 电商畅销SKU名称
	 * @return
	 */
	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/pageForVO" )
	//@PreAuthorize("@pms.hasPermission('wms_goodsku_view')" )
	public R getGoodSkuPageVO(Page page, GoodSkuDTO goodSkuDTO) {
		return R.ok(goodSkuService.getGoodSkuVoPage(page, goodSkuDTO));
	}


    /**
     * 通过id查询电商畅销SKU信息
     * @param sku id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{sku}" )
    //@PreAuthorize("@pms.hasPermission('wms_goodsku_view')" )
    public R getById(@PathVariable("sku" ) String sku) {
        return R.ok(goodSkuService.getById(sku));
    }

    /**
     * 新增电商畅销SKU信息
     * @param goodSku 电商畅销SKU信息
     * @return R
     */
    @ApiOperation(value = "新增电商畅销SKU信息", notes = "新增电商畅销SKU信息")
    @SysLog("新增电商畅销SKU信息" )
    @PostMapping
    //@PreAuthorize("@pms.hasPermission('wms_goodsku_add')" )
    public R save(@RequestBody GoodSku goodSku) {
        return R.ok(goodSkuService.save(goodSku));
    }

    /**
     * 修改电商畅销SKU信息
     * @param goodSku 电商畅销SKU信息
     * @return R
     */
    @ApiOperation(value = "修改电商畅销SKU信息", notes = "修改电商畅销SKU信息")
    @SysLog("修改电商畅销SKU信息" )
    @PutMapping
    //@PreAuthorize("@pms.hasPermission('wms_goodsku_edit')" )
    public R updateById(@RequestBody GoodSku goodSku) {
        return R.ok(goodSkuService.updateById(goodSku));
    }

    /**
     * 通过id删除电商畅销SKU信息
     * @param sku id
     * @return R
     */
    @ApiOperation(value = "通过id删除电商畅销SKU信息", notes = "通过id删除电商畅销SKU信息")
    @SysLog("通过id删除电商畅销SKU信息" )
    @DeleteMapping("/{sku}" )
    //@PreAuthorize("@pms.hasPermission('wms_goodsku_del')" )
    public R removeById(@PathVariable String sku) {
        return R.ok(goodSkuService.removeById(sku));
    }

}
