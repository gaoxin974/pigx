package com.pig4cloud.pigx.wms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pig4cloud.pigx.wms.entity.ERPCheckFeedback;
import com.pig4cloud.pigx.wms.mapper.ERPCheckFeedbackMapper;
import com.pig4cloud.pigx.wms.service.ERPCheckFeedbackService;
import org.springframework.stereotype.Service;


@Service
public class ERPCheckFeedbackServiceImpl extends ServiceImpl<ERPCheckFeedbackMapper, ERPCheckFeedback> implements ERPCheckFeedbackService {

}
