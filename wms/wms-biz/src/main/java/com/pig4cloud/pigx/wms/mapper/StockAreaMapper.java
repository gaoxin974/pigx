package com.pig4cloud.pigx.wms.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.pig4cloud.pigx.common.data.datascope.PigxBaseMapper;
import com.pig4cloud.pigx.wms.entity.StockArea;
import com.pig4cloud.pigx.wms.vo.StockAreaDetailForVO;
import com.pig4cloud.pigx.wms.vo.StockAreaForVO;

@Mapper
public interface StockAreaMapper extends PigxBaseMapper<StockArea> {

//	@InterceptorIgnore(tenantLine = "1")
	List<StockAreaForVO> listStockAreaForVO(@Param("tier") int tier);

	List<StockArea> listStockAreaByAreaIdForVO(@Param("areaId")int areaId,@Param("tier")int tier);

	StockAreaDetailForVO getStockAreaDetailVO(@Param("stockAreaNo")String stockAreaNo);
}
